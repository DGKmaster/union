clc
clear
% Define Base Coordinate System (BCS)
% Define Rotation matrix (R) as identity matrix
R_BCS = eye(3);
tr_BCS = [0,0,0]';
BCS = SE3(R_BCS,tr_BCS);

% Define CS1 with respect to BCS
R_CS1wB = rotz(pi/4);
tr_CS1wB = [2,2,1]';
CS1wB = SE3(R_CS1wB,tr_CS1wB); 

% Define CS2 with respect to CS1
R_CS2wCS1 = rotz(pi/4);
tr_CS2wCS1 = [0,2,1]';
CS2wCS1 = SE3(R_CS2wCS1,tr_CS2wCS1); 
% Define CS2 with respect to BCS
CS2wB = SE3(CS1wB.T*CS2wCS1.T);

% Define vector in CS2
V_CS2 = [1,1,1]';
% Transform vector from CS2 to BCS
V_B = (h2e(CS2wB.T*e2h(V_CS2)))';

% Define point 1 in CS1
P1_CS1 = [2,0,1]';
% Transorm point from CS1 to BCS
P1_B = h2e(CS1wB.T*e2h(P1_CS1));

% Define point 2 in CS1 by translation point 1
tr_P1_CS1 = [1,1,0]';
P2_CS1 = h2e(SE3(tr_P1_CS1).T*e2h(P1_CS1));
% Transorm point from CS1 to BCS
P2_B = h2e(CS1wB.T*e2h(P2_CS1));

% Define plot properties
figure(1);
axis([-.5, 4, -.5, 5, -.5, 3]); 
grid on;
hold on;
% Show BCS
trplot(BCS,'frame','BCS','color','k'); 
% Show CS1 
trplot(CS1wB,'frame','CS1','color','b');
% Show CS2 
trplot(CS2wB,'frame','CS2','color','g');

% Show real vector
arrow3((CS2wB.t)',V_B,'r');
% Show end vector point from CS1
arrow3((CS1wB.t)',V_B,'b');
% Show end vector point from BCS
arrow3((BCS.t)',V_B,'g');

% Show point 1
plotp(P1_B,'*','color','r');
% Show point 2
plotp(P2_B,'*','color','b');