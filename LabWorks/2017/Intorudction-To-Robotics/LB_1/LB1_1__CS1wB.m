% Define base coordinate system (BCS)
% Initialize rotation matrix (R) as identity matrix
R = eye(3);
BCS = R;
% Show BCS on 3D plot in black colour
figure(1);
trplot(BCS,'frame','BCS','color','k');
grid on;

% Calculate rotated coordinate system with respect to BCS (CS1wB)
z1 = pi/4;
y = pi/4;
z2 = pi/3;
% Use Euler transformation
R = eul2r(z1,y,z2);
CS1wB = R;
hold on;
% Show CS1 in blue colour
trplot(CS1wB, 'frame', 'CS1', 'color', 'b');
grid on;