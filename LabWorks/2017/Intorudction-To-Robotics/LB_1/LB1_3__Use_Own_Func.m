% Define base coordinate system (BCS)
% Initialize rotation matrix (R) as identity matrix
R = eye(3);
BCS = R;
% Show BCS on 3D plot in black colour
figure(1);
trplot(BCS,'frame','BCS','color','k');
grid on;

% Calculate rotated coordinate system with respect to BCS (CS1wB)
z1 = pi/4;
y = pi/4;
z2 = pi/3;

% Calculate rotation matrixes
RotZ1 = [ 1,       0,        0;
         0, cos(z1), -sin(z1);
         0, sin(z1), cos(z1)];
     
RotY = [  cos(y),    0,  sin(y);
               0,    1,       0;
         -sin(y),    0, cos(y)];
     
RotZ2 = [ cos(z2), -sin(z2),  0;
         sin(z2),  cos(z2),  0;
               0,        0,  1];
% Calculate result rotation matrix (R)
R = RotZ1*RotY*RotZ2;
CS1wB = R;
hold on;
% Show CS1 in blue colour
trplot(CS1wB, 'frame', 'CS1', 'color', 'b');
grid on;

% Create new vector in CS1
vector_CS1 = [0, 1, 1];
hold on;

% Interpret vector from CS1 to BCS
vector_BCS = R*vector_CS1';
hold on;
% Draw vector_CS1
arrow3( [0,0,0], vector_BCS', 'g' );
% Increase the grid size
axis([-2,2,-2,2,-2,2]);   
axis([-2,2,-2,2,-2,2]);   