function user_al5dInvKin( varargin )
clc 

global d1 a2 a3 a4;

global x y z psi phi2;
global robot;
global th3_config;
global robot_fig

%% Inverse kinematics

% Angle calculation
th1 = atan2(-y,-x)

% Calculation of rotation matrix by Euler Z-X-Z
Rz1 = [ cos(th1), -sin(th1), 0; ...
        sin(th1),  cos(th1), 0; ...
           0,       0,       1]; ...

Rx = [ 1,      0,       0; ...
         0, cos(psi), -sin(psi); ...
         0, sin(psi),  cos(psi)] ...

Rz2 = [ cos(phi2), -sin(phi2), 0; ...
        sin(phi2),  cos(phi2), 0; ...
           0,         0,       1]; ...

% Rotation matrix Euler Z-X-Z
R = Rz1*Rx*Rz2;

% Robot pose in 4x4 homogenous matrix
H_4_wrt_0_des = [R, [x;y;z]; ...
                [0,0,0,1] ];

%% Calculate joints q2 (th2) and q3 (th3)
% Position of RF1 wrist. RF0 (RF - reference frame)
p_0_1_wrt_0 = [0;0;d1];
            
p_0_3_wrt_0 = [ x - a4*H_4_wrt_0_des(1,1); ...
                y - a4*H_4_wrt_0_des(2,1); ...
                z - a4*H_4_wrt_0_des(3,1)];

% Find p_1_3_wrt_0
p_1_3_wrt_0 = p_0_3_wrt_0 - p_0_1_wrt_0;

% Rotation matrix to modify p_1_3_wrt_0 -> p_1_3_wrt_1
R_1_wrt_0 = [ cos(th1), 0,  sin(th1); ...
              sin(th1), 0, -cos(th1); ...
                 0,     1,        0];

% Calculate pose
p_1_3_wrt_1 =  R_1_wrt_0'*p_1_3_wrt_0;

% Find th3
costh3 = (( norm(p_1_3_wrt_1) )^2 - a2^2 - a3^2 )/ 2*a2*a3;
sinth3 = sqrt(1 - costh3^2);
%sinth3 = -sqrt(1 - costh3^2);
th3 = atan2(sinth3, costh3);

% Find th2
th2 = atan2(p_1_3_wrt_1(2)*(a2+a3*costh3) - p_1_3_wrt_1(1)*a3*sinth3, ...
            p_1_3_wrt_1(1)*(a2+a3*costh3) + p_1_3_wrt_1(2)*a3*sinth3);


        
%% Calculate joints q4 (th4)
th4 = atan2(-H_4_wrt_0_des(3,1)*cos(th2 + th3) + H_4_wrt_0_des(3,2)*sin(th2 + th3), ...
            -H_4_wrt_0_des(3,1)*sin(th2 + th3) - H_4_wrt_0_des(3,2)*cos(th2 + th3));

% Set offsets
th2 = th2 - pi/2;
th3 = th3 - pi/2;
th4 = th4 + pi/2;

% Collect all angles in one array
th = [th1,th2,th3,th4];

% Draw manipulator
figure(robot_fig);
robot.plot(th,'delay',0);

end