clear all
close all
clc

format compact

% Let's assemble the serial kinematic structure
al5d = al5d_rtb_build();

% Direct kinematics using Euler Z-X-Z angles
psi = -pi/2; % As about x (Only +/- pi/2)
phi2 = +pi/2; % As around z2

% Default manipulator position
px = 0.1;
py = 0.1;
pz = 0.3;

% Configuration for the link 3
th3_config = +1; % Only values +/- 1

% GUI usage
gui_al5dInvKin( al5d, [px,py,pz,psi,phi2], th3_config );