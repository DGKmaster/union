function [ al5d ] = al5d_rtb_build()

global d1 a2 a3 a4;

% Parameters for manipulator links
d1 = 70e-3;
a2 = 146e-3;
a3 = 187e-3;
a4 = 86e-3;

% Set up manipulator links parameters
L1 = Link('d',d1,'a',0,'alpha',pi/2,'offset',0);
L2 = Link('d',0,'a',a2,'alpha',0,'offset',pi/2);
L3 = Link('d',0,'a',a3,'alpha',0,'offset',pi/2);
L4 = Link('d',0,'a',a4,'alpha',0,'offset',0);

% Set up manipulator
al5d = SerialLink([L1,L2,L3,L4],'name','SCARA');

end