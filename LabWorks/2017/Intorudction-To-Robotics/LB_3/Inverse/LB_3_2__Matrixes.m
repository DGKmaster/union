close all 
clear all 
clc 
%Build direct kinematics model with Denavit-Hartenberg method 

% DH parameters for Link 1
th1off = sym(0); 
syms d1 real; 
a1 = sym(0); 
alpha1 = sym(pi/2); 

% DH parameters for Link 2
th2off = sym(pi/2); 
d2 = sym(0); 
syms a2 real; 
alpha2 = sym(0); 

% DH parameters for Link 3
th3off = sym(pi/2); 
d3 = sym(0); 
syms a3 real; 
alpha3 = sym(0); 

% DH parameters for Link 4
th4off = sym(0); 
d4 = sym(0); 
syms a4 real; 
alpha4 = sym(0); 

T1w0 = Link('d',d1,'a',a1,'alpha',alpha1,'offset',th1off); 
T2w1 = Link('d',d2,'a',a2,'alpha',alpha2,'offset',th2off); 
T3w2 = Link('d',d3,'a',a3,'alpha',alpha3,'offset',th3off); 
T4w3 = Link('d',d4,'a',a4,'alpha',alpha4,'offset',th4off); 

%al5d = SerialLink([t1w0,t2w1,t3w2,t4w3],'name','al5d'); 

syms th1 th2 th3 th4 real 

syms r11 r12 r13 r21 r22 r23 r31 r32 r33 px py pz real 

% Position of the top of the robot - given
KS4w0des = [r11,    r12,    r13,    px; ... 
            r21,    r22,    r23,    py; ... 
            r31,    r32,    r33,    pz; ... 
            sym(0), sym(0), sym(0), sym(1) ]; 

% The position of the top of the robot - from the dir. Kin.
disp('Position KS4 according to KS0:') 
KS4w0 = simplify(T1w0.A(th1)*T2w1.A(th2)*T3w2.A(th3)*T4w3.A(th4)) 

disp('Position KS1 according to KS0:') 
KS1w0 = T1w0.A(th1) 

% Position KS3 according to KS0
disp('Position KS3 with respect to KS0: KS3w0=KS4w0des*inv(T4w3) ') 
KS3w4 = inv(T4w3.A(th4));
KS3w0 = simplify(KS4w0des*KS3w4.T) 

% Position KS3 according to KS0
disp('Position KS3 with respect to KS0: KS3w0=KS1w0*KS2w1*KS3w2') 
KS3w0 = simplify(T1w0.A(th1)*T2w1.A(th2)*T3w2.A(th3))