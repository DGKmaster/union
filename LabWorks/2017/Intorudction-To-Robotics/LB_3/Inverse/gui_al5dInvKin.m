function varargout = gui_al5dInvKin(varargin)
% GUI_AL5DINVKIN MATLAB code for gui_al5dInvKin.fig
%      GUI_AL5DINVKIN, by itself, creates a new GUI_AL5DINVKIN or raises the existing
%      singleton*.
%
%      H = GUI_AL5DINVKIN returns the handle to a new GUI_AL5DINVKIN or the handle to
%      the existing singleton*.
%
%      GUI_AL5DINVKIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_AL5DINVKIN.M with the given input arguments.
%
%      GUI_AL5DINVKIN('Property','Value',...) creates a new GUI_AL5DINVKIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_al5dInvKin_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_al5dInvKin_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui_al5dInvKin

% Last Modified by GUIDE v2.5 26-Aug-2017 18:03:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_al5dInvKin_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_al5dInvKin_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui_al5dInvKin is made visible.
function gui_al5dInvKin_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui_al5dInvKin (see VARARGIN)

% Choose default command line output for gui_al5dInvKin
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
  
global robot;
  robot = varargin{1,1};
  
global robot_fig
  robot_fig = figure(); % Slika v katero se izrise kin. struktura robota
  
global range_orientation
  range_orientation = pi; % Skalirna vrednost s katero se pomnozijo drsniki za orientacijo (drsniki so v obmocju od -1 do 1)

global range_translation
  range_translation = 0.5; % Skalirna vrednost s katero se pomnozijo drsniki za translacijo (drsniki so v obmocju od -1 do 1)
  
global x y z psi phi2;
global pr;
  
  pr = varargin{1,2};
  x = pr(1);
  y = pr(2);
  z = pr(3);
  psi = pr(4);
  phi2 = pr(5);

  set(handles.slider_X,'Value',x/range_translation);
  set(handles.slider_Y,'Value',y/range_translation);
  set(handles.slider_Z,'Value',z/range_translation);
  set(handles.slider_PHI2,'Value',phi2/range_orientation);

  if psi < 0
    set(handles.PSI_pos,'Value',0);
    set(handles.PSI_neg,'Value',1);
  else
    set(handles.PSI_pos,'Value',1);
    set(handles.PSI_neg,'Value',0);
  end
  
  set(handles.text_X,'String',num2str(x));
  set(handles.text_Y,'String',num2str(y));
  set(handles.text_Z,'String',num2str(z));
  set(handles.text_PHI2,'String',num2str(phi2));
    
  global th3_config;
    th3_config = varargin{1,3};
  
  if th3_config < 0
    set(handles.th3_config_pos,'Value',0);
    set(handles.th3_config_neg,'Value',1);
  else
    set(handles.th3_config_pos,'Value',1);
    set(handles.th3_config_neg,'Value',0);
  end
  
  user_al5dInvKin();
  
% UIWAIT makes gui_al5dInvKin wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gui_al5dInvKin_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on slider movement.
function slider_X_Callback(hObject, eventdata, handles)
% hObject    handle to slider_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global x
global range_translation
x = range_translation*get(handles.slider_X,'Value');
set(handles.text_X,'String',num2str(x));
user_al5dInvKin();

  
% --- Executes during object creation, after setting all properties.
function slider_X_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on slider movement.
function slider_Y_Callback(hObject, eventdata, handles)
% hObject    handle to slider_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global y
global range_translation
y = range_translation*get(handles.slider_Y,'Value');
set(handles.text_Y,'String',num2str(y));
user_al5dInvKin();
  

% --- Executes during object creation, after setting all properties.
function slider_Y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_Z_Callback(hObject, eventdata, handles )
% hObject    handle to slider_Z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global z
global range_translation
z = range_translation*get(handles.slider_Z,'Value');
set(handles.text_Z,'String',num2str(z));
user_al5dInvKin();


% --- Executes during object creation, after setting all properties.
function slider_Z_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_Z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_PHI2_Callback(hObject, eventdata, handles)
% hObject    handle to slider_PHI2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global phi2;
global range_orientation;
phi2 = range_orientation*get(handles.slider_PHI2,'Value');
set(handles.text_PHI2,'String',num2str(phi2));
user_al5dInvKin();

% --- Executes during object creation, after setting all properties.
function slider_PHI2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_PHI2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on button press in th3_config_pos.
function th3_config_pos_Callback(hObject, eventdata, handles)
% hObject    handle to th3_config_pos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of th3_config_pos

global th3_config

th3cfg = get(handles.th3_config_pos,'Value');

if th3cfg == 1
  set(handles.th3_config_neg,'Value',0);
  th3_config = +1;
else
  set(handles.th3_config_neg,'Value',1);
  th3_config = -1;
end

user_al5dInvKin();


% --- Executes on button press in th3_config_neg.
function th3_config_neg_Callback(hObject, eventdata, handles)
% hObject    handle to th3_config_neg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of th3_config_neg
global th3_config

th3cfg = get(handles.th3_config_neg,'Value');

if th3cfg == 1
  set(handles.th3_config_pos,'Value',0);
  th3_config = -1;
else
  set(handles.th3_config_pos,'Value',1);
  th3_config = +1;
end

user_al5dInvKin();


% --- Executes on button press in PSI_neg.
function PSI_neg_Callback(hObject, eventdata, handles)
% hObject    handle to PSI_neg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PSI_neg
global psi

buttonVal = get(handles.PSI_neg,'Value');

if buttonVal == 1
  set(handles.PSI_pos,'Value',0);
  psi = -pi/2;
else
  set(handles.PSI_pos,'Value',1);
  psi = pi/2;
end

user_al5dInvKin();

% --- Executes on button press in PSI_pos.
function PSI_pos_Callback(hObject, eventdata, handles)
% hObject    handle to PSI_pos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PSI_pos
global psi

buttonVal = get(handles.PSI_pos,'Value');

if buttonVal == 1
  set(handles.PSI_neg,'Value',0);
  psi = pi/2;
else
  set(handles.PSI_neg,'Value',1);
  psi = -pi/2;
end

user_al5dInvKin();


% --- Executes on button press in resetbutton.
function resetbutton_Callback(hObject, eventdata, handles)
% hObject    handle to resetbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global range_orientation;
global range_translation;
global x y z psi phi2;
global pr;
  
  x = pr(1);
  y = pr(2);
  z = pr(3);
  psi = pr(4);
  phi2 = pr(5);

  set(handles.slider_X,'Value',x/range_translation);
  set(handles.slider_Y,'Value',y/range_translation);
  set(handles.slider_Z,'Value',z/range_translation);
  set(handles.slider_PHI2,'Value',phi2/range_orientation);

  if psi < 0
    set(handles.PSI_pos,'Value',0);
    set(handles.PSI_neg,'Value',1);
  else
    set(handles.PSI_pos,'Value',1);
    set(handles.PSI_neg,'Value',0);
  end
  
  set(handles.text_X,'String',num2str(x));
  set(handles.text_Y,'String',num2str(y));
  set(handles.text_Z,'String',num2str(z));
  set(handles.text_PHI2,'String',num2str(phi2));
    
  user_al5dInvKin();
  

function text_X_Callback(hObject, eventdata, handles)
% hObject    handle to text_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_X as text
%        str2double(get(hObject,'String')) returns contents of text_X as a double
global x;
global range_translation;
x = str2double(get(handles.text_X,'String'));
set(handles.slider_X,'Value',x/range_translation);
user_al5dInvKin();

% --- Executes during object creation, after setting all properties.
function text_X_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function text_Y_Callback(hObject, eventdata, handles)
% hObject    handle to text_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_Y as text
%        str2double(get(hObject,'String')) returns contents of text_Y as a double
global y;
global range_translation;
y = str2double(get(handles.text_Y,'String'));
set(handles.slider_Y,'Value',y/range_translation);
user_al5dInvKin();

% --- Executes during object creation, after setting all properties.
function text_Y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function text_Z_Callback(hObject, eventdata, handles)
% hObject    handle to text_Z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_Z as text
%        str2double(get(hObject,'String')) returns contents of text_Z as a double
global z;
global range_translation;
z = str2double(get(handles.text_Z,'String'));
set(handles.slider_Z,'Value',z/range_translation);
user_al5dInvKin();

% --- Executes during object creation, after setting all properties.
function text_Z_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_Z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function text_PHI2_Callback(hObject, eventdata, handles)
% hObject    handle to text_PHI2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_PHI2 as text
%        str2double(get(hObject,'String')) returns contents of text_PHI2 as a double
global phi2;
global range_orientation;
phi2 = str2double(get(handles.text_PHI2,'String'));
set(handles.slider_PHI2,'Value',phi2/range_orientation);
user_al5dInvKin();

% --- Executes during object creation, after setting all properties.
function text_PHI2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_PHI2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
