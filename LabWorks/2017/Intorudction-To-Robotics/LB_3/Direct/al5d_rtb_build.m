function [ al5d ] = al5d_rtb_build()

% Parameters of manipulator AL5D
d1 = 70e-3;
a1 = 146e-3;
a2 = 187e-3;
a3 = 86e-3;

% Create manipulator links based on parameters
L1 = Link('d',d1,'a',0,'alpha',pi/2,'offset',0);
L2 = Link('d',0,'a',a1,'alpha',0,'offset',pi/2);
L3 = Link('d',0,'a',a2,'alpha',0,'offset',pi/2);
L4 = Link('d',0,'a',a3,'alpha',0,'offset',0);

% Create manipulator based on links
al5d = SerialLink([L1,L2,L3,L4],'name','SCARA');

end