function user_al5ddk( )
% Summary of this function goes here
% Detailed explanation goes here
clc 

global q1 q2 q3 q4;
global robot;
global robot_fig;

% Initialize link angles
q = [q1,q2,q3,q4];

% Show manipultor
figure(robot_fig);

robot.plot(q,'delay',0);

%% Calculating pose (direct kinematics)

% Calculate direct kinematics
dk = robot.fkine(q).T
R = dk(1:3,1:3);

% Find cosine of y angle
cosy = sqrt(R(3,3)^2+R(3,2)^2);

% Calculate RPY angles from rotation matrix
% using atan2 as built-in tr2rpy function
% is not work properly
%rpy = tr2rpy(R, 'xyz', 'deg');
x = atan2(R(3,2)/cosy,R(3,3)/cosy)*(180/pi)
y = atan2(-R(3,1),cosy)*(180/pi)
z = atan2(R(2,1)/cosy,R(1,1)/cosy)*(180/pi)

% Calculate Euler Z-Y-Z angles from rotation matrix
eul = tr2eul(R, 'deg')

% Calculate tcp position

end

