function varargout = gui_al5ddk(varargin)
% GUI_AL5DDK MATLAB code for gui_al5ddk.fig
%      GUI_AL5DDK, by itself, creates a new GUI_AL5DDK or raises the existing
%      singleton*.
%
%      H = GUI_AL5DDK returns the handle to a new GUI_AL5DDK or the handle to
%      the existing singleton*.
%
%      GUI_AL5DDK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_AL5DDK.M with the given input arguments.
%
%      GUI_AL5DDK('Property','Value',...) creates a new GUI_AL5DDK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_al5ddk_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_al5ddk_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui_al5ddk

% Last Modified by GUIDE v2.5 17-Aug-2017 09:08:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_al5ddk_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_al5ddk_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before gui_al5ddk is made visible.
function gui_al5ddk_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui_al5ddk (see VARARGIN)

% Choose default command line output for gui_al5ddk
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global robot;
  robot = varargin{1,1};
  
global robot_fig
  robot_fig = figure(); % Slika v katero se izrise kin. struktura robota
  
global range
  range = pi; % Skalirna vrednost s katero se pomnozijo drsniki (drsniki so v obmocju od -1 do 1)
  
global q1 q2 q3 q4;
global q;
  q = varargin{1,2};

  q1 = q(1);
  q2 = q(2);
  q3 = q(3);
  q4 = q(4);

  set(handles.slider_q1,'Value',q1/range);
  set(handles.slider_q2,'Value',q2/range);
  set(handles.slider_q3,'Value',q3/range);
  set(handles.slider_q4,'Value',q4/range);

  set(handles.text_q1,'String',num2str(q1));
  set(handles.text_q2,'String',num2str(q2));
  set(handles.text_q3,'String',num2str(q3));
  set(handles.text_q4,'String',num2str(q4));
  
user_al5ddk();
  
% UIWAIT makes gui_al5ddk wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gui_al5ddk_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider_q1_Callback(hObject, eventdata, handles)
% hObject    handle to slider_q1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global q1;
global range;
q1 = range*get(handles.slider_q1,'Value');
set(handles.text_q1,'String',num2str(q1));
user_al5ddk();

% --- Executes during object creation, after setting all properties.
function slider_q1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_q1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_q2_Callback(hObject, eventdata, handles )
% hObject    handle to slider_q2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global q2;
global range;
q2 = range*get(handles.slider_q2,'Value');
set(handles.text_q2,'String',num2str(q2));
user_al5ddk();

% --- Executes during object creation, after setting all properties.
function slider_q2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_q2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on slider movement.
function slider_q3_Callback(hObject, eventdata, handles)
% hObject    handle to slider_q3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global q3;
global range;
q3 = range*get(handles.slider_q3,'Value');
set(handles.text_q3,'String',num2str(q3));
user_al5ddk();


% --- Executes during object creation, after setting all properties.
function slider_q3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_q3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on slider movement.
function slider_q4_Callback(hObject, eventdata, handles)
% hObject    handle to slider_q4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global q4;
global range;
q4 = range*get(handles.slider_q4,'Value');
set(handles.text_q4,'String',num2str(q4));
user_al5ddk();

% --- Executes during object creation, after setting all properties.
function slider_q4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_q4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function text_q1_Callback(hObject, eventdata, handles)
% hObject    handle to text_q1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_q1 as text
%        str2double(get(hObject,'String')) returns contents of text_q1 as a double
global q1;
global range;
q1 = str2double(get(handles.text_q1,'String'));
set(handles.slider_q1,'Value',q1/range);
user_al5ddk();

% --- Executes during object creation, after setting all properties.
function text_q1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_q1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function text_q2_Callback(hObject, eventdata, handles)
% hObject    handle to text_q2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_q2 as text
%        str2double(get(hObject,'String')) returns contents of text_q2 as a double
global q2;
global range;
q2 = str2double(get(handles.text_q2,'String'));
set(handles.slider_q2,'Value',q2/range);
user_al5ddk();


% --- Executes during object creation, after setting all properties.
function text_q2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_q2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function text_q3_Callback(hObject, eventdata, handles)
% hObject    handle to text_q3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_q3 as text
%        str2double(get(hObject,'String')) returns contents of text_q3 as a double
global q3;
global range;
q3 = str2double(get(handles.text_q3,'String'));
set(handles.slider_q3,'Value',q3/range);
user_al5ddk();


% --- Executes during object creation, after setting all properties.
function text_q3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_q3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function text_q4_Callback(hObject, eventdata, handles)
% hObject    handle to text_q4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_q4 as text
%        str2double(get(hObject,'String')) returns contents of text_q4 as a double
global q4;
global range;
q4 = str2double(get(handles.text_q4,'String'));
set(handles.slider_q4,'Value',q4/range);
user_al5ddk();


% --- Executes during object creation, after setting all properties.
function text_q4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_q4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in button_reset.
function button_reset_Callback(hObject, eventdata, handles)
% hObject    handle to button_reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global q;
global q1 q2 q3 q4;
global range;

  q1 = q(1);
  q2 = q(2);
  q3 = q(3);
  q4 = q(4);

  set(handles.slider_q1,'Value',q1/range);
  set(handles.slider_q2,'Value',q2/range);
  set(handles.slider_q3,'Value',q3/range);
  set(handles.slider_q4,'Value',q4/range);

  set(handles.text_q1,'String',num2str(q1));
  set(handles.text_q2,'String',num2str(q2));
  set(handles.text_q3,'String',num2str(q3));
  set(handles.text_q4,'String',num2str(q4));
 
  user_al5ddk();
  
