clear all
close all
clc

format compact

% Let's assemble the serial kinematic structure
al5d = al5d_rtb_build();

% Initial set of coordinates
q = [ 0 0 0 0 ];

%figure();
%al5d.plot(q);

% Teach function
%ur.teach();

% Startup GUI
gui_al5ddk( al5d, q );