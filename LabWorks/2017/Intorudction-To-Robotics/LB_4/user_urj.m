function user_urj( varargin )
clc 
%nargin

%% Check input arguments
if nargin < 1
  interpolate = 0;
else
  interpolate = varargin{1,1};
end

%% Set global variables
global q1 q2 q3 q4 q5 q6; % Robot joint angles
global robot; % Object for storing robot parameters
global robot_fig;
global q_traj q_traj_idx;
global p; % TCP position for plotting

% Create robot joint angles array
q = [q1,q2,q3,q4,q5,q6];

% Plot robot structure
figure(robot_fig);
[az,el] = view();

%% Draw teach points in Cartesian coordinates
if q_traj_idx > 1
    disp('Saved points');
    q_traj
    
    idx = q_traj_idx - 1;
    dk = robot.fkine(q_traj(:,idx));
    p(:,idx) = dk.t;
    p = p(:,1:1:idx);
    
    % Plot saved points
    plotp(p,'o'); 
else
    p = [0;0;0];
    plotp(p, 'o');
end

%% Show plot if "Interpolate" button is not pressed
if interpolate == 0
    figure(robot_fig);
    robot.plot(q,'delay',0,'view',[az,el]);
else
%% INTERPOLATION if "Interpolate" button is pressed
   qr = q_traj';   
   
   % Write data about trajectory in files
   fidqi = fopen('qi_traj.dat','w+'); % Position vector
   fidqdi = fopen('qdi_traj.dat','w+'); % Velocity vector
   fidqddi = fopen('qddi_traj.dat','w+'); % Acceleration vector
   fidtime = fopen('time.dat','w+'); % Time vector
   
   % Add all trajectory points in one array
   t_sum = [];
   % It serves to record the entire time on the trajectory
   t_last_seq = 0;
   
   % Check if at least there are two points in q_traj
   [m, n] = size(qr);
   if m >= 2
       % For all teached points
       for i = 1:1:(m-1)
           % Set of joints for start point
           q0 = qr(i,:);
           % Set of joints for end point
           qf = qr(i+1,:);

           % Max velocity on trajectory
           qdref = 10*pi;

           % How long to transverse path from q0 to qf
           t_segment(i) = norm(qf - q0)/qdref;

           % Set time step
           dt = 1e-3;
           T = (0:dt:t_segment(i))';
           
           % Interpolate trajectory between points
           [qi,qdi,qddi] = jtraj(q0,qf,T)
           
           % Accumulate all trajectory points
           t_sum = [t_sum, t_last_seq.*ones(size(T')) + T'];
           t_last_seq = max(t_sum);
           
           % Show animation
           robot.plot(qi, 'fps', 20);
           
           % Write data to bin file
           fwrite(fidqi, qi', 'float32');
           fwrite(fidqdi, qdi', 'float32');
           fwrite(fidqddi, qddi', 'float32');
           clear qi qdi qddi;
       end
   
   end
   
   % Close files
   fclose(fidqi);
   fclose(fidqdi);
   fclose(fidqddi);
   
   % Write accumulated time vector
   fwrite(fidtime, t_sum, 'float32');
   fclose(fidtime);
   
   % Write reference joints (qr)
   fidqr = fopen('qr_traj.dat','w+');
   fwrite(fidqr, qr', 'float32');
   fclose(fidqr);
   
end % end of if interpolate == 1
  
end % end of funciton user_urj