clear all
close all
clc

format compact

% Get the robot serial kinematic structure
ur = ur5_rtb_build(); 

% The initial state of the joint angles
q = [ 0 0 0 0 0 0 ]; 

%figure();
%ur.plot(q);

% Draw a 3D model with STL files
%figure();
ur.model3d = 'stl';
ur.plot3d( q, 'path', 'stl', 'view', [-37,30] );

% Teach function
%ur.teach();

% Start GUI 
gui_urj( ur, q );