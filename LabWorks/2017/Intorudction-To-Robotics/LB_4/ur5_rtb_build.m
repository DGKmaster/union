function [ ur ] = ur5_rtb_build(  )
%ur_dirkinBuild Build direct kinematics model with Denavit-Hartenberg method

% DH parameters for link 1
th1off = 0;
d1 = 0.0892;
a1 = 0;
alpha1 = 90*(pi/180);

% DH parameters for link 2
th2off = 90*(pi/180);
d2 = 0;
a2 = 0.425;
alpha2 = 0;

% DH parameters for link 3
th3off = 0;
d3 = 0;
a3 = 0.392;
alpha3 = 0;

% DH parameters for link 4
th4off = -90*(pi/180);
d4 = 0.1093;
a4 = 0;
alpha4 = 90*(pi/180);

% DH parameters for link 5
th5off = 0;
d5 = 0.09475;
a5 = 0;
alpha5 = -90*(pi/180);

% DH parameters for link 6
th6off = 0;
d6 = 0.0825;
a6 = 0;
alpha6 = 180*(pi/180);

% Create link objects based on link parameters
link1 = Link('d',d1,'a',a1,'alpha',alpha1,'offset',th1off);
link2 = Link('d',d2,'a',a2,'alpha',alpha2,'offset',th2off);
link3 = Link('d',d3,'a',a3,'alpha',alpha3,'offset',th3off);
link4 = Link('d',-d4,'a',a4,'alpha',alpha4,'offset',th4off);
link5 = Link('d',-d5,'a',a5,'alpha',alpha5,'offset',th5off);
link6 = Link('d',-d6,'a',a6,'alpha',alpha6,'offset',th6off);

% Create robot object based on links
ur = SerialLink([link1,link2,link3,link4,link5,link6],'name','ur');

end