%% Prepare environment
clear all
close all
clc

%% Set up start and end point
% Set up start and end points for joint trajectory
q0 = [0 ,0 ,0 ,0 , 0,0];
qf = [1, 2, 3, 4, 5, 6];
% Set up start and end points for scalar trajectory
s0 = 0;
sf = 5;
% Set up time vector
t = 0:1e-3:5;

%% Create trajectory
% Joint trajectory
[qi, qid, qidd] = jtraj(q0, qf, t);
% Scalar trajectory
[si, sid, sidd] = lspb(s0, sf, t);

%% Plot joint trajectory
% Position plot
figure(1);
subplot(3,1,1);
plot(t,qi);
ylabel('Interpolated q');
% Velocity plot
subplot(3,1,2);
plot(t,qid);
ylabel('Interpolated qd');
% Acceleration plot
subplot(3,1,3);
plot(t,qidd);
ylabel('Interpolated qdd');

%% Plot scalar trajectory
% Position plot
figure(2);
subplot(3,1,1);
plot(t,si);
ylabel('Interpolated s');
% Velocity plot
subplot(3,1,2);
plot(t,sid);
ylabel('Interpolated sd');
% Acceleration plot
subplot(3,1,3);
plot(t,sidd);
ylabel('Interpolated sdd');