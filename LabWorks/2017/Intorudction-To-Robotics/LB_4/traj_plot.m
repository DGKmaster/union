%clear all
%close all
%clc

%% Get stored information
% Open file with whole poistion trajectory
fid = fopen('qi_traj.dat','r');
qi = fread(fid, [6,inf],'float32');
qi = qi';
fclose(fid);

% Open file with all end points
fid = fopen('qr_traj.dat','r');
qr = fread(fid, [6,inf],'float32');
qr = qr';
fclose(fid);

% Get point quantity
[m,n] = size(qr);

% Assemble the serial kinematic structure
ur = ur5_rtb_build(); 

% Interpolated trajectory in internal coordinates
% convert to trajectory in external coordinates
Xi = ur.fkine(qi);
xi = Xi.tv;

% Points determining the trajectory
Xr = ur.fkine(qr);
xr = Xr.tv;

%% Plot trajectory in 3D
% If it is composed, then the trajectory is displayed 
% in the image where the robot is located
figure(); 
for i=1:m
    hold on;
    plotp(xr(:,i));
    t=text(xr(1,i),xr(2,i),xr(3,i),num2str(i));
    t.Color = 'red';
    t.FontSize = 15;
    t.FontWeight = 'bold';
end
hold on; 
plot2(xi');
grid on;

%% Plot position, velocity and acceleration in time changing
% Get velocity
fid = fopen('qdi_traj.dat','r');
qdi = fread(fid, [6,inf],'float32');
qdi = qdi';
fclose(fid);
% Get acceleration
fid = fopen('qddi_traj.dat','r');
qddi = fread(fid, [6,inf],'float32');
qddi = qddi';
fclose(fid);
% Get time vector
fid = fopen('time.dat','r');
T = fread(fid, [inf],'float32');
fclose(fid);

% Plot position
figure();
subplot(3,1,1);
plot(T,qi);
ylabel('q[rad]');
grid;
% Plot velocity
subplot(3,1,2);
plot(T,qdi);
ylabel('qd [rad/s]');
grid;
% Plot acceleration
subplot(3,1,3);
plot(T,qddi);
ylabel('qdd [rad/s^2]');
xlabel('Time [s]');
grid;

% traj = mstraj(q(2:m,:),0.6*pi,[],q(1,:),0.01,0.5);
% Xi_rvc = ur.fkine(traj);
% xi_rvc = Xi_rvc.tv;
% plot2(xi_rvc');
  