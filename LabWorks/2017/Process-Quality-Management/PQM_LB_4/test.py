from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


class TestSite:
    """
    Simple class for site testing
    """

    def __init__(self):
        """
        Initialising browser
        """
        # Open browser
        self._browser = webdriver.Firefox()
        # Maximize window
        self._browser.maximize_window()
        # Wait for page loading
        self._browser.implicitly_wait(5)

    def open_by_link(self, site_name):
        """
        Open website
        @param site_name: Site name
        """
        self._browser.get(site_name)

    def finish(self):
        """
        Wait and finish test
        """
        time.sleep(2)
        self._browser.quit()


class TestShop(TestSite):
    """
    Extend functionality for Internet shop testing
    """
    _string_first_product = 0
    _string_computer = 0
    _string_search = 0
    found_first_product = False
    found_computer = False
    found_search = False

    def test(self):
        # Delete cookies to do not be influenced by other tests
        self._browser.delete_all_cookies()
        # Open browser
        self.open_by_link('https://arvutiladu.ee/en/')
        time.sleep(2)
        self._test_first_product()
        self._test_computer()
        self._test_search()
        self._check_cart()
        self._empty_cart()

    def _check_cart(self):
        time.sleep(5)
        _elem_first_product = self._browser.find_elements_by_partial_link_text(self._string_first_product)
        _elem_computer = self._browser.find_elements_by_partial_link_text(self._string_computer)
        _elem_search = self._browser.find_elements_by_partial_link_text(self._string_search)
        for item in _elem_first_product:
            if self._string_first_product == item.text:
                self.found_first_product = True
                break
        for item in _elem_computer:
            if self._string_computer == item.text:
                self.found_computer = True
                break
        for item in _elem_search:
            if self._string_search == item.text:
                self.found_search = True
                break
        print('Real ' + self._string_first_product + ' ' + str(self.found_first_product))
        print('Real ' + self._string_computer + ' ' + str(self.found_computer))
        print('Real ' + self._string_search + ' ' + str(self.found_search))

    def _find_name(self):
        """
        Find product name
        @return: Product name
        """
        _elem = self._browser.find_element_by_tag_name('h1')
        return _elem.text

    def _add_to_cart(self):
        """
        Add product on its page to the cart
        """
        _elem = self._browser.find_element_by_id('product-addtocart-button')
        _elem.click()

    def _test_first_product(self):
        """
        Find first product on main page and add it to cart
        """
        # Search first product
        _elem = self._browser.find_element_by_class_name('prolabel-wrapper')
        # Go to its page
        _elem.click()
        time.sleep(2)
        # Find and save its name
        self._string_first_product = self._find_name()
        # Add to cart
        self._add_to_cart()

    def _test_computer(self):
        """
        Find first product in category computers and add it to cart
        """
        # Search computers part
        _elem = self._browser.find_element_by_partial_link_text('Computers')
        _elem.click()
        time.sleep(2)
        _elem = self._browser.find_element_by_class_name('product-primary')
        _elem.click()
        # Find and save its name
        self._string_computer = self._find_name()
        # Add to cart
        self._add_to_cart()

    def _test_search(self):
        """
        Search product by company and add first to cart
        """
        # Search lenovo product
        _elem = self._browser.find_element_by_id('search')
        for i in range(27):
            _elem.send_keys(Keys.BACK_SPACE)
        _elem.send_keys('lenovo' + Keys.ENTER)
        time.sleep(3)
        _elem = self._browser.find_element_by_class_name('product-primary')
        _elem.click()
        # Find and save its name
        self._string_search = self._find_name()
        # Add to cart
        self._add_to_cart()

    def _empty_cart(self):
        """
        Empty shop cart
        """
        _elem = self._browser.find_element_by_id('empty_cart_button')
        _elem.click()


class TestString(TestSite):
    """
    Extend functionality for String testing
    """
    _string_reverse = 'reverse'
    _string_length = 'element'
    _string_count = 'count these words'

    def open_func(self, name):
        """
        Open string functions in main menu
        @param name: Name of function
        """
        # Find hyperlink
        _elem = self._browser.find_element_by_partial_link_text(name)
        _elem.send_keys(Keys.ENTER)

    def input(self, text):
        # Find input field
        _elem = self._browser.find_element_by_name('txtText1')
        # Input string
        _elem.send_keys(text)

    def parse_answer(self, case):
        # Parse answer
        if case == 'reverse':
            _elem = self._browser.find_element_by_name('txtText2')
            return _elem.text
        elif case == 'length':
            _elem = self._browser.find_element_by_id('lblLength')
            _text = _elem.text.replace('Your string is ', '')
            _text = int(_text.replace(' characters long.', ''))
            return _text
        elif case == 'count':
            _elem = self._browser.find_element_by_id('lblLength')
            _text = _elem.text.replace('Your text is ', '')
            _text = int(_text.replace(' word(s) long.', ''))
            return _text

    def test_reverse(self):
        self.open_by_link('http://string-functions.com/')
        self.open_func('Reverse A String')
        self.input(self._string_reverse)
        self.push_button('cmdReverse')
        _answer = self.parse_answer('reverse')
        return self._string_reverse[::-1] == _answer

    def test_length(self):
        self.open_by_link('http://string-functions.com/')
        self.open_func('Calculate String Length')
        self.input(self._string_length)
        self.push_button('cmdCalculate')
        _answer = self.parse_answer('length')
        return len(self._string_length) == _answer

    def test_count(self):
        self.open_by_link('http://string-functions.com/')
        self.open_func('Word Count Tool')
        self.input(self._string_count)
        self.push_button('cmdCalculate')
        _answer = self.parse_answer('count')
        return len(self._string_count.split()) == _answer

    def push_button(self, button_name):
        _elem = self._browser.find_element_by_name(button_name)
        _elem.send_keys(Keys.ENTER)
        time.sleep(1)
