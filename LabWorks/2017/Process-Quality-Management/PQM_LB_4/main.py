import test

# String functions testing
test_string = test.TestString()
if test_string.test_reverse():
    print('Test String Reverse is successful')
else:
    print('Test String Reverse is NOT successful')
if test_string.test_length():
    print('Test String Length is successful')
else:
    print('Test String Length is NOT successful')
if test_string.test_count():
    print('Test String Count is successful')
else:
    print('Test String Count is NOT successful')
test_string.finish()

# Internet shop testing
test_shop = test.TestShop()
test_shop.test()
test_shop.finish()
