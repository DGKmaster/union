# Process And Quality Management (PQM)

Java code for Process and Quality Management. ATM simulator realisation and unit tests.

* **PQM_LB_2** - Java ATM and Card classes for ATM work simulating.
* **PQM_LB_3** - Unit tests for Lab 2.
* **PQM_LB_4** - Selenium IDE usage for web-sites testing.
* **PQM_LB_5** - Selenium IDE usage for estudij web-site testing.
