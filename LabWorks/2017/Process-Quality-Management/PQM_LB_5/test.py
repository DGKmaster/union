from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import sys
from bs4 import BeautifulSoup
import datetime
import random


class TestSite:
    """
    Class for site testing
    """

    def __init__(self):
        """
        Initialising browser
        """
        self._password = input("Enter password: ")
        # Open browser
        self._browser = webdriver.Firefox()
        # Maximize window
        self._browser.maximize_window()
        # Wait for page loading
        self._browser.implicitly_wait(5)

    def open_by_link(self, site_name):
        """
        Open website
        @param site_name: Site name
        """
        self._browser.get(site_name)

    def finish(self):
        """
        Wait and finish test
        """
        time.sleep(2)
        self._browser.quit()

    _username = 'artem.volkov@student.um.si'
    _password = ''
    _check_test_1 = True
    _check_test_2 = True
    _check_test_3 = True
    _result_list = []
    _topics_list = []
    _date_list = []
    _submission_list = []

    def test(self):
        # Delete cookies to do not be influenced by other tests
        self._browser.delete_all_cookies()
        # Open browser
        self.open_by_link('https://estudij.um.si/?lang=en')
        time.sleep(2)
        self._login()
        self._test_1()
        self._test_2()
        self._test_3()
        # Print result
        print('Test 1: ' + str(self._check_test_1))
        print('Test 2: ' + str(self._check_test_2))
        print('Test 3: ' + str(self._check_test_3))
        # self.finish()

    # self._browser.find_element_by_partial_link_text('')
    # _elem = self._browser.find_element_by_tag_name('')
    # _elem = self._browser.find_element_by_id('')
    # _elem = self._browser.find_element_by_class_name('')
    # _elem.send_keys(Keys.BACK_SPACE)
    # _elem.click()
    # _elem = self._browser.find_element_by_name('')
    # _elem.text.replace('', '')
    # time.sleep(2)

    def _login(self):
        # Login
        self._browser.find_element_by_partial_link_text('Log in').click()
        time.sleep(2)
        self._browser.find_element_by_id('username').send_keys(self._username)
        self._browser.find_element_by_id('password').send_keys(self._password)
        self._browser.find_element_by_id('loginbtn').click()
        # Find course
        self._browser.find_element_by_partial_link_text('OBVLADOVANJE PROCESOV IN KAKOVOSTI').click()
        time.sleep(2)

    def _test_1(self):
        """
        Verify that all 4 first tasks is sent
        """
        # Find present results
        self._browser.find_element_by_partial_link_text('Assignments').click()
        time.sleep(2)
        # Parse page
        _html = self._browser.page_source
        _parsed_html = BeautifulSoup(_html, 'lxml')
        self._topics_list = _parsed_html.body.find_all('td', attrs={'class': 'cell c0'})
        self._submission_list = _parsed_html.body.find_all('td', attrs={'class': 'cell c3'})

        # Inspect each row
        for i in range(len(self._topics_list)):
            if self._submission_list[i].text == 'Submitted for grading':
                self._result_list.append('Task ' + str(i + 1) + ' is sent')
            elif self._submission_list[i].text == 'No submission':
                self._result_list.append('Task ' + str(i + 1) + ' is NOT posted')
                if i < 4:
                    self._check_test_1 = False
            else:
                self._result_list.append('Task ' + str(i + 1) + ' status is unknown')
            print(self._result_list[i])

        if self._check_test_1:
            print('Test 1 is passed')
        else:
            print('Test 1 is failed')

    def _test_2(self):
        """
        Verify that sent first four tasks are sent in time
        """
        print('')
        # Inspect each row
        for i in range(4):
            # range(len(self._topics_list))
            if 'is sent' in self._result_list[i]:
                if i == 0:
                    self._browser.find_element_by_link_text('Naloga 1: Statična analiza izvorne kode [utež: 1]').click()
                if i == 1:
                    self._browser.find_element_by_link_text('Naloga 2: Pisanje javadoc dokumentacije [utež: 2]').click()
                if i == 2:
                    self._browser.find_element_by_link_text('Naloga 3: Testiranje enot [utež: 2]').click()
                if i == 3:
                    self._browser.find_element_by_link_text(
                        'Naloga 4: Testiranje spletnih strani (Selenium IDE) [utež: 1]').click()
                if i == 4:
                    self._browser.find_element_by_link_text(
                        'Naloga 5: Testiranje spletnih strani (Selenium WebDriver) [utež: 2]').click()
                time.sleep(2)
                # Parse page
                _html = self._browser.page_source
                _parsed_html = BeautifulSoup(_html, 'lxml')
                self._date_list.append(
                    _parsed_html.body.find('td', attrs={'class': 'latesubmission cell c1 lastcol'}).text)
                self._browser.back()
                time.sleep(2)
                if self._date_list[i].find('late'):
                    self._check_test_2 = False
                self._result_list[i] += ', ' + self._date_list[i]
                print(self._result_list[i])
        if self._check_test_2:
            print('Test 2 is passed')
        else:
            print('Test 2 is failed')

    def _test_3(self):
        """
        Test forum messaging
        """
        # Find course
        self._browser.find_element_by_partial_link_text('OBVLADOVANJE PROCESOV IN KAKOVOSTI').click()
        time.sleep(2)
        self._browser.find_element_by_partial_link_text('Testni forum').click()
        time.sleep(2)
        self._browser.find_element_by_partial_link_text('Task 5').click()
        time.sleep(2)
        self._browser.find_element_by_partial_link_text('Reply').click()
        time.sleep(3)
        _now = datetime.datetime.now()
        _out_string = str(_now.hour) + ':' + str(_now.minute) + ':' + str(_now.second) + ', ' + str(
            _now.day) + '.' + str(_now.month) + '.' + str(_now.year) + ' | ' + str(random.random())
        self._browser.find_element_by_id('id_message_ifr').send_keys(_out_string)
        time.sleep(3)
        self._browser.find_element_by_name('submitbutton').click()
        # Parse page
        if _out_string in self._browser.page_source:
            self._check_test_3 = True
            print('Test 3 is passed')
        else:
            self._check_test_3 = False
            print('Test 3 is failed')
