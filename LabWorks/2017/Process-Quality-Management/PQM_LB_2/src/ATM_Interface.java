public interface ATM_Interface {
    /**
     * Add ATM funds
     * @param amount How much money is put in ATM
     * @throws Exception If amount cannot be divided by 10
     * @throws Exception If amount is bigger than 4000 euros
     */
    void putMoneyInATM(double amount) throws Exception;

    /**
     * Read card information
     * @param card Inserted card
     * @throws Exception If card does not have client name
     * @throws Exception If card does not have PIN
     */
    void inputCard(Card card) throws Exception;

    /**
     * Check PIN information
     * @param PIN Check input PIN with card PIN
     * @return True if input PIN is correct, false otherwise
     * @throws Exception If PIN is incorrect
     * @throws Exception If three attempts are failed card will be confiscated
     */
    boolean inputPIN(String PIN) throws Exception;

    /**
     * Print card balance
     * @return Card balance
     * @throws Exception If no card is in ATM
     * @throws Exception If PIN is not entered
     */
    double getCardStatement() throws Exception;

    /**
     * Give card money
     * @param amount How much money will be given
     * @throws Exception If no card is in ATM
     * @throws Exception If PIN is not entered
     * @throws Exception If too much money is requested in one time
     * @throws Exception If amount cannot be divided by 10
     * @throws Exception If not enough money is inside ATM
     * @throws Exception If not enough money on card
     */
    void getCardMoney(double amount) throws Exception;

    /**
     * Delete card information and make attributes to initial states
     * @throws Exception If no card is in ATM
     * @throws Exception If card is confiscated due to three wrong input PIN attempts
     */
    void outputCard() throws Exception;
}
