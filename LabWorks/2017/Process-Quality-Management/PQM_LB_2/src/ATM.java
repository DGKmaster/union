// Class to work with bank cards
public class ATM implements ATM_Interface {
    // Money in ATM
    private double money;
    // Read entered card information and store it
    private Card inCard;
    // PIN is verified
    private boolean checkPIN;
    // Card is inserted
    private boolean insertedCard;
    // Only 3 attempts
    private int attemptsPIN;
    // If 3 attempts is failed
    private boolean eatenCard;

    /**
     * Default constructor
     * Initializes all attributes to zero (false)
     */
    public ATM() {
        this.money = 0;
        this.inCard = new Card();
        this.checkPIN = false;
        this.insertedCard = false;
        this.attemptsPIN = 0;
        this.eatenCard = false;
    }

    /**
     * Add ATM funds
     * @param amount How much money is put in ATM
     * @throws Exception If amount cannot be divided by 10
     * @throws Exception If amount is bigger than 4000 euros
     */
    @Override
    public void putMoneyInATM(double amount) throws Exception {
        try {
            if (amount % 10 != 0) {
                throw new Exception("Only 10 euro banknote is accepted");
            }
            if (amount > 4000) {
                throw new Exception("Too much money at once");
            }
            this.money += amount;
        } catch (Exception e) {
            System.err.println(e);
        }

    }


    /**
     * Read card information
     * @param card Inserted card
     * @throws Exception If card does not have client name
     * @throws Exception If card does not have PIN
     */
    @Override
    public void inputCard(Card card) throws Exception {
        try {
            // Copy information
            this.inCard = card;

            // Verify information about client
            if (this.inCard.getClientName().isEmpty()) {
                throw new Exception("Card is not initialised");
            }
            // Verify information about PIN
            if (this.inCard.getPIN().isEmpty()) {
                throw new Exception("PIN is not initialised");
            }
            // Card is inserted
            this.insertedCard = true;
        } catch (Exception e) {
            System.err.println(e);
        }

    }

    /**
     * Check PIN information
     * @param PIN Check input PIN with card PIN
     * @return True if input PIN is correct, false otherwise
     * @throws Exception If PIN is incorrect
     * @throws Exception If three attempts are failed card will be confiscated
     */
    @Override
    public boolean inputPIN(String PIN) throws Exception {
        try {
            if (!this.insertedCard) {
                throw new Exception("There is no bank card");
            }
            this.checkPIN = this.inCard.getPIN().equals(PIN);
            if (!this.checkPIN) {
                System.err.println("Wrong attempt");
            }
            ++this.attemptsPIN;
            if (this.attemptsPIN > 3) {
                this.eatenCard = true;
                throw new Exception("Wrong three attempts");
            }
            return this.checkPIN;
        } catch (Exception e) {
            System.err.println(e);
            return this.checkPIN;
        }
    }

    /**
     * Print card balance
     * @return Card balance
     * @throws Exception If no card is in ATM
     * @throws Exception If PIN is not entered
     */
    @Override
    public double getCardStatement() throws Exception {
        try {
            if (!this.insertedCard) {
                throw new Exception("There is no bank card");
            }
            if (!this.checkPIN) {
                throw new Exception("PIN is not entered");
            }
            return inCard.getBalance();
        } catch (Exception e) {
            System.err.println(e);
            return 0;
        }
    }

    /**
     * Give card money
     * @param amount How much money will be given
     * @throws Exception If no card is in ATM
     * @throws Exception If PIN is not entered
     * @throws Exception If too much money is requested in one time
     * @throws Exception If amount cannot be divided by 10
     * @throws Exception If not enough money is inside ATM
     * @throws Exception If not enough money on card
     */
    @Override
    public void getCardMoney(double amount) throws Exception {
        try {
            if (!this.insertedCard) {
                throw new Exception("There is no bank card");
            }
            if (!this.checkPIN) {
                throw new Exception("PIN is not entered");
            }
            if (amount > 100) {
                throw new Exception("Too much for one time");
            }
            if (amount % 10 != 0) {
                throw new Exception("Only 10 euro banknote");
            }
            if (amount > this.money) {
                throw new Exception("Not enough money in ATM");
            }
            if (amount > (this.inCard.getBalance() + this.inCard.getLimit())) {
                throw new Exception("Not enough money on card");
            }
            this.money -= amount;
            this.inCard.setBalance(this.inCard.getBalance() - amount);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    //

    /**
     * Delete card information and make attributes to initial states
     * @throws Exception If no card is in ATM
     * @throws Exception If card is confiscated due to three wrong input PIN attempts
     */
    @Override
    public void outputCard() throws Exception {
        try {
            if (!insertedCard) {
                throw new Exception("There is no bank card");
            }
            if (eatenCard) {
                throw new Exception("Your card is confiscated");
            }

            this.inCard = new Card();
            this.insertedCard = false;
            this.eatenCard = false;
            this.attemptsPIN = 0;
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}