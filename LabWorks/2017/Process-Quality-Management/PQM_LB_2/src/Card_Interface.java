public interface Card_Interface {
    /**
     * Set client name as string
     * @param name Client name
     */
    void setClientName(String name);
    /**
     * Give client name
     * @return Client name
     */
    String getClientName();

    /**
     * Set card PIN
     * @param PIN Change card PIN to argument and must be 4 characters
     * @throws Exception If PIN length is different than 4 characters
     */
    void setPIN(String PIN) throws Exception;
    /**
     * Get card PIN
     * @return Card PIN
     */
    String getPIN();

    /**
     * Set card balance
     * @param balance Change card balance as input
     */
    void setBalance(double balance);
    /**
     * Give card balance
     * @return Card balance
     */
    double getBalance();

    /**
     * Set card limit
     * @param limit Change card limit as input value
     * @throws Exception If limit is negative
     */
    void setLimit(double limit) throws Exception;
    /**
     * Get card limit
     * @return Card limit
     */
    double getLimit();
}
