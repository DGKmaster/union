import org.junit.*;

import java.io.*;

import static org.junit.Assert.fail;

public class Card_Test {
    /**
     * Set client name and verify it. If it is different test is failed.
     */
    @Test
    public void setGetClientName_Test_Once() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetClientName_Test_Once.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetClientName_Test_Once.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setClientName("John");

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If name is different
            if (!myCard.getClientName().equals("John")) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Set client name and verify it twice. If it is different test is failed.
     */
    @Test
    public void setGetClientName_Test_Twice() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetClientName_Test_Once.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetClientName_Test_Once.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setClientName("John");
            myCard.setClientName("Bob");

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If name is different
            if (!myCard.getClientName().equals("Bob")) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Set card PIN and verify it. If it is different test is failed.
     */
    @Test
    public void setGetPIN_Test_Once() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetPIN_Test_Once.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetPIN_Test_Once.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setPIN("1234");

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If name is different
            if (!myCard.getPIN().equals("1234")) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Set card PIN and verify it twice. If it is different test is failed.
     */
    @Test
    public void setGetPIN_Test_Twice() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetPIN_Test_Twice.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetPIN_Test_Twice.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setPIN("1234");
            myCard.setPIN("1235");

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If name is different
            if (!myCard.getPIN().equals("1235")) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Set incorrect card PIN which is less than 4 characters and verify it. If no exception is thrown test is failed.
     */
    @Test
    public void setGetPIN_Test_Less_PIN_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetPIN_Test_Less_PIN_Exception.txt");
            System.setOut(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetPIN_Test_Less_PIN_Exception.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setPIN("123");

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Set incorrect card PIN which is more than 4 characters and verify it. If no exception is thrown test is failed.
     */
    @Test
    public void setGetPIN_Test_More_PIN_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetPIN_Test_More_PIN_Exception.txt");
            System.setOut(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetPIN_Test_More_PIN_Exception.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setPIN("123");

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Set card balance and verify it. If it is different test is failed.
     */
    @Test
    public void setGetBalance_Test_Once() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetBalance_Test_Once.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetBalance_Test_Once.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setBalance(300);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If balance is different
            if (!(myCard.getBalance() == 300)) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Set card balance and verify it twice. If it is different test is failed.
     */
    @Test
    public void setGetBalance_Test_Twice() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetBalance_Test_Twice.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetBalance_Test_Twice.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setBalance(300);
            myCard.setBalance(200);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If balance is different
            if (!(myCard.getBalance() == 200)) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Set card limit and verify it. If it is different test is failed.
     */
    @Test
    public void setGetLimit_Test_Once() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetLimit_Test_Once.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetLimit_Test_Once.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setLimit(300);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If balance is different
            if (!(myCard.getLimit() == 300)) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Set card limit and verify it twice. If it is different test is failed.
     */
    @Test
    public void setGetLimit_Test_Twice() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetLimit_Test_Twice.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetLimit_Test_Twice.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setLimit(300);
            myCard.setLimit(100);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If balance is different
            if (!(myCard.getLimit() == 100)) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Set card limit to zero and verify it. If it is different test is failed.
     */
    @Test
    public void setGetLimit_Test_To_Zero() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetLimit_Test_To_Zero.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetLimit_Test_To_Zero.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setLimit(0);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If balance is different
            if (!(myCard.getLimit() == 0)) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Set card limit to below zero. If no exception is thrown test is failed.
     */
    @Test
    public void setGetLimit_Test_Below_Zero_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("setGetLimit_Test_Below_Zero_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("setGetLimit_Test_Below_Zero_Exception.txt"));

            // Card initialisation
            Card myCard = new Card();
            myCard.setLimit(-10);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}