import org.junit.*;
import java.io.*;
import static org.junit.Assert.fail;

public class ATM_Test {

    /**
     * Test put money in ATM. Sum can be divided by 10 and less 4000.
     * If no exception is thrown test is passed.
     */
    @Test
    public void putMoneyInATM_Test_Lower_Money() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("putMoneyInATM_Test_Lower_Money.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("putMoneyInATM_Test_Lower_Money.txt"));

            // Test behavior
            ATM myATM = new ATM();
            myATM.putMoneyInATM(100);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Test behaviour if sum can not be divided by 10.
     * If no exception is thrown test is failed.
     */
    @Test
    public void putMoneyInATM_Test_Not_Divide_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("putMoneyInATM_Test_Not_Divide_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("putMoneyInATM_Test_Not_Divide_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            myATM.putMoneyInATM(110);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine != null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Test behaviour if sum is bigger than 4000.
     * If no exception is thrown test is failed.
     */
    @Test
    public void putMoneyInATM_Test_Upper_Money_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("putMoneyInATM_Test_Upper_Money_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("putMoneyInATM_Test_Upper_Money_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            myATM.putMoneyInATM(4100);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Test usual behaviour. If all card parameters is set test is passed.
     */
    @Test
    public void inputCard_Test_All_Parameters_Set() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("inputCard_Test_All_Parameters_Set.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("inputCard_Test_All_Parameters_Set.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            myATM.inputCard(myCard);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Client name is not set. If no exception is thrown test is failed.
     */
    @Test
    public void inputCard_Test_No_Client_Name_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("inputCard_Test_No_Client_Name_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("inputCard_Test_No_Client_Name_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            //myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            myATM.inputCard(myCard);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * PIN is not set. If no exception is thrown test is failed.
     */
    @Test
    public void inputCard_Test_No_PIN_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("inputCard_Test_No_PIN_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("inputCard_Test_No_PIN_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            myCard.setClientName("John");
            //myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            myATM.inputCard(myCard);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Input correct PIN. If exception is thrown test is failed.
     * If correct input PIN is unrecognised test is failed.
     */
    @Test
    public void inputPIN_Test_First_Correct_PIN() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("inputPIN_Test_First_Correct_PIN.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("inputPIN_Test_First_Correct_PIN.txt"));

            // Test behavior
            // ATM initialisation
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);

            myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            boolean test_PIN = myATM.inputPIN("1234");

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If correct input PIN is unrecognised
            if (!test_PIN) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Input incorrect PIN. If no exception is thrown test is failed.
     * If incorrect input PIN is recognised test is failed.
     */
    @Test
    public void inputPIN_Test_First_Incorrect_PIN_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("inputPIN_Test_First_Incorrect_PIN_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("inputPIN_Test_First_Incorrect_PIN_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            myATM.inputCard(myCard);
            boolean test_PIN = myATM.inputPIN("1235");

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
            // If incorrect input PIN is recognised
            if (test_PIN) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Input incorrect PIN on second attempt. If exception is thrown test is failed.
     * If correct input PIN is unrecognised test is failed.
     */
    @Test
    public void inputPIN_Test_Second_Correct_PIN_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("inputPIN_Test_Second_Correct_PIN_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("inputPIN_Test_Second_Correct_PIN_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            myATM.inputCard(myCard);
            myATM.inputPIN("1235");
            boolean test_PIN = myATM.inputPIN("1234");

            // Read and close stream
            String currentLine = reader.readLine();
            currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If correct input PIN is unrecognised
            if (!test_PIN) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Input incorrect PIN twice. If no exception is thrown test is failed.
     * If incorrect input PIN is recognised test is failed.
     */
    @Test
    public void inputPIN_Test_Second_Incorrect_PIN_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("inputPIN_Test_Second_Incorrect_PIN_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("inputPIN_Test_Second_Incorrect_PIN_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            myATM.inputCard(myCard);
            myATM.inputPIN("1235");

            boolean test_PIN = myATM.inputPIN("1235");
            // Read and close stream
            String currentLine = reader.readLine();
            currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
            // If incorrect input PIN is recognised
            if (test_PIN) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Input correct PIN on third attempt. If exception is thrown test is failed.
     * If correct input PIN is unrecognised test is failed.
     */
    @Test
    public void inputPIN_Test_Third_Correct_PIN_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("inputPIN_Test_Third_Correct_PIN_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("inputPIN_Test_Third_Correct_PIN_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            myATM.inputCard(myCard);
            myATM.inputPIN("1235");
            myATM.inputPIN("1235");
            boolean test_PIN = myATM.inputPIN("1234");

            // Read and close stream
            String currentLine = reader.readLine();
            currentLine = reader.readLine();
            currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
            // If correct input PIN is unrecognised
            if (!test_PIN) {
                fail();
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Input incorrect PIN three times. If no exception is thrown test is failed.
     * If incorrect input PIN is recognised test is failed.
     */
    @Test
    public void inputPIN_Test_Third_Incorrect_PIN_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("inputPIN_Test_Third_Incorrect_PIN_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("inputPIN_Test_Third_Incorrect_PIN_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            myATM.inputCard(myCard);
            myATM.inputPIN("1235");
            myATM.inputPIN("1235");
            boolean test_PIN = myATM.inputPIN("1235");
            // Read and close stream
            String currentLine = reader.readLine();
            currentLine = reader.readLine();
            currentLine = reader.readLine();
            currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
            // If incorrect input PIN is recognised
            if (test_PIN) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Test usual behaviour. If all statement balance is same as card balance
     * test is passed.
     */
    @Test
    public void getCardStatement_Test_Check_Balance() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardStatement_Test_Check_Balance.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardStatement_Test_Check_Balance.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            double balanceATM = myATM.getCardStatement();
            double balanceCard = myCard.getBalance();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If balance is different
            if (balanceCard != balanceATM) {
                fail();
            }
            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * No card in ATM. If no exception is thrown test is failed.
     */
    @Test
    public void getCardStatement_No_Card_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardStatement_No_Card_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardStatement_No_Card_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            //myATM.inputCard(myCard);
            //myATM.inputPIN("1234");
            myATM.getCardStatement();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * No PIN entered. If no exception is thrown test is failed.
     */
    @Test
    public void getCardStatement_No_PIN_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardStatement_No_PIN_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardStatement_No_PIN_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            myATM.inputCard(myCard);
            //myATM.inputPIN("1234");
            myATM.getCardStatement();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Get money without take overdraft. If exception is thrown test is failed.
     */
    @Test
    public void getCardMoney_Test_Get_Part_Money_Without_Limit() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardMoney_Test_Get_Part_Money_Without_Limit.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardMoney_Test_Get_Part_Money_Without_Limit.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(300);
            myCard.setLimit(100);
            myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            myATM.getCardMoney(50);
            double balanceATM = myATM.getCardStatement();
            double balanceCard = myCard.getBalance();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If balance is different
            if (balanceCard != balanceATM) {
                fail();
            }

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Get money with take overdraft. If exception is thrown test is failed.
     */
    @Test
    public void getCardMoney_Test_Get_All_Money_Without_Limit() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardMoney_Test_Get_All_Money_Without_Limit.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardMoney_Test_Get_All_Money_Without_Limit.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(20);
            myCard.setLimit(30);
            myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            myATM.getCardMoney(20);
            double balanceATM = myATM.getCardStatement();
            double balanceCard = myCard.getBalance();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If balance is different
            if (balanceCard != balanceATM) {
                fail();
            }

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Get money with take overdraft. If exception is thrown test is failed.
     */
    @Test
    public void getCardMoney_Test_Get_All_Money_With_Part_Limit() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardMoney_Test_Get_All_Money_With_Part_Limit.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardMoney_Test_Get_All_Money_With_Part_Limit.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(20);
            myCard.setLimit(30);
            myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            myATM.getCardMoney(40);
            double balanceATM = myATM.getCardStatement();
            double balanceCard = myCard.getBalance();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If balance is different
            if (balanceCard != balanceATM) {
                fail();
            }

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Get money with take overdraft. If exception is thrown test is failed.
     */
    @Test
    public void getCardMoney_Test_Get_All_Money_With_All_Limit() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardMoney_Test_Get_All_Money_With_All_Limit.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardMoney_Test_Get_All_Money_With_All_Limit.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(20);
            myCard.setLimit(30);
            myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            myATM.getCardMoney(50);
            double balanceATM = myATM.getCardStatement();
            double balanceCard = myCard.getBalance();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If balance is different
            if (balanceCard != balanceATM) {
                fail();
            }

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * No Card is entered. If no exception is thrown test is failed.
     */
    @Test
    public void getCardMoney_Test_No_Card_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardMoney_Test_No_Card_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardMoney_Test_No_Card_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(20);
            myCard.setLimit(30);
            //myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            myATM.getCardMoney(50);
            //double balanceATM = myATM.getCardStatement();
            //double balanceCard = myCard.getBalance();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * No PIN is entered. If no exception is thrown test is failed.
     */
    @Test
    public void getCardMoney_Test_No_PIN_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardMoney_Test_No_PIN_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardMoney_Test_No_PIN_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(20);
            myCard.setLimit(30);
            myATM.inputCard(myCard);
            //myATM.inputPIN("1234");
            myATM.getCardMoney(50);
            //double balanceATM = myATM.getCardStatement();
            //double balanceCard = myCard.getBalance();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Too much money is requested to get. If no exception is thrown test is failed.
     */
    @Test
    public void getCardMoney_Test_Request_Too_Much_Money_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardMoney_Test_Request_Too_Much_Money_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardMoney_Test_Request_Too_Much_Money_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(200);
            myCard.setLimit(30);
            myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            myATM.getCardMoney(150);
            //double balanceATM = myATM.getCardStatement();
            //double balanceCard = myCard.getBalance();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Sum cannot be divided by 10. If no exception is thrown test is failed.
     */
    @Test
    public void getCardMoney_Test_Not_Divide_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardMoney_Test_Not_Divide_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardMoney_Test_Not_Divide_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(200);
            myCard.setLimit(30);
            myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            myATM.getCardMoney(15);
            //double balanceATM = myATM.getCardStatement();
            //double balanceCard = myCard.getBalance();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Not enough money in ATM. If no exception is thrown test is failed.
     */
    @Test
    public void getCardMoney_Test_Not_Enough_ATM_Money_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardMoney_Test_Not_Enough_ATM_Money_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardMoney_Test_Not_Enough_ATM_Money_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(50);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(200);
            myCard.setLimit(30);
            myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            myATM.getCardMoney(70);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Not enough money on card. If no exception is thrown test is failed.
     */
    @Test
    public void getCardMoney_Test_Not_Enough_Card_Money_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("getCardMoney_Test_Not_Enough_Card_Money_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("getCardMoney_Test_Not_Enough_Card_Money_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(40);
            myCard.setLimit(30);
            myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            myATM.getCardMoney(90);

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Output card if everything is ok. If exception is thrown test is failed.
     */
    @Test
    public void outputCard_Test_Normal() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("outputCard_Test_Normal.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("outputCard_Test_Normal.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(20);
            myCard.setLimit(30);
            myATM.inputCard(myCard);
            myATM.inputPIN("1234");
            myATM.getCardMoney(40);
            myATM.outputCard();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If exception is thrown
            if (currentLine != null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * No card is entered. If no exception is thrown test is failed.
     */
    @Test
    public void outputCard_Test_No_Card_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("outputCard_Test_No_Card_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("outputCard_Test_No_Card_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(40);
            myCard.setLimit(30);
            //myATM.inputCard(myCard);
            //myATM.inputPIN("1234");
            myATM.outputCard();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Three wrong PIN input attempts. If no exception is thrown test is failed.
     */
    @Test
    public void outputCard_Test_Eaten_Card_Exception() {
        try {
            // Prepare error stream
            PrintStream printStream = new PrintStream("outputCard_Test_Eaten_Card_Exception.txt");
            System.setErr(printStream);
            BufferedReader reader = new BufferedReader(new FileReader("outputCard_Test_Eaten_Card_Exception.txt"));

            // Test behavior
            ATM myATM = new ATM();
            Card myCard = new Card();
            myATM.putMoneyInATM(400);
            // Card initialisation
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(40);
            myCard.setLimit(30);
            myATM.inputCard(myCard);
            myATM.inputPIN("1235");
            myATM.inputPIN("1235");
            myATM.inputPIN("1235");
            myATM.outputCard();

            // Read and close stream
            String currentLine = reader.readLine();
            reader.close();

            // If no exception is thrown
            if (currentLine == null) {
                fail();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}