public class Main {
    public static void main(String args[]) {
        try {
            // Card initialisation
            Card myCard = new Card();
            myCard.setClientName("John");
            myCard.setPIN("1234");
            myCard.setBalance(150);
            myCard.setLimit(100);

            // ATM initialisation
            ATM myATM = new ATM();
            myATM.putMoneyInATM(300);

            // Testing
            myATM.inputCard(myCard);
            System.out.println("Check PIN: " + myATM.inputPIN("1234"));
            System.out.println("Balance: " + myATM.getCardStatement());
            myATM.getCardMoney(100);
            System.out.println("Balance: " + myATM.getCardStatement());
            myATM.getCardMoney(100);
            System.out.println("Balance: " + myATM.getCardStatement());
            myATM.outputCard();

            // Another card initialisation
            Card myNewCard = new Card();
            myNewCard.setClientName("Brad");
            myNewCard.setPIN("1237");
            myNewCard.setBalance(50);
            myNewCard.setLimit(10);

            // Testing
            myATM.inputCard(myNewCard);
            System.out.println("Check PIN: " + myATM.inputPIN("1237"));
            System.out.println("Balance: " + myATM.getCardStatement());
            myATM.getCardMoney(90);
            System.out.println("Balance: " + myATM.getCardStatement());
            myATM.getCardMoney(10);
            System.out.println("Balance: " + myATM.getCardStatement());
            myATM.outputCard();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}