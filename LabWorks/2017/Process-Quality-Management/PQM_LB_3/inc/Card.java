public class Card implements Card_Interface {
    // Money on card
    private double balance;
    // Card PIN
    private String PIN;
    // Money below zero balance that client can get
    private double limit;
    // Client name
    private String name;

    /**
     * Default constructor
     */
    public Card() {
        this.balance = 0;
        this.PIN = null;
        this.limit = 0;
        this.name = null;
    }

    /**
     * Set client name as string
     * @param name Client name
     */
    @Override
    public void setClientName(String name) {
        this.name = name;
    }

    /**
     * Give client name
     * @return Client name
     */
    @Override
    public String getClientName() {
        return this.name;
    }

    /**
     * Set card PIN
     * @param PIN Change card PIN to argument and must be 4 characters
     * @throws Exception If PIN length is different than 4 characters
     */
    @Override
    public void setPIN(String PIN) throws Exception {
        try {
            this.PIN = PIN;
            if (this.PIN.length() != 4) {
                throw new Exception("PIN length must be 4 characters");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Get card PIN
     * @return Card PIN
     */
    @Override
    public String getPIN() {
        return this.PIN;
    }

    /**
     * Set card balance
     * @param balance Change card balance as input
     */
    @Override
    public void setBalance(double balance) {
        this.balance = balance;
    }

    /**
     * Give card balance
     * @return Card balance
     */
    @Override
    public double getBalance() {
        return this.balance;
    }

    /**
     * Set card limit
     * @param limit Change card limit as input value
     * @throws Exception If limit is negative
     */
    @Override
    public void setLimit(double limit) throws Exception {
        try {
            if (limit < 0) {
                throw new Exception("Limit must be positive");
            }
            this.limit = limit;
        } catch (Exception e) {
            System.err.println(e);
        }

    }

    /**
     * Get card limit
     * @return Card limit
     */
    @Override
    public double getLimit() {
        return this.limit;
    }
}
