%% Robot parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Link mass
m_1 = 5;
m_2 = 3;

%% Link length
l_1 = 1.5;
l_2 = 2;

%% Link center mass length
l_c_1 = 0.75;
l_c_2 = 1;

%% Inertia moment
I_1 = 0.7;
I_2 = 0.5;

%% Friction coefficients
k_fric_1 = 0.06;
k_fric_2 = 0.03;

%% Gravity
g_0 = 9.81;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Euler Lagrange matrices coefficients
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ro_1 = m_1*l_c_1^2 + m_2*l_1^2 + 2*m_2*l_1*l_c_2^2 + I_1;
ro_2 = m_2*l_c_2^2 + I_2;
ro_3 = m_2*l_1*l_c_2;
ro_4 = (m_1*l_c_1 + m_2*l_1)*g_0;
ro_5 = m_2*l_c_2*g_0;
ro_6 = m_2*l_c_2;
ro_7 = k_fric_1;
ro_8 = k_fric_2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Observer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
H_p = [10 5; -2 2];
H_v = [3 1; -2 2];
H_s = [5 -2; -2 1];

k = 1000;
k_1 = 10;
k_2 = 10;
k_3 = 10;
k_4 = 10;
K = [k_1 0 k_2 0; 0 k_3 0 k_4];

Re_H = eig([-H_p eye(2) zeros(2,2); 
            -H_v zeros(2,2) eye(2);
            -H_s zeros(2,2) zeros(2,2)]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Compensation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
omega_1 = 1;
omega_2 = 1;

S_1 = [ 0 0 0; 0 0 1; 0 -omega_1^2 0];
S_2 = [ 0 0 0; 0 0 1; 0 -omega_2^2 0];

char_polynom_1 = poly(S_1);
char_polynom_2 = poly(S_2);

GG = [0; 0; 1];
F =[0 1 0; 0 0 1; -1 -1 -1];
GAMMA = [1 -15 1];

lambda_1 = eig(S_1)
lambda_2 = eig(S_2)

eig(F + GG*GAMMA)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
