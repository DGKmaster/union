%% Signal properties
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Amplitude
A_1 = 2;
A_2 = 3;

%% Frequency
omega_1 = 4;
omega_2 = 5;

%% Initial phase
psi_1 = 3;
psi_2 = 2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Regulator coefficients
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% P part
k_p_1 = 7;
k_p_2 = 7;
K_p = [k_p_1 0; 0 k_p_2];

%% D part
k_d_1 = 4;
k_d_2 = 4;
K_d = [k_d_1 0; 0 k_d_2];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Robot parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Link mass
m_1 = 5;
m_2 = 3;

%% Link length
l_1 = 1.5;
l_2 = 2;

%% Link center mass length
l_c_1 = 0.75;
l_c_2 = 1;

%% Inertia moment
I_1 = 0.7;
I_2 = 0.5;

%% Friction coefficients
k_fric_1 = 0.06;
k_fric_2 = 0.03;

%% Gravity
g_0 = 9.81;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Euler Lagrange matrices coefficients
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ro_1 = m_1*l_c_1^2 + m_2*l_1^2 + 2*m_2*l_1*l_c_2^2 + I_1;
ro_2 = m_2*l_c_2^2 + I_2;
ro_3 = m_2*l_1*l_c_2;
ro_4 = (m_1*l_c_1 + m_2*l_1)*g_0;
ro_5 = m_2*l_c_2*g_0;
ro_6 = m_2*l_c_2;
ro_7 = k_fric_1;
ro_8 = k_fric_2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Observer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
H_p = [10 1; -2 2];
H_v = [3 -1; -1 1];
o = 0.001;

Re_H = eig([-H_p eye(2); -H_v zeros(2,2)]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
