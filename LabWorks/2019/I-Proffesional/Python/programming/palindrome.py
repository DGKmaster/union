
def is_palindrome(n):
    return str(n) == str(n)[::-1]

# input_string = 'babaded'
input_string = input()

length = len(input_string)

max_length = 0
for i in range(length):
    for j in range(i,length):
        _slice = input_string[i:j+1] + input_string[0]
        # print(_slice, is_palindrome(_slice))
        if is_palindrome(_slice):
            if(len(_slice) > max_length):
                max_length = len(_slice)

print(length - max_length)
