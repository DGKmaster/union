from math import factorial, pow

values_str = '0 0 0 1'

values_int = [int(x) for x in values_str.split(' ')]

source_value = int(pow(2,values_int[0]) * \
                pow(3,values_int[1]) * \
                pow(5,values_int[2]) * \
                pow(7,values_int[3]))

# print(source_value)

find_flag = False
for i in range(40):
    check_value = factorial(i)

    if check_value == source_value:
        print('YES')
        find_flag = True
        break

if not find_flag:
    print('NO')
