from math import pow

def main():
    input_array = '1000000000 1000000000'
    # input_array = '2 2'
    # input_array = input()
    a, b = [int(x) for x in input_array.split(' ')]

    result = a * b

    max_x = 0
    for x in range(result + 1, 1, -1):
        if result % pow(x, 2) == 0:
            max_x = x
            break

    print(max_x)

if __name__ == '__main__':
    main()
