#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import logging
import time

logging.basicConfig(level=logging.DEBUG)

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("base/relay/led1")

def on_message(client, userdata, msg):
    if msg.payload.decode() == "1":
        print("Turn On!")
        client.disconnect()
    elif msg.payload.decode() == "0":
        print("Turn Off!")

client = mqtt.Client(client_id='mqtt-iprofi-2020-dgk')
logger = logging.getLogger(__name__)
client.enable_logger(logger)

client.connect('sandbox.rightech.io', port=1883, keepalive=5)
time.sleep(2)

client.on_connect = on_connect
client.on_message = on_message

client.loop_forever()
