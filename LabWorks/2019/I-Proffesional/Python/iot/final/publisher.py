#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import logging
import time

logging.basicConfig(level=logging.DEBUG)

client = mqtt.Client(client_id='mqtt-iprofi-2020-dgk')

logger = logging.getLogger(__name__)
client.enable_logger(logger)

client.connect('sandbox.rightech.io', port=1883, keepalive=5)
time.sleep(2)

print(client.publish(topic='base/state/temperature', payload='36.7', qos=0))
print(client.publish(topic='base/state/pos', payload='{ "lon": 31.407202720642, "lat": 55.808113079 }', qos=0))
print(client.publish(topic='distance', payload='36.6', qos=0))
print(client.publish(topic='move', payload='False', qos=0))

client.disconnect()
