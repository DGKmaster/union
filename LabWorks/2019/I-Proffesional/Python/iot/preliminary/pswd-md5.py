from math import factorial, ceil
import hashlib


# Get password from input
input_psk = 'IoT19'


if input_psk[0].isalpha():
    last_number = chr(ord(input_psk[1]) + 1)

    input_psk = input_psk + str(last_number).swapcase()

elif input_psk[0].isdigit():
    x = factorial(int(input_psk[0])) + ord(input_psk[0])
    input_psk = input_psk + str(x)

# Calculate
digits_num = 0
digits_sum = 0

for i in input_psk:
    if i.isdigit():
        digits_num += 1
        digits_sum += int(i)

process_psk = ''

if digits_num == 0:
    process_psk =  input_psk + '$'
else:
    process_psk = input_psk + str(ceil(digits_sum/digits_num))

output_psk = hashlib.md5()
output_psk.update(process_psk.encode())

print(output_psk.hexdigest())
