import ipaddress

# Get IP address from input
input_ip = 'e2a1:693a:f4ff:8fa8:f6f2:c413:0f62:979e'
# Get net mask from input
input_mask = '120'

# Form net parameters object
net = ipaddress.ip_network(input_ip + '/' + input_mask, strict=False)

# Print mask value
print(str(len(list(net.hosts())) + 1) + ';' + str(net.network_address))
