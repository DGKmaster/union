# num = '8'

# items = ['143.00',
#         '192.00',
#         '200.00',
#         '100.00',
#         '125.00',
#         '148.00',
#         '246.00',
#         '225.00']

num = input()

items = []
for i in range(int(num)):
    items.append(input())

int_items = [float('{0:.2f}'.format(float(i)/11)) for i in items]
avg = float('{0:.3f}'.format(sum(int_items)/len(int_items)))

out_items = []
for item in int_items:
    if abs(item - avg) > 3:
        out_items.append(item)

print(avg, str(out_items).replace('[','').replace(']','').replace(',',''))
