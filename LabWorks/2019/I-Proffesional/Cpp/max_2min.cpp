#include <iostream>
#include <sstream>
#include <vector>
#include <array>
 
typedef unsigned long long int Int;
 
int main() {
	std::vector<Int> input;
 
	//int length = 7;
 
	int length;
	std::cin >> length;
 
	for (int i = 0; i < length - 1; i++) {
		std::string input_string;
		std::getline(std::cin, input_string, ' ');
 
		std::stringstream input_stream;
 
		input_stream << input_string;
 
		Int input_int;
 
		input_stream >> input_int;
 
		input.push_back(input_int);
 
		input_stream.clear();
	}
 
	std::vector<Int> result;
 
	result.push_back(input[0]);
 
	for (int i = 1; i < length - 1; i++) {
		if (input[i] > result[i-1]) {
			result.push_back(input[i]);
		}
		else if(input[i] < 2*result[i - 1]){
			result.push_back(input[i]);
		}
	}
 
	std::cout << result.size() << std::endl;
 
	return 0;
}
