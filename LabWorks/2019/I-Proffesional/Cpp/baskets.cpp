#include <iostream>
 
int factorial(int n) {
	if (n <= 1) {
		return 1;
	}
	else {
		return n * factorial(n - 1);
	}
}
 
int count_possibilities(int n, int k) {
	return float(factorial(n)) / float((factorial(n - k) * factorial(k)));
}
 
int main() {
 
	// std::cout << factorial(5);
 
	int n = 12;
 
	int count = 2;
 
	for (int k = 2; k < n; k++) {
		//std::cout << k;
 
		int a = n - k;
 
		count += count_possibilities(k, a);
	}
 
	std::cout << count;
 
	return 0;
}
