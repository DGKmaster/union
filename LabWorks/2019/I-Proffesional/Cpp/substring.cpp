#include <iostream>
#include <string>
 
int main() {
	std::string input;
 
	std::cin >> input;
	//input = "ababcabcab";
 
	int max_size = 0;
	for (int i = 2; i < input.size(); i++) {
		std::string substr_1 = input.substr(0, i);
		std::string substr_2 = input.substr(i, input.size() - 2*i);
 
		if (substr_1 + substr_2 + substr_1 == input) {
			max_size = substr_1.size() + substr_2.size() - 1;
		}
	}
 
	std::cout << max_size << std::endl;
 
	return 0;
}
