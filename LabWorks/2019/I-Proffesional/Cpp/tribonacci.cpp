#include <iostream>
#include <vector>
 
typedef unsigned long long int Int;
 
std::vector<Int> storage = { 1, 1, 1 };
 
Int tribonacci(const Int n) {
	if (n <= 2) {
		return 1;
	}
	else {
		if (storage.size() < n) {
			storage.push_back(tribonacci(n - 1));
		}
 
		Int result = storage[n - 1] + storage[n - 2] + storage[n - 3];
 
		return result;
	}
}
 
int main() {
	//Int a = 4;
	//std::cout << tribonacci(a);
 
	Int L;
	Int R;
 
	//Int L = 1;
	//Int R = 5;
	//Int L = 722172502;
	//Int R = 980543990;
 
	std::cin >> L >> R;
 
	//std::cout << L << R << std::endl;
 
	Int index = 2;
	Int count = 0;
 
	while (true) {
		Int x = tribonacci(index);
 
		if (x < L) {
 
		}
		else if(x > R) {
			break;
		}
		else {
			count++;
		}
 
		index++;
	}
 
	std::cout << count << std::endl;
 
	return 0;
}
