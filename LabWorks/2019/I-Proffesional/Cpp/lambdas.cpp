#include <iostream>
#include <vector>
 
int main() {
	std::vector<float> lambdas =
	{ 10212.6, 7152.3, 5394.4, 4803.3, 4209.8, 3818.3, 3326.3, 3605.3, 2633.9, 2030.3,
	1282.8, 1135.8, 955.3, 704.5, 662.4, 0.0, 3.6, 45.3, 102.7, 125.8, 274.0, 229.5, 491.3,
	466.9, 2258.3 };
	//std::vector<float> lambdas = { 10212.6, 7152.3, 5394.4 };
 
	std::vector<float> lengths;
 
	int n = lambdas.size();
 
	float lambdas_sum = 0;
	for (float i : lambdas) {
		lambdas_sum += i;
	}
 
 
	float coef = 1.0 / n;
 
	for (int j = 1; j < n + 1; j++) {
		float sum = 0;
 
		for (int k = j; k < n + 1; k++) {
			sum += 1.0 / float(k);
		}
 
		lengths.push_back(sum * coef);
	}
 
	std::vector<float> components;
 
	for (int i = 0; i < lambdas.size(); i++){
		float norm = lambdas[i] / lambdas_sum;
 
		if (norm > lengths[i]) {
			components.push_back(norm);
		}
	}
 
	return 0;
}

#include <iostream>
 
int factorial(int n) {
	if (n <= 1) {
		return 1;
	}
	else {
		return n * factorial(n - 1);
	}
}
 
int count_possibilities(int n, int k) {
	return float(factorial(n)) / float((factorial(n - k) * factorial(k)));
}
 
int main() {
 
	// std::cout << factorial(5);
 
	int n = 12;
 
	int count = 2;
 
	for (int k = 2; k < n; k++) {
		//std::cout << k;
 
		int a = n - k;
 
		count += count_possibilities(k, a);
	}
 
	std::cout << count;
 
	return 0;
}
