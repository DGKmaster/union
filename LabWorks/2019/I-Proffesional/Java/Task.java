public class Task {
    static class A {
        int val = 1;
        public int getVal() {
            return val;
        }
        public A() {
            System.out.println(getVal());
        }
    }

    static class B extends A {
        int val = 2;
        public int getVal() {
            return val;
        }
        public B() {
            System.out.println(getVal());
        }
    }

    public static void main(String[] args) {
        A b = new B();
    }
}
