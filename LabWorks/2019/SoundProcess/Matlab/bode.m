%% Low Pass Filter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lpf_tf = tf([1], [tau 1]);
lpf_dtf_manual = tf([A_x], [1 -A_y], T);
lpf_dtf_auto = c2d(lpf_tf, T);

figure;
bode(lpf_dtf_manual);
hold on;
bode(lpf_dtf_auto);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Notch Filter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nf_tf = tf([1 0 (omega_mid)^2], [1 omega_width (omega_mid)^2]);
nf_dtf_manual = tf([x_2 x_1 x_0], [y_2 y_1 y_0], T);
nf_dtf_auto = c2d(nf_tf, T);

figure;
bode(nf_dtf_manual);
hold on;
bode(nf_dtf_auto);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
