%% General
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f_work = 44100;
T = 1 / f_work;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Low Pass Filter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f_0 = 5000;
tau = 1 / (2 * pi * f_0);

A_x = T / (tau + T);
A_y = tau / (tau + T);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Notch Filter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f_1 = 1234;
f_2 = 4321;

f_mid = (f_2 - f_1) / 2 ;
f_width = f_2 - f_1;

npFilter = dsp.NotchPeakFilter( ...
    'CenterFrequency', f_mid, 'Bandwidth', f_width);
[Bnotch,Anotch,Bpeak,Apeak] = tf(npFilter);

y_2 = Anotch(1);
y_1 = Anotch(2);
y_0 = Anotch(3);

x_2 = Bnotch(1);
x_1 = Bnotch(2);
x_0 = Bnotch(3);

omega_1 = f_1 / (2 * pi);
omega_2 = f_2 / (2 * pi);

omega_mid = f_mid / (2 * pi);
omega_width = f_width / (2 * pi);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
