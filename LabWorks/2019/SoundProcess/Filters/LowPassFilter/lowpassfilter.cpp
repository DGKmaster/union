#include "lowpassfilter.h"

LowPassFilter::LowPassFilter(float old_output)
	: AbstractFilter()
{
	this->old_output = old_output;
}

float LowPassFilter::update(float input)
{
	m_output = A_x * input + A_y * old_output;

	old_output = m_output;

	return m_output;
}
