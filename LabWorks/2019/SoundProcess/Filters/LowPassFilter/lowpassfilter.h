#ifndef LOWPASSFILTER_H
#define LOWPASSFILTER_H

#include "Filters/AbstractFilter/abstractfilter.h"

class LowPassFilter : public AbstractFilter
{
private:
	const float A_x = 0.4160;
	const float A_y = 0.5840;

	float old_output;

public:
	explicit LowPassFilter(float old_output);

	virtual float update(float input) override;
};

#endif // LOWPASSFILTER_H
