#include "notchfilter.h"

NotchFilter::NotchFilter(float old_output,
                         float old_input,
                         float old_old_output,
                         float old_old_input) : AbstractFilter()
{
    this->old_output = old_output;
    this->old_old_output = old_old_output;
    this->old_input = old_input;
    this->old_old_input = old_old_input;
}

float NotchFilter::update(float input)
{
    m_output = (y_2 * input 
                + y_1 * old_input 
                + y_0 * old_old_input 
                - x_1 * old_output 
                - x_0 * old_old_output) / x_2;

    old_output = m_output;    
    old_input = input;
    old_old_output = old_output;
    old_old_input = old_input;

    return m_output;
}
