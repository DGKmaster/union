#ifndef NOTCHFILTER_H
#define NOTCHFILTER_H

#include "Filters/AbstractFilter/abstractfilter.h"

class NotchFilter : public AbstractFilter
{
private:
    const float x_2 = 0.8173;
    const float x_1 = -1.5953;
    const float x_0 = 0.8173;

    const float y_2 = 0.6346;
    const float y_1 = -1.5953;
    const float y_0 = 1;

    float old_output;
    float old_old_output;
    float old_input;
    float old_old_input;

public:
    explicit NotchFilter(float old_output,
                         float old_input,
                         float old_old_output,
                         float old_old_input);

    virtual float update(float input) override;
};

#endif // NOTCHFILTER_H
