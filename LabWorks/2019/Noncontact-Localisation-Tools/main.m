%% Task 1. Clear
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc;
close all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Task 2. Init base Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Robot width
wheel_base = 0.2;
%% Robot center
delta = wheel_base / 2; 

%% Circle trajectory radius
R = 1;
r = 0.05;

%% Trajectory center offset
x_offset = -1;

sample_angle = pi / 50;
sample_time = 0.01;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Task 3. Set variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Sampling
angle_mid = 0:sample_angle:2*pi*R/r;
time = 0:sample_time:sample_time * (numel(angle_mid) - 1);
time_last = sample_time * (numel(angle_mid) - 1);

%% Robot turn angle
angle_v = r * (pi * angle_mid / 180) * 180 / (pi * R);
angle_v_t = timeseries(double(angle_v), time);

%% Robot linear position
x = -1 + R * cos(angle_v);
y = R * sin(angle_v);
x_t = timeseries(double(x), time);
y_t = timeseries(double(y), time);

%% Wheel angles
angle_in = angle_mid * (R - delta) / R;
angle_out = angle_mid * (R + delta) / R;
angle_in_t = timeseries(double(subs(angle_in)), time);
angle_out_t = timeseries(double(subs(angle_out)), time);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
