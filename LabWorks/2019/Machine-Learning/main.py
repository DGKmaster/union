#Подключение библиотек
from imageai.Detection import ObjectDetection
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
exec_path = os.getcwd()
detector = ObjectDetection()
#Задание типа модели
detector.setModelTypeAsYOLOv3()
detector.setModelPath(os.path.join(exec_path, "yolo.h5"))
detector.loadModel()

for i in range(1,11):
    list = detector.detectObjectsFromImage(
        #Задание имени входного и выходного файла
        input_image=os.path.join(exec_path, str(i) + ".jpg"),
        output_image_path=os.path.join(exec_path, str(i) + "_out_yolo_50.jpg"),
        #Задание порога отсечения
        minimum_percentage_probability=50
    )

print('Done!')
