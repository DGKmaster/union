%% Solve differential equation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
syms x_1_ss(t) x_2_ss(t) u(t) C_1_s C_2_s

ode_1 = diff(x_1_ss, t) == x_2_ss;
ode_2 = diff(x_2_ss, t) == -9*x_1_ss + 1/2*(C_1_s*sin(3*t) + C_2_s*cos(3*t));

cond_1 = x_1_ss(3.14) == 2;
cond_2 = x_2_ss(3.14/2) == 0;

S_dsolve = dsolve([ode_1; ode_2], [cond_1; cond_2]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Solve linear equation for C_1 and C_2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x_1_s(t) = vpa(simplify(S_dsolve.x_1_ss), 2);
x_2_s(t) = vpa(simplify(S_dsolve.x_2_ss), 2);

eqn_1 = vpa(x_1_s(pi),2) == 2;
eqn_2 = vpa(x_2_s(pi/2),2) == 0;

S_solve = solve(eqn_1, eqn_2);

C_1_r = double(S_solve.C_1_s);
C_2_r = double(S_solve.C_2_s);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Substitute symbolic C_1 and C_2 to real ones
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x_1_r(t) = subs(x_1_s(t), {'C_1_s', 'C_2_s'}, {C_1_r, C_2_r});
x_2_r(t) = subs(x_2_s(t), {'C_1_s', 'C_2_s'}, {C_1_r, C_2_r});

%% Check boundary values
double(x_1_r(pi));
double(x_2_r(pi/2));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
