A = [-2 1; 0 -2];
b = [3; 1];

W = [1 1; 1 7];
V = 2;

P = sym('P', 2);

eqn = A*P + P*A.' + G*W*G.' - P*C.'*V^(-1)*C*P == 0;
solution = solve(eqn, P);
variant = 3;

P = double([solution.P1_1(variant) solution.P1_2(variant);
            solution.P2_1(variant) solution.P2_2(variant)]);
L = P*C.'*V^(-1);
