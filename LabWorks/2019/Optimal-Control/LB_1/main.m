%% Task 1.2 c(x,u)=0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
syms x u lambda;
eqn_1 = 6*x + 7*u + 1 + 3*lambda == 0;
eqn_2 = 12*u + 7*x + 2 - 2*lambda*u == 0;
eqn_3 = 3*x - u^2 + 1 == 0;

sol = solve(eqn_1,eqn_2,eqn_3,x,u,lambda);
double(sol.x)
double(sol.u)
double(sol.lambda)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Task 1.3 c(x,u)<=0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
syms x u lambda;
eqn_1 = 6*x + 7*u + 1 + 3*lambda == 0;
eqn_2 = 12*u + 7*x + 2 - 2*lambda*u == 0;
eqn_3 = lambda*(3*x - u^2 + 1) == 0;
eqn_4 = 0 <= lambda;

sol = solve(eqn_1,eqn_2,eqn_3,eqn_4,x,u,lambda);
double(sol.x)
double(sol.u)
double(sol.lambda)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Task 2.1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
J(x, u) = 3*x^2 + 6*u^2 + 7*x*u + x + 2*u - 14;

vector = [0 0];

delta(x,u) = hessian(J(x, u)) \ gradient(J(x, u));

for i = 0:5
    vector = vector - delta(vector(2),vector(1));
end

sol = double(vector);
fprintf('x=%f \n u=%f \n', sol(2,1), sol(1,1));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Task 2.2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
J(x, u) = 3*x^2 + 6*u^2 + 7*x*u + x + 2*u - 14;

vector = [1 1];

learn_rate = 0.1;

speed_fall(x, u) = gradient(J(x, u));
for i = 0:30
    vector = vector - learn_rate*speed_fall(vector(2), vector(1));
end

sol = double(vector);
fprintf('x=%f \n u=%f \n', sol(2,1), sol(1,1));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
