#include "lib.h"

int h1tod( char c ) {
	if( c >= '0' && c <= '9' ) 
	{
		return c - '0';
	} 
	else 
		if( c >= 'a' && c <= 'f' ) 
		{
			return c - ( 'a' - 10 );
		} 
		else 
			if( c >= 'A' && c <= 'F' )
			{
				return c - ( 'A' - 10 );
			}
			else
			{
				throw runtime_error( "invalid hex digit" );
			}
}

double htod( char const* str ) 
{
	double result = 0;
	while( str[ 0 ] != '.' && str[ 0 ] != 'x' && str[ 0 ] != 'X' ) 
	{
		if( str[ 0 ] == 0 ) 
		{
			return result;
		}
		result = result * 16 + h1tod( str[ 0 ] );
		str++;
	}
	if( str[ 0 ] == '.' ) 
	{
		str++;
		double factor = 1.0/16;
		while( str[ 0 ] != 'x' && str[ 0 ] != 'X' ) 
		{
			if( str[ 0 ] == 0 ) 
			{
				return result;
			}
			result += h1tod( str[ 0 ] ) * factor;
			factor /= 16;
			str++;
		}
	}
	return result * pow( double( 2 ), atoi( str + 1 ) );
}
