/// Input 3 numbers and calculate function value

#include <cstdio>
#include <cstdlib>

int main()
{ 
	float x, y, z, s;
	
    printf("Input x=");
	scanf("%f", &x);
    
	printf("Input y=");
	scanf("%f", &y);
    
	printf("Input z=");
	scanf("%f", &z);
    
    s = (x+y+z)*(x+y+z);
    
	printf("x=%.3f y=%.3f z=%.3f\n"
           "Answer        S=%.3f\n"   
           "*********************\n"
           , x, y, z, s);
} 
