/// Sort 1D array
#include <iostream>

using namespace std;

int main() {
    float x[10];
    
    cout<<"Input 10 numbers"<<endl;
    
    for(size_t i = 0; i < 10; i++) {
        cin>>x[i];
    }

    for(size_t i = 0, j = 5; i < 5; i++, j++) {
        float min_value = x[i];
        float max_value = x[j];
        size_t min_index = i;
        size_t max_index = j;
        
        for(size_t i_index = i, j_index = j; i_index < 5; i_index++, j_index++) {
            if (min_value > x[i_index]) {
                min_value = x[i_index];
                min_index = i_index;
            }
            if (max_value < x[j_index]) {
                max_value = x[j_index];
                max_index = j_index;
            }
        }
        
        swap(x[i],x[min_index]);
        swap(x[j],x[max_index]);
    }
    
    for(size_t i = 0; i < 10; i++) {
        cout<<x[i]<<'\t';
    }
    
    cout<<endl;

    return 0;
}

