/// Read and write array to file
#include <iostream>
#include <fstream>

using namespace std;

int main() {
    float min_value, max_value, start_array[10], end_array[12];
    
    ofstream F ("F.txt");
    
    cout<<"Input 10 integer numbers"<<endl;
    
    for(size_t i = 0; i < 10; i++) {
        cin>>start_array[i];
        
        if(start_array[i] != static_cast<int>(start_array[i])) {
            cout<<"Not integer"<<endl;
            return -1;
        }
        
        F<<start_array[i]<<" ";
    }
    
    min_value = max_value = start_array[0];
    
    for(size_t i = 1; i < 10; i++) {
        if(min_value < start_array[i]) {
            min_value = start_array[i];
        }
        if(max_value > start_array[i]) {
            max_value = start_array[i];
        }
    }
    
    F<<" "<<min_value<<endl<<" "<<max_value<<endl;
    
    F.close();
    
    ifstream D ("F.txt");
    for(size_t i = 0; i < 12; i++) {
        D>>end_array[i];
        cout<<end_array[i]<<" ";
    }
    D.close();
    
    cout<<endl;
    
    return 0;
}

