/// File stream usage
#include <iostream>
#include <fstream>

using namespace std;

int main() {
    size_t n;
    float m[100],l;

    ofstream F ( "F.txt" );
    cout<<"Input amount ";
    cin>>n;
    cout<<endl;
    for(size_t i = 0; i < n; i++) {
        cin>>l;
        F<<l<<" ";
    }
    F.close();
    
    ifstream D ("F.txt");
    for(size_t i = 0; i < n; i++) {
        D>>m[i];
    }
    swap(m[0],m[n-1]);
    D.close();
    
    ofstream G ( "G.txt" );
    for(size_t i = 0; i < n; i++) {
        G<<m[i]<<" ";
    }
    
    G.close();
    
    return 0;
}
