/// Struct usage
#include <iostream>
#include <cstdlib>

using namespace std;

struct M {
    size_t m;
    size_t n;
    float c[100][100];
};

struct N {
    size_t m;
    float c[100];
};

N structFunction(M x);

int main() {
    size_t m,n;
    
    cout<<"Rows = ";
    cin>>m;
    cout<<"Columns = ";
    cin>>n;
    
    M x;
    x.m=m;
    x.n=n;
    
    for(size_t i = 0; i < m; i++) {
        
        cout<<endl<<i + 1<<") ";
        
        for(size_t  j = 0; j < n; j++) {
            x.c[i][j] = rand() % 10;
            cout<<x.c[i][j]<<" ";
        }
    }
    
    cout<<endl<<endl<<"[a;b] = [6;9]"<<endl<<endl;
    
    N k = structFunction(x);
    
    for(size_t i=0; i<k.m; i++) {
        cout<<i+1<<") "<<k.c[i]<<endl;
    }
    
    return 0;
}
N structFunction(M x) {
    N k;
    k.m = x.m;
    
    for(size_t i=0; i < x.m; i++) {
        
        size_t b = 0;
        
        for(size_t j = 0; j < x.n; j++) {
            if( (x.c[i][j] >= 6)&&(x.c[i][j] <= 9)) {
                b++;
            }
        }
        
        k.c[i] = b;
    }
    
    return k;
}

