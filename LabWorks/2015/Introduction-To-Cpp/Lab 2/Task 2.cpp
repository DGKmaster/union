/// Iteration over array example
#include <iostream>
#include <cmath>

using namespace std;

int main() {

    float x[100]= {0};
    int s = 0,i = 0;
    
    cout<<"Input integer values for X array"<<endl;
    
    do {
        cin>>x[i];
        if ( (ceil(x[i])-x[i]) != 0) {
            cout<<"Number must be integer"<<endl;
            return 0;
        }
        i++;
    }
    while (x[i-1]!=0);

    for(size_t j = 0; j < 100; j += 2) {
        s += x[j];
    }

    cout<<"s = "<<s<<endl;

    return 0;
}

