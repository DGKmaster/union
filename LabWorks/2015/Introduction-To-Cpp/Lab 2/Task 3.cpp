/// Matrix calculation example
#include <iostream>

using namespace std;

int main() {

    int a[2][2];
    int b[2][3];
    int c[2][2];

    int d[2][2] = {0};
    
    cout<<"Input first A matrix"<<endl;
    for(size_t i = 0; i < 2; i++) {
        for(size_t j = 0; j < 2; j++) {
            cin>>a[i][j];
        }
    }
    cout<<"Input second B matrix"<<endl;
    for(size_t i = 0; i < 2; i++) {
        for(size_t j = 0; j < 3; j++) {
            cin>>b[i][j];
        }
    }
    cout<<"A+B*Bt = "<<endl;
    for (size_t i=0; i < 2; i++) {
        for (size_t j = 0; j < 2; j++) {
            for (size_t k = 0 ; k < 3; k++) {
                d[i][j] += b[i][k] * b[j][k];
            }
            c[i][j] = a[i][j] + d[i][j];
            cout<<c[i][j]<<"\t";
        }
        cout<<endl;
    }
    
    cout<<endl;
    
    return 0;
}

