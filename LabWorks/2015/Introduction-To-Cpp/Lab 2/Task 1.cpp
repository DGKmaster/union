/// Switch usage example
#include <iostream>

using namespace std;

int main() {
    int c;
    float f;
    cout<<"Input number from 1 to 10"<<endl;
    cin>>f;
    c=f;
    if ((c>=1)&&(c<=10)&&((f-c)==0)) {
        switch (c) {
            case 1:
                cout<<"It is one";
                break;
            case 2:
                cout<<"It is two";
                break;
            case 3:
                cout<<"It is three";
                break;
            case 4:
                cout<<"It is four";
                break;
            case 5:
                cout<<"It is five";
                break;
            case 6:
                cout<<"It is six";
                break;
            case 7:
                cout<<"It is seven";
                break;
            case 8:
                cout<<"It is eight";
                break;
            case 9:
                cout<<"It is nine";
                break;
            case 10:
                cout<<"It is ten";
                break;
        }
    }
    else {
        cout<<"Number must be integer";
    }
    
    cout<<endl;
    
    return 0;
}

