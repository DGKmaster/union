/// Function usage
#include <iostream>

using namespace std;

float mathFunction(const float first_number, const float second_number) {
    float nominator = first_number * first_number + second_number * second_number;
    float denominator = first_number * first_number + 2 * first_number * second_number + 3 * second_number * second_number + 4;

    if(denominator == 0) {
        cout<<"Denominator is zero"<<endl;

        return 0;
    }

    float c = nominator / denominator;
    return c;
}

int main() {

    float s,t;

    cout<<"Input S"<<endl;
    cin>>s;

    cout<<"Input T"<<endl;
    cin>>t;

    float k = mathFunction(1.2, s) + mathFunction(t, s) - mathFunction(2 * s - 1, s * t);

    cout<<"K = "<<k<<endl;

    return 0;
}
