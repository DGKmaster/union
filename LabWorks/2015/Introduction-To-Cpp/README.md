# Introduction To C++

Basic math algortihms written in C++.

* **Lab 1** - Input 3 numbers and calculate function value; Increment and decrement example; Interval check.
* **Lab 2** - Switch usage example; Iteration over array example; Matrix calculation example.
* **Lab 3** - Function usage.
* **Lab 4** - Struct usage.
* **Lab 5** - File stream usage.
* **Lab 6** - External static library usage.
* **Special 1** - Sort 1D array.
* **Special 2** - Read and write array to file
* **Special 3** - Min value and near min value searching in 1D array.
