/// Min value and near min value searching in 1D array
#include <iostream>
#include <cstdio>

using namespace std;

int main() {
    float x[20];
    float min_value, near_min_value, array_size;
    size_t min_index, near_min_index;

    cout<<"Input array size as integer from 2 to 20"<<endl;
    cin>>array_size;

    if ((array_size > 20) || (array_size != static_cast<int>(array_size)) || (array_size < 2)) {
        cout<<"Bad array size"<<endl;
        return -1;
    }

    for(size_t i = 0; i < array_size; i++) {
        cin>>x[i];
    }

    min_value = x[0];
    min_index = 0;
    
    near_min_value = x[1];
    near_min_index = 1;

    for(size_t i = 0; i < array_size; i++) {
        if (min_value > x[i]) {
            near_min_value = min_value;
            near_min_index = min_index;
            
            min_value = x[i];
            min_index = i;
        }
    }

    cout<<"Min value: x["<<min_index<<"]"<<" = "<<min_value<<endl<<
        "Near min value: x["<<near_min_index<<"]"<<" = "<<near_min_value<<endl;

    return 0;
}

