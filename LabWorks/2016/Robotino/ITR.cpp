#define _USE_MATH_DEFINES

#include <cmath>
#include <iostream>
#include <conio.h>

// 1 - straight, 8 - right, 3 - left 1.9
#include "rec/robotino/com/all.h"
#include "rec/core_lt/utils.h"
#include "rec/core_lt/Timer.h"

using namespace rec::robotino::com;
using namespace std;

rec::core_lt::Timer timer, timer1;
int timex;
bool xvatit = 1;

class MyCom : public Com {
    public:
        MyCom() {
        }

        void errorEvent( Error error, const char* errorString ) {
            std::cerr << "Error: " << errorString << std::endl;
        }

        void connectedEvent() {
            std::cout << "Connected." << std::endl;
        }

        void connectionClosedEvent() {
            std::cout << "Connection closed." << std::endl;
        }
};
void drive(float xp, float yp, float zp, int time, bool erst);
void avoid();
MyCom com;
Motor motor1;
Motor motor2;
Motor motor3;
OmniDrive omniDrive;
Bumper bumper;
DistanceSensor sensor1, sensorL, sensorR;
void init( const std::string& hostname ) {
    // Initialize the actors
    motor1.setMotorNumber( 0 );

    motor2.setMotorNumber( 1 );

    motor3.setMotorNumber( 2 );

    // Connect
    std::cout << "Connecting..." << std::endl;
    com.setAddress( hostname.c_str() );

    com.connect();

    std::cout << std::endl << "Connected" << std::endl;
}

void avoid() {
    int ui;
    timer1.start();
    int timet;
    omniDrive.setVelocity( 0, 0, 0);
    if(sensorR.voltage() < sensorL.voltage()) { // left
        ui = 1;
        while(sensor1.voltage() > 1.6) {
            drive(0, -100, 0, 1000, 0); //�� �������
        }
        drive(0, -100, 0, 2000, 0); // ��� ���� ����� �� ��������� ����� ������ �����
    }
    else {
        ui = -1;
        while(sensor1.voltage() > 1.6) {
            drive(0, 100, 0, 1000, 0);//�� �������
            cout<<timer.msecsElapsed()<<endl;
        }
        drive(0, 100, 0, 2000, 0);// ��� ���� ����� �� ��������� ����� ������ �����
    }
    timet = timer1.msecsElapsed();
    drive(100, 0, 0, 3000, 0); // �������� ����� ����� ������� ����������� �����
    timer1.start();
    switch(ui) {
        case 1: {
                while(sensorL.voltage() > 1) {
                    drive(100, 0, 0, 1000, 0);
                }
                drive(100, 0, 0, 2000, 0);
                timex = timer1.msecsElapsed();
                cout<<timer1.msecsElapsed()<<endl;
                drive(0, 100, 0, timet, 0);
                break;
            }
        case -1: {
                while(sensorR.voltage() > 1) {
                    drive(100, 0, 0, 1000, 0);
                }
                drive(100, 0, 0, 2000, 0);
                timex = timer1.msecsElapsed();
                cout<<timer1.msecsElapsed()<<endl;
                drive(0, -100, 0, timet, 0);
                break;
            }
    }

}

void drive(float xp, float yp, float zp, int time, bool erst) {
    timer.start();
    int timez;
    int timeup = time;
    while( com.isConnected() && timer.msecsElapsed() < timeup ) {
        omniDrive.setVelocity( xp, yp, zp);
        if( (sensor1.voltage() > 1.9) && erst && xvatit) {
            timez = timer.msecsElapsed();
            avoid();
            timer.start();
            timeup -= timez + timex;
            xvatit = 0;
        }
        com.waitForUpdate();
    }
}

void destroy() {
    com.disconnect();
}

int main( int argc, char **argv ) {
    cout << "example_forward" << endl;

    string hostname = "172.26.1.1";

    if( argc > 1 ) {
        hostname = argv[1];
    }

    try {
        init( hostname );

        const float U1 = 4000, T1 = 1600, R1 = 6000, T2 = 1400, D1 = 10000, T3 = 2000, L1 = 8000, T4 = 1500;
        const float xpower = 100, ypower = 100, zpower = -60;

        sensor1.setSensorNumber(0);
        sensorL.setSensorNumber(2);
        sensorR.setSensorNumber(7);

        getch();

        drive(100, 0, 0, U1, 1);
        drive(0, 0, -60, T1, 1);
        drive(100, 0, 0, R1, 1);
        drive(0, 0, -60, T2, 1);
        drive(100, 0, 0, D1, 1);
        drive(0, 0, -60, T3, 1);
        drive(100, 0, 0, L1, 1);
        drive(0, 0, -60, T4, 1);

        destroy();
    }

    catch( const rec::robotino::com::ComException& e ) {
        std::cerr << "Com Error: " << e.what() << std::endl;
    }

    catch( const std::exception& e ) {
        std::cerr << "Error: " << e.what() << std::endl;
    }

    catch( ... ) {
        std::cerr << "Unknow Error" << std::endl;
    }

    //std::cout << "Press any key to exit..." << std::endl;
    //rec::core_lt::waitForKey();
}
