#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include "rec/robotino/com/all.h"
#include "rec/core_lt/utils.h"
#include "rec/core_lt/Timer.h"
#include "rec/robotino/com/Actor.h"


using namespace rec::robotino::com;
using namespace std;

rec::core_lt::Timer timer;
int timex;

class MyCom : public Com {
    public:
        MyCom() {
        }

        void errorEvent(Error error, const char* errorString) {
            std::cerr << "Error: " << errorString << std::endl;
        }

        void connectedEvent() {
            std::cout << "Connected." << std::endl;
        }

        void connectionClosedEvent() {
            std::cout << "Connection closed." << std::endl;
        }
};

void drive(float xp, float yp, float zp, int time);
void destroy();
MyCom com;
Motor motor1;
Motor motor2;
Motor motor3;
OmniDrive omniDrive;
int en0R, en0L, en1R, en1L;

void init(const std::string& hostname) {
    // Initialize the actors
    motor1.setMotorNumber(0);

    motor2.setMotorNumber(1);

    motor3.setMotorNumber(2);

    // Connect
    std::cout << "Connecting..." << std::endl;
    com.setAddress(hostname.c_str());

    com.connect();

    std::cout << std::endl << "Connected" << std::endl;
}

void drive(float xp, float yp, float zp, int time) {
    timer.start();
    int timez;
    while (com.isConnected() && timer.msecsElapsed() < time) {
        omniDrive.setVelocity(xp, yp, zp);
        com.waitForUpdate();
    }
}

int main(int argc, char **argv) {
    cout << "example_forward" << endl;
    string hostname = "172.26.1.1";
    if (argc > 1) {
        hostname = argv[1];
    }

    try {
        init(hostname);
        system("pause");
        timer.start();
        en0L = motor1.actualPosition();
        en0R = motor3.actualPosition();
        en1L = motor1.actualPosition() - en0L;
        en1R = motor3.actualPosition() - en0R;
        drive(100, 0, 0, 2000);
        cout<<motor1.actualPosition()<<"    "<<motor3.actualPosition()<<endl;
        cout<<en0L<<"    "<<en0R<<endl;
        cout<<en1L<<"    "<<en1R<<endl;
        system("pause");
        destroy();
    }
    catch (const rec::robotino::com::ComException& e) {
        std::cerr << "Com Error: " << e.what() << std::endl;
    }
    catch (const std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }
    catch (...) {
        std::cerr << "Unknow Error" << std::endl;
    }
    //std::cout << "Press any key to exit..." << std::endl;
    //rec::core_lt::waitForKey();
}

void destroy() {
    com.disconnect();
}

