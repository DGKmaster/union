#include <avr/io.h>
#include <util/delay.h> //Подключение библиотек
#define SPI_DDR     DDRB
#define SPI_PORT    PORTB
#define SPI_2		PB1
#define SPI_SS      PB2
#define SPI_MOSI    PB3
#define SPI_MISO    PB4
#define SPI_SCK     PB5
char d[7] ={
	0x77,  //a
	0x1F,  //b
	0x4E,  //c
	0x3D,  //d
	0x4F,  //e
	0x47,  //f
	0x00   //пусто
};
void spi(char cmd,char data,int devC)   //Функция передачи двух пакетов по 8 бит по протоколу SPI
{
	char nameD = devC==0?1<<SPI_SS:devC==1?1<<SPI_2:(1<<SPI_SS | 1<<SPI_2);
	SPI_PORT &= ~nameD;                       //сбрасываем SS в 0
	SPDR = cmd;                               //отправляем данные по SPI адрес
	while(!(SPSR&(1<<SPIF)));                 //ждем окончания отправки
	SPDR = data;                              //отправляем данные по SPI данные
	while(!(SPSR&(1<<SPIF)));                 //ждем окончания отправки
	SPI_PORT |= nameD;                        //устанавливаем SS в 1
}

void clrdig ()          //Функция очистки индикаторов
{
	for(int i=1; i<=16; i++)
	{
		if(i<=8)
		{
			spi(i,0,0);
		}
		else
		{
			spi(i%8,0,1);
		}
	}
}
void init(int devC)
{
	spi(0x0C,0x00, devC);     //Отключение индикаторов
	spi(0x09,0x00, devC);     //Отключение декодирования
	spi(0x0A,0x0A, devC);     //Интенсивность свечения индикаторов
	spi(0x0B,0x07, devC);     //Количество индикаторов начиная с 0
	spi(0x0F,0x00, devC);     //Отключение теста индикаторов
	spi(0x0C,0x01, devC);     //Включение индикаторов
}
int main()
{
	SPI_DDR = (1<<SPI_MOSI)|(1<<SPI_SCK)|(1<<SPI_SS)|(1<<SPI_2);  //настраиваем MOSI, SCK, SS
	SPI_PORT |=(1<<SPI_SS)|(1<<SPI_2);                  //устанавливаем SS в 1
	SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR1)|(1<<SPR0);      //через регистр SPCR настраиваеи
	
	init(2);
	clrdig();        //Очистка всех индикаторов
	int wd[4] = {d[1], d[2], d[3]};
	int lg = sizeof(wd)/sizeof(wd[0]);
	int pos = 0;
	while(1)     //Бесконечный цикл
	{
		for(int i=0; i<16; i++)
		{
			spi(i%8+1,wd[(i+pos)%lg],i/8);
		}
			_delay_ms(100);
			clrdig();
			pos++;
			
	}
	return 0;
}