#include <avr/io.h>
#include <util/delay.h>
#define SPI_DDR     DDRB
#define SPI_PORT    PORTB
#define SPI_2		PB1
#define SPI_SS      PB2
#define SPI_MOSI    PB3
#define SPI_MISO    PB4
#define SPI_SCK     PB5
char d[7] ={
	0x77,  //a
	0x1F,  //b
	0x4E,  //c
	0x3D,  //d
	0x4F,  //e
	0x47,  //f
	0x00   //пусто
};
void spi(char cmd,char data)   //Функция передачи двух пакетов по 8 бит по протоколу SPI
{
	SPI_PORT &= ~(1<<SPI_SS);                       //сбрасываем SS в 0
	SPDR = cmd;                                     //отправляем данные по SPI адрес
	while(!(SPSR&(1<<SPIF)));                       //ждем окончания отправки
	SPDR = data;                                    //отправляем данные по SPI данные
	while(!(SPSR&(1<<SPIF)));                       //ждем окончания отправки
	SPI_PORT |= (1<<SPI_SS);                        //устанавливаем SS в 1
}

void clrdig ()          //Функция очистки индикаторов
{
	spi(0x01,d[6]);
	spi(0x02,d[6]);
	spi(0x03,d[6]);
	spi(0x04,d[6]);
	spi(0x05,d[6]);
	spi(0x06,d[6]);
	spi(0x07,d[6]);
	spi(0x08,d[6]);
	
}
int main()
{
	SPI_DDR = (1<<SPI_MOSI)|(1<<SPI_SCK)|(1<<SPI_SS)|(1<<SPI_2);    //настраиваем MOSI, SCK, SS
	SPI_PORT |=(1<<SPI_SS);                              //устанавливаем SS в 1
	SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR1)|(1<<SPR0);       //через регистр SPCR настраиваеи
	spi(0x0C,0x00);     //Отключение индикаторов
	spi(0x09,0x00);     //Отключение декодирования
	spi(0x0A,0x0A);     //Интенсивность свечения индикаторов
	spi(0x0B,0x07);     //Количество индикаторов начиная с 0
	spi(0x0F,0x00);     //Отключение теста индикаторов
	spi(0x0C,0x01);     //Включение индикаторов
	
	clrdig();           //Очистка всех индикаторов
	int i=0;                //Переменная перебора выводимых символов
	int j=1;
	int g=1;                //Переменная осчета номера индикатора
	while(1){           //Бесконечный цикл
		
			spi(j,d[i]);
			spi(j+1,d[i+1]);    //Вывод на индикатор j символа i
			_delay_ms(300);    //Задержка выполнения 1000мс
			i++;
			if(i>7)i=0;
			
			spi(j-1,d[6]);
		
	}
	return 0;
}