#include <avr/io.h>
#include <util/delay.h>

#define SPI_DDR DDRB
#define SPI_PORT PORTB
#define SPI_SSA PB2
#define SPI_SSB PB1
#define SPI_MOSI PB3
#define SPI_SCK PB5

char digit_table[ 18 ] = {
	0x7E, //0
	0x30, //1
	0x6D, //2
	0x79, //3
	0x33, //4
	0x5B, //5
	0x5F, //6
	0x70, //7
	0x7F, //8
	0x7B, //9
	0x77, //a
	0x1F, //b
	0x4E, //c
	0x3D, //d
	0x4F, //e
	0x47, //f
	0x80, //.
	0x00,
};

void spi_send( int ss, char cmd, char data )
{
	SPI_PORT &= ~( 1 << ss );
	SPDR = cmd;
	while( !( SPSR & ( 1 << SPIF ) ) )
	{
	}
	SPDR = data;
	while( !( SPSR & ( 1 << SPIF ) ) )
	{
	}
	SPI_PORT |= 1 << ss;
}

void display_set( int digit, int value )
{
	if( digit <= 8 )
	{
		spi_send( SPI_SSA, digit, value );
	}
	else
	{
		spi_send( SPI_SSB, digit - 8, value );
	}
}

void display_clear()
{
	for( int i = 1; i <= 16; ++i )
	{
		display_set( i, 0 );
	}
}

void display_init( int ss )
{
	// Initialize MAX7221
	spi_send( ss, 0x0C, 0x00 ); // Disable the displays
	spi_send( ss, 0x09, 0x00 ); // Disable decoding
	spi_send( ss, 0x0A, 0x0A ); // Display brightness
	spi_send( ss, 0x0B, 0x07 ); // Last digit index
	spi_send( ss, 0x0F, 0x00 ); // Disable display tests
	spi_send( ss, 0x0C, 0x01 ); // Enable the displays
}

/*
  40
02  20
  01
04  10
  08  80
*/

char str[ 32 ] = {
	// 0x77, 0x33, 0x33, 0x00, 0x0e, 0x15, 0x11, 0x77, 0x7e,
	0x7E, //0
	0x30, //1
	0x6D, //2
	0x79, //3
	0x33, //4
	0x5B, //5
	0x5F, //6
	0x70, //7
	0x7F, //8
	0x7B, //9,
};
int str_len = 16;
int str_offset = 0;

int main()
{
	// Сonfigure MOSI, SCK and SS as outputs
	SPI_DDR = ( 1 << SPI_MOSI ) | ( 1 << SPI_SCK ) | ( 1 << SPI_SSA ) | ( 1 << SPI_SSB );
	SPI_PORT |= ( 1 << SPI_SSA ) | ( 1 << SPI_SSB );
	SPCR = ( 1 << SPE ) | ( 1 << MSTR ) | ( 1 << SPR1 ) | ( 1 << SPR0 );
	display_init( SPI_SSA );
	display_init( SPI_SSB );
	display_clear();
	while( 1 )
	{
		for( int i = 1; i <= 16; ++i )
		{
			display_set( i, str[ ( i - 1 + str_offset ) % str_len ] );
		}
		_delay_ms( 500 );
		str_offset = ( str_offset + 1 ) % str_len;
	}
	return 0;
}