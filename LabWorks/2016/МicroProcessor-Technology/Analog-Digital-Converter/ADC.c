#define F_CPU 8000000 // Рабочая частота контроллера
#define BAUD 9600 // Скорость обмена данными
#define valueOfUBBR F_CPU/BAUD/16-1 //расчет значения UBRR
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <avr/interrupt.h>

void initUART(unsigned int ubrr) {
    UBRR0L = (unsigned char) ubrr;		//Младшие 8 бит UBRRL_value
    UBRR0H = (unsigned char) (ubrr>>8); //Старшие 8 бит UBRRL_value
    UCSR0B |=(1<<TXEN0)|(1<<RXEN0);		//Бит разрешения передачи
    UCSR0C |=(1<< UCSZ00)|(1<< UCSZ01); //Устанавливем формат 8 бит данных
}

void initADC() {
    /// Опорное напряжение AVCC (+5в)
    ADMUX|=(1<<REFS0);
    /// Включение АЦП, деление частоты на 64
    ADCSRA|=(1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADATE);
    /// По переполнению таймера
    ADCSRB|=(1<<ADTS2)|(1<<ADTS1); 
}

void initInterrupt() {
    /// Старт таймера 0, частота /1024
    TCCR0B|=(1<<CS02)|(1<<CS00); 
    /// Разрешение прерываний по переполнению таймера 0
    TIMSK0|=(1<<TOIE0); 
}

/// Отправка символа
void send(char value) {
    /// Ожидаем когда очистится буфер						
    while(!( UCSR0A & (1 << UDRE0))); 
    UDR0 = value;
}


void send_int_Uart(int volt) {
    unsigned char temp;
    temp=volt/100;
    send(temp/10+'0');
    send('.');
    send(temp%10+'0');
    temp=volt%100;
    send(temp/10+'0');
    send(temp%10+'0');
}

ISR(TIMER0_OVF_vect) {
    //бит начала преобразований
    ADCSRA|=(1<<ADSC);			
    
    //когда записан преобразование заканчивается
    while(!(ADCSRA&(1<<ADIF))) { 
        float voltage;
        
        //приводим значение напряжения
        voltage = (int) ADCW * 5000.00 / 1024.00;  
        send_int_Uart((int)voltage);
        send(13);
    }
}

int main(void) {
    initUART(valueOfUBBR);
    initADC();
    initInterrupt();
    
    //глобальное разрешение прерываний
    sei();			
    
    while(1) {
    }
}

