;****** Программа decbin_sum_counter
newcount:
mov r17, r16; Если уже 9 единиц
andi r17, 0x0F
cpi r17, 9
breq newten

newinc:
inc r16 ; Если меньше 9 единиц
cpi r16, 153
breq endcount
rjmp newcount

newten:
mov r17, r16
andi r17, 0xF0
swap r17
inc r17
swap r17
mov  r16, r17
rjmp newcount

endcount:

nop