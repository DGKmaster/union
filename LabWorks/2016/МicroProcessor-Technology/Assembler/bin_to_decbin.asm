;****** Программа bin_to_decbin
.dseg
a: .byte 1 ; Число в bin
b: .byte 1 ; Количество десятков
c: .byte 1 ; Количество единиц
d: .byte 1 ; Число в 2-10
.cseg
; Загрузка числа
mov r17, r16 
sts a, r16
clr r16
; r16 = 0, r17 = bin
; Подсчёт десятков
next:
subi r17, 10 
inc r16
cpi r17, 0
brpl next
dec r16
sts b, r16
clr r17
clr r16
; Нахождение единиц 
lds r16, a ; r16 = bin
ldi r17, 10 ; r17 = 10
lds r18, b ; r18 = Число десятков
mul r18, r17 ; Умножение на 10
sub r16, r0 ; Нахождение единиц 
sts c, r16
clr r0
clr r16
clr r17
clr r18
; Запись 2-10 числа в r16
lds r16, b
lds r17, c
lsl r16
lsl r16
lsl r16
lsl r16
add r16,r17
sts d, r16
nop