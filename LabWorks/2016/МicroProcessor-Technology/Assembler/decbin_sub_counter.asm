;****** Программа decbin_sub_counter
newcount:
mov r17, r16; Если уже 9 единиц
andi r17, 0x0F
cpi r17, 0
breq newten

newinc:
dec r16
cpi r16, 0
breq endcount
rjmp newcount

newten:
mov r17, r16
andi r17, 0xF0
swap r17
dec r17
swap r17
ori r17, 9
mov  r16, r17
rjmp newcount

endcount:
nop