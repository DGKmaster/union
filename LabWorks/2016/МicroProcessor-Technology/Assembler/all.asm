;Волков Артем 8Е41, Задача 1, Задания 7,8,9
;****** Программа decbin_to_bin
mov r17,r16   ; Исходное двоично-десятичное число хранится в r16
              ; Оно копируется в регистр r17
andi r17,0xF0 ; Выделение старшей тетрады (десятков)
              ; Путём операции побитовой И (F0 = 1111 0000)
swap r17      ; Обмен тетрадами внутри аккумулятора
ldi r18,10    ; Умножение десятков на десять
mul r17,r18   ;
mov r17,r16   ; Выделение младшей тетрады (единиц)
andi r17,0x0F ; Операция побитового И на (0000 1111)
add r17,r0    ; Сложение результатов. Результат остаётся в r17	

;****** Программа bin_to_decbin
.dseg
a: .byte 1 ; Число в bin
b: .byte 1 ; Количество десятков
c: .byte 1 ; Количество единиц
d: .byte 1 ; Число в 2-10
.cseg
; Загрузка числа
mov r17, r16 
sts a, r16
clr r16
; r16 = 0, r17 = bin
; Подсчёт десятков
next:
subi r17, 10 
inc r16
cpi r17, 0
brpl next
dec r16
sts b, r16
clr r17
clr r16
; Нахождение единиц 
lds r16, a ; r16 = bin
ldi r17, 10 ; r17 = 10
lds r18, b ; r18 = Число десятков
mul r18, r17 ; Умножение на 10
sub r16, r0 ; Нахождение единиц 
sts c, r16
clr r0
clr r16
clr r17
clr r18
; Запись 2-10 числа в r16
lds r16, b
lds r17, c
lsl r16
lsl r16
lsl r16
lsl r16
add r16,r17
sts d, r16
nop

; Задание 7
;****** Программа add_two_bin_using_decbin 
; Сегмент данных
.dseg
; Первое число
a1: .byte 1 ; Число в bin
b1: .byte 1 ; Количество десятков
c1: .byte 1 ; Количество единиц
d1: .byte 1 ; Число в 2-10

; Второе число
a2: .byte 1 ; Число в bin
b2: .byte 1 ; Количество десятков
c2: .byte 1 ; Количество единиц
d2: .byte 1 ; Число в 2-10

; Третье число
a3: .byte 1 ; Число в bin
b3: .byte 1 ; Количество десятков
c3: .byte 1 ; Количество единиц
d3: .byte 1 ; Число в 2-10

;Программный сегмент
.cseg
; Загрузка десятичных чисел из регистров r16, r17
sts a1, r16 
sts a2, r17

; Первое число
; Подсчёт десятков
lds r17, a1
clr r16
next1:
subi r17, 10 
inc r16
cpi r17, 0
brpl next1
dec r16
sts b1, r16 ; Выгрузка количества десятков в переменную

; Нахождение единиц 
lds r16, a1 ; r16 = bin
ldi r17, 10 ; r17 = 10
lds r18, b1 ; r18 = Число десятков
mul r18, r17 ; Умножение на 10
sub r16, r0 ; Нахождение единиц 
sts c1, r16 ; Выгрузка количества единиц

; Запись 2-10 числа в r16
lds r16, b1 ; Загрузка в регистр количества десятков
lds r17, c1 ; Загрузка в регистр количества единиц
lsl r16   ; Сдвиг количества десятков на старшую тетраду
lsl r16
lsl r16
lsl r16
add r16,r17 ; Запись на младшую тетраду количества единиц
sts d1, r16 ; Запись числа в виде 2-10 в перменную

; Второе число
; Подсчёт десятков
lds r17, a2
clr r16
next2:
subi r17, 10 
inc r16
cpi r17, 0
brpl next2
dec r16
sts b2, r16

; Нахождение единиц 
lds r16, a2 ; r16 = bin
ldi r17, 10 ; r17 = 10
lds r18, b2 ; r18 = Число десятков
mul r18, r17 ; Умножение на 10
sub r16, r0 ; Нахождение единиц 
sts c2, r16

; Запись 2-10 числа в r16
lds r16, b2
lds r17, c2
lsl r16
lsl r16
lsl r16
lsl r16
add r16,r17
sts d2, r16

; Сложение единиц
lds r16, d1
lds r17, d2

andi r16, 0x0F
andi r17, 0x0F
add r16, r17
sts a3, r16

; Третье число
; Подсчёт десятков
lds r17, a3
clr r16
next3:
subi r17, 10 
inc r16
cpi r17, 0
brpl next3
dec r16
sts b3, r16
clr r17
clr r16
; Нахождение единиц 
lds r16, a3 ; r16 = bin
ldi r17, 10 ; r17 = 10
lds r18, b3 ; r18 = Число десятков
mul r18, r17 ; Умножение на 10
sub r16, r0 ; Нахождение единиц 
sts c3, r16

; Запись 2-10 числа в r16
lds r16, b3
lds r17, c3
lsl r16
lsl r16
lsl r16
lsl r16
add r16,r17
sts d3, r16

; Нахождение суммы в 2-10 Представление результата в r16
lds r16, d1
lds r17, d2
lds r18, d3
andi r16, 0xF0
andi r17, 0xF0
add r16, r17
add r16, r18

nop

;****** Программа decbin_sum_counter
newcount:
mov r17, r16; Если уже 9 единиц
andi r17, 0x0F
cpi r17, 9
breq newten

newinc:
inc r16 ; Если меньше 9 единиц
cpi r16, 153
breq endcount
rjmp newcount

newten:
mov r17, r16
andi r17, 0xF0
swap r17
inc r17
swap r17
mov  r16, r17
rjmp newcount

endcount:

nop

;****** Программа decbin_sub_counter
newcount:
mov r17, r16; Если уже 9 единиц
andi r17, 0x0F
cpi r17, 0
breq newten

newinc:
dec r16
cpi r16, 0
breq endcount
rjmp newcount

newten:
mov r17, r16
andi r17, 0xF0
swap r17
dec r17
swap r17
ori r17, 9
mov  r16, r17
rjmp newcount

endcount:
nop