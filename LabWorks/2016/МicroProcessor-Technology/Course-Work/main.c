#define F_CPU 8000000 // Рабочая частота контроллера
#define BAUD 9600 // Скорость обмена данными
#define F_D F_CPU/255/1024
#define valueOfUBBR F_CPU/BAUD/16-1 //расчет значения UBRR
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <avr/interrupt.h>

void send_int_Uart(unsigned int volt);
void initUART(unsigned int ubrr);
void initADC();
void initInterrupt();
void send(char value);

unsigned int i;
unsigned int j;
unsigned int voltage;
unsigned int i_medVolt;
unsigned int j_medVolt;
unsigned int medVolt = 0;
unsigned int flag = 0;
unsigned int frequency;
unsigned char temp;
unsigned int gulag = 23456;

void send_int_Uart(unsigned int volt) {
    temp = volt / 10000;
    send(temp + '0');
    temp = (volt % 10000) / 1000;
    send(temp + '0');
    temp = (volt % 1000) / 100;
    send(temp + '0');
    temp = (volt % 100) / 10;
    send(temp + '0');
    temp = (volt % 10) / 1;
    send(temp + '0');
}

ISR(TIMER0_OVF_vect) {
    if (flag == 0) {
        i_medVolt = 0;
        i = 0;
    }
    else {
        j_medVolt = 0;
        j = 0;
    }
    ADCSRA|=(1<<ADSC);			//бит начала преобразований
    while( !(ADCSRA & (1<<ADIF)) );  //когда записан преобразование заканчивается
    voltage = ADCW * 5000.00 / 1024.00;  //приводим значение напряжения
    if(flag == 0) {
        i_medVolt += voltage*voltage;
        ++i;
    }
    else {
        j_medVolt += voltage*voltage;
        ++j;
    }
}

int main(void) {
    initUART(valueOfUBBR);
    initADC();
    initInterrupt();
    sei();			//глобальное разрешение прерываний
    while(1) {
        if(voltage < 400) {
            medVolt = i_medVolt + j_medVolt;//sqrt( ( 1/(i+j) ) * (i_medVolt + j_medVolt) );

            send('i');
            send('m');
            send(32);
            send_int_Uart(i_medVolt);
            send(32);

            send('j');
            send('m');
            send(32);
            send_int_Uart(j_medVolt);
            send(32);

            send('m');
            send(32);
            send_int_Uart(medVolt);
            send(32);

            send('i');
            send(32);
            send_int_Uart(i);
            send(32);

            send('j');
            send(32);
            send_int_Uart(j);
            send(32);

            send('f');
            send(32);
            frequency = F_D/(i+j);
            send_int_Uart(frequency);
            send(13);
            if (flag == 0) {
                flag = 1;
            }
            else {
                flag = 0;
            }
        }
    }
}

void initUART(unsigned int ubrr) {
    UBRR0L = (unsigned char) ubrr;		//Младшие 8 бит UBRRL_value
    UBRR0H = (unsigned char) (ubrr>>8); //Старшие 8 бит UBRRL_value
    UCSR0B |=(1<<TXEN0)|(1<<RXEN0);		//Бит разрешения передачи
    UCSR0C |=(1<< UCSZ00)|(1<< UCSZ01); //Устанавливем формат 8 бит данных
}
void initADC() {
    ADMUX|=(1<<REFS0);			//Опорное напряжение AVCC (+5в)
    ADCSRA|=(1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADATE); //Включене АЦП, деление частоты на 64
    ADCSRB|=(1<<ADTS2)|(1<<ADTS1); //по переполнению таймера
}

void initInterrupt() {
    TCCR0B|=(1<<CS02)|(1<<CS00); //старт таймера 0, частота /1024
    TIMSK0|=(1<<TOIE0); //разрешение прерываний по переполнению таймера 0
}

void send(char value) {
    //Отправка символа
    while(!( UCSR0A & (1 << UDRE0))); // Ожидаем когда очистится буфер
    UDR0 = value;
}

send('f');
send('l');
send(32);
send_int_Uart(flag);
send(32);

#define F_CPU 8000000 // Рабочая частота контроллера
#define BAUD 9600 // Скорость обмена данными
#define F_D F_CPU/255
#define valueOfUBBR F_CPU/BAUD/16-1 //расчет значения UBRR
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <avr/interrupt.h>

void send_int_Uart(unsigned int volt);
void initUART(unsigned int ubrr);
void initADC();
void initInterrupt();
void send(char value);

void send_int_Uart(unsigned int volt) {
    unsigned char temp;
    temp = volt / 10000;
    send(temp + '0');
    temp = (volt % 10000) / 1000;
    send(temp + '0');
    temp = (volt % 1000) / 100;
    send(temp + '0');
    temp = (volt % 100) / 10;
    send(temp + '0');
    temp = (volt % 10) / 1;
    send(temp + '0');
}

unsigned int i;
unsigned int j;
float voltage;
float i_medVolt;
float j_medVolt;
unsigned int medVolt;
unsigned int flag = 0;
unsigned int frequency;
ISR(TIMER0_OVF_vect) {
    ADCSRA|=(1<<ADSC);			//бит начала преобразований
    while( !(ADCSRA & (1<<ADIF)) );  //когда запись преобразование заканчивается
    voltage = ADCW;  //приводим значение напряжения
    if(flag == 0) {
        i_medVolt += voltage*voltage;
        ++i;
    }
    else {
        j_medVolt += voltage*voltage;
        ++j;
    }
    if(voltage < 10) {
        medVolt = sqrtf( (i_medVolt + j_medVolt) / (i+j) ) * (5000 / 1024) / 6 * 100 * 1.06;
        send('i');
        send('m');
        send(32);
        send_int_Uart(i_medVolt);
        send(32);

        send('j');
        send('m');
        send(32);
        send_int_Uart(j_medVolt);
        send(32);

        send('m');
        send(32);
        send_int_Uart(medVolt);
        send(32);

        send('i');
        send(32);
        send_int_Uart(i);
        send(32);

        send('j');
        send(32);
        send_int_Uart(j);
        send(32);

        send('f');
        send(32);
        frequency = F_D/(i+j) * 0.3;
        send_int_Uart(frequency);
        send(32);

        send(13);
        if (flag == 0) {
            j_medVolt = 0;
            j = 0;
            flag = 1;
        }
        else {
            i_medVolt = 0;
            i = 0;
            flag = 0;
        }
    }
}

int main(void) {
    initUART(valueOfUBBR);
    initADC();
    initInterrupt();
    sei();			//глобальное разрешение прерываний
    while(1) {
    }
}

void initUART(unsigned int ubrr) {
    UBRR0L = (unsigned char) ubrr;		//Младшие 8 бит UBRRL_value
    UBRR0H = (unsigned char) (ubrr>>8); //Старшие 8 бит UBRRL_value
    UCSR0B |=(1<<TXEN0)|(1<<RXEN0);		//Бит разрешения передачи
    UCSR0C |=(1<< UCSZ00)|(1<< UCSZ01); //Устанавливем формат 8 бит данных
}
void initADC() {
    ADMUX|=(1<<REFS0);			    //Опорное напряжение AVCC (+5в)
    ADCSRA|=(1<<ADEN)|(1<<ADATE);   //Включене АЦП, деление частоты на 2
    ADCSRB|=(1<<ADTS2)|(1<<ADTS1);  //по переполнению таймера
}

void initInterrupt() {
    TCCR0B|=(1<<CS00);  //старт таймера 0
    TIMSK0|=(1<<TOIE0); //разрешение прерываний по переполнению таймера 0
}

void send(char value) {
    //Отправка символа
    while(!( UCSR0A & (1 << UDRE0))); // Ожидаем когда очистится буфер
    UDR0 = value;
}

