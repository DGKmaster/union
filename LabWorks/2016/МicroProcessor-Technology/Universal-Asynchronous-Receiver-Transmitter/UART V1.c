#define F_CPU 8000000 // Рабочая частота контроллера
#define BAUD 9600 // Скорость обмена данными
#define F_D F_CPU/255
#define valueOfUBBR F_CPU/BAUD/16-1 //расчет значения UBRR
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <avr/interrupt.h>

void send_int_Uart(unsigned int volt);
void initUART(unsigned int ubrr);
void initADC();
void initInterrupt();
void send(char value);

void send_int_Uart(unsigned int volt) {
    unsigned char temp;
    temp = volt / 10000;
    send(temp + '0');
    temp = (volt % 10000) / 1000;
    send(temp + '0');
    temp = (volt % 1000) / 100;
    send(temp + '0');
    temp = (volt % 100) / 10;
    send(temp + '0');
    temp = (volt % 10) / 1;
    send(temp + '0');
}

unsigned int i;
float voltage;
float allVolt;
unsigned int medVolt;
unsigned int frequency;
ISR(TIMER0_OVF_vect) {
    ADCSRA|=(1<<ADSC);			//бит начала преобразований
    while( !(ADCSRA & (1<<ADIF)) );  //когда запись преобразование заканчивается
    voltage = ADCW;  //приводим значение напряжения
    if(voltage > 3) {
        allVolt += voltage*voltage;
        ++i;
    }
    if( (voltage < 3) && (i > 10) && (allVolt > 10000) ) {
        medVolt = sqrtf( allVolt / i ) * (5000 / 1024) * 100 / 16.3;
        send('a');
        send('v');
        send(32);
        send_int_Uart(allVolt);
        send(32);

        send('m');
        send(32);
        send_int_Uart(medVolt);
        send(32);

        send('i');
        send(32);
        send_int_Uart(i);
        send(32);

        send('f');
        send(32);
        frequency = F_D/(i*9.36);
        send_int_Uart(frequency);
        send(32);

        send(13);
        allVolt = 0;
        i = 0;
    }
}

int main(void) {
    initUART(valueOfUBBR);
    initADC();
    initInterrupt();
    sei();			//глобальное разрешение прерываний
    while(1) {
    }
}

void initUART(unsigned int ubrr) {
    UBRR0L = (unsigned char) ubrr;		//Младшие 8 бит UBRRL_value
    UBRR0H = (unsigned char) (ubrr>>8); //Старшие 8 бит UBRRL_value
    UCSR0B |=(1<<TXEN0)|(1<<RXEN0);		//Бит разрешения передачи
    UCSR0C |=(1<< UCSZ00)|(1<< UCSZ01); //Устанавливем формат 8 бит данных
}
void initADC() {
    ADMUX|=(1<<REFS0);			    //Опорное напряжение AVCC (+5в)
    ADCSRA|=(1<<ADEN)|(1<<ADATE);   //Включене АЦП, деление частоты на 2
    ADCSRB|=(1<<ADTS2)|(1<<ADTS1);  //по переполнению таймера
}

void initInterrupt() {
    TCCR0B|=(1<<CS00);  //старт таймера 0
    TIMSK0|=(1<<TOIE0); //разрешение прерываний по переполнению таймера 0
}

void send(char value) {
    //Отправка символа
    while(!( UCSR0A & (1 << UDRE0))); // Ожидаем когда очистится буфер
    UDR0 = value;
}

