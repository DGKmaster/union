#define F_CPU 8000000 // Рабочая частота контроллера
#define BAUD 9600 // Скорость обмена данными
#define UBRRL_value F_CPU/BAUD/16-1 //Согласно заданной скорости 
#include <avr/io.h>                 //подсчитываем значение для регистра UBRR
#include <util/delay.h>
unsigned char value;
char text[] = {};
void init_USART(unsigned int ubrr)
{
	UBRR0L = (unsigned char) ubrr;       //Младшие 8 бит UBRRL_value
	UBRR0H = (unsigned char) (ubrr>>8);  //Старшие 8 бит UBRRL_value
	UCSR0B |=(1<<TXEN0)|(1<<RXEN0);         //Разрешение передачи и приёма
	UCSR0C |=(1<<UCSZ00)|(1<<UCSZ01); //Устанавливем формат 8 бит данных
}
void send_Uart(unsigned char c)
{
	while(!(UCSR0A&(1<<UDRE0)));
	UDR0 = c;
}
unsigned char getch_Uart(void)
{
	while(!(UCSR0A&(1<<RXC0)));
	return UDR0;
}
void send_Uart_str(unsigned char *s)
{
	while (*s != 0) send_Uart(*s++);
}
int main(void)
{
	init_USART(UBRRL_value); //инициализация USART
	int i = 0;
	while(1)
	{
		if(UCSR0A&(1<<RXC0))
		{
			send_Uart(getch_Uart());
			text[i]=UDR0;
			i++;
		}
		if(UDR0==0x54) break;
	}
	send_Uart(13);
	for (int i = 20; i >= 0; --i)
	{
		send_Uart(text[i]);
		_delay_ms(50);
	}
	_delay_ms(200);
	send_Uart(13);
}