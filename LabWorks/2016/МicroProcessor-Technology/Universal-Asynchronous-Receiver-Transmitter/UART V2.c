#define F_CPU 8000000 // Clock Speed
#define BAUD 9600
#define UBRRL_value F_CPU/16/BAUD-1
#include <avr/io.h>         
#include <util/delay.h>
void USART_Init( unsigned int ubrr)
{
	/*Set baud rate */
	UBRR0H = (unsigned char)(ubrr>>8);
	UBRR0L = (unsigned char)ubrr;
	/*Enable receiver and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/* ”станавливем формат 8 бит данных */
	UCSR0C |=(1<<UCSZ00)|(1<<UCSZ01);
}
void USART_Transmit( unsigned char data )
{
	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) )
	;
	/* Put data into buffer, sends the data */
	UDR0 = data;
}
unsigned char USART_Receive( void )
{
	/* Wait for data to be received */
	while ( !(UCSR0A & (1<<RXC0)) )
	;
	/* Get and return received data from buffer */
	return UDR0;
}
int main( void )
{
	USART_Init(UBRRL_value);
	int text[] = {}, i = 0;
	char answer[] = "cat";
	char welcome[] = "Input password";
	char correct[] = "Correct password!";
	char incorrect[] = "Incorrect password!";
	int flag = 1;
	//Welcome
	for(int j = 0; j < (sizeof(welcome)/sizeof(welcome[0])); j++)
	{
		USART_Transmit( welcome[j] );
	}
	USART_Transmit( 0x0D );
	//Receiving password
	while(1) 
	{
		text[i] = USART_Receive();
		if(text[i] == 0x0D)
		{
			break;
		}
		if( text[i] != answer[i])
		{
			flag = 0;
		}
		USART_Transmit( text[i] );
		i++;
	}
	USART_Transmit( 0x0D );
	//Correct password
	if(flag == 1)
	{
		for(int j = 0; j < (sizeof(correct)/sizeof(correct[0])); j++)
		{
			USART_Transmit( correct[j] );
		}
	}
	//Incorrect password
	else
	{
		for(int j = 0; j < (sizeof(incorrect)/sizeof(incorrect[0])); j++)
		{
			USART_Transmit( incorrect[j] );
		}
	}
}