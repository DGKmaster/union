import pandas as pd
from sklearn import linear_model


# Load data from CSV format
data_train = pd.read_csv('data/candy-data.csv', delimiter=';', decimal=',')
data_test = pd.read_csv('data/candy-test.csv', delimiter=';', decimal=',')


# Multivariate Linear Regression
##########################################################################
print('\n \n LINEAR CASE  \n')

# Set input columns names
x_names_lin = ['chocolate', 'fruity', 'caramel', 'peanutyalmondy', 'nougat', 'crispedricewafer', 'hard', 'bar',
                  'pluribus', 'sugarpercent', 'pricepercent']
# Set output columns names
y_names_lin = 'winpercent'

# Get train data frames
x_train_lin = data_train[x_names_lin]
y_train_lin = data_train[y_names_lin]

# Get test data frames
x_test_lin = data_test[x_names_lin]
y_test_lin = data_test['Y']

# Fit linear regression model
solver_lin = linear_model.LinearRegression()
model_lin = solver_lin.fit(x_train_lin, y_train_lin)

# Make a prediction on test set
prediction_lin = model_lin.predict(x_test_lin)

# Write formatted output
for i in range(len(prediction_lin)):
    # Get Y value based on 50% offset
    if prediction_lin[i] > 50:
        Y_value = 1
    else:
        Y_value = 0
    print('Win percent: %3f | Y predict: %3d | Y test: %3d' % (prediction_lin[i], Y_value, y_test_lin[i]))

# Print average sum of predicted win percent
print('Average: ', sum(prediction_lin) / len(prediction_lin))

# Get all theta parameters
print('Params: ', solver_lin.coef_)
##########################################################################


# Logistic Regression
##########################################################################
print('\n \n LOGISTIC CASE  \n')

# Set input columns names
x_names_log = ['chocolate', 'fruity', 'caramel', 'peanutyalmondy', 'nougat', 'crispedricewafer', 'hard', 'bar',
                  'pluribus', 'sugarpercent', 'pricepercent']
# Set output columns names
y_names_log = 'Y'

# Get train data frames
x_train_log = data_train[x_names_log]
y_train_log = data_train[y_names_log]

# Get test data frames
x_test_log = data_test[x_names_log]

# Fit linear regression model
solver_log = linear_model.LogisticRegression().fit(x_train_log, y_train_log)
model_log = solver_log.fit(x_train_log, y_train_log)

# Make a prediction on test set
print(model_log.predict(x_test_log))
##########################################################################
