import math
import hashlib


# Calculate Fibonacci sequence
def F(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return F(n-1)+F(n-2)


# Get password from input
input_psk = input()

# Form list of password characters
# to modify case of one letter
new = list(input_psk)

# If last number (a) is digit - calculate x
if input_psk[-1].isdigit():
    last_number = int(input_psk[-1])
    # x = a! - F(a)
    x = math.factorial(last_number) - F(last_number)
    # Add x to right
    input_psk = input_psk + str(x)
# Else if it is char - change case
elif input_psk[-1].isupper():
    new[-1] = new[-1].lower()
    input_psk = ''.join(new)
elif input_psk[-1].islower():
    new[-1] = new[-1].upper()
    input_psk = ''.join(new)

# Calculate
digits_num = 0
digits_sum = 0

for i in input_psk:
    if i.isdigit():
        digits_num += 1
        digits_sum += int(i)

process_psk = ''

if digits_num == 0:
    process_psk = '@' + input_psk
else:
    process_psk = str(math.ceil(digits_sum/digits_num)) + input_psk

output_psk = hashlib.md5()
output_psk.update(process_psk.encode())

print(output_psk.hexdigest())
