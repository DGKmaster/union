import ipaddress

# Get IP address from input
input_ip = input()
# Get net mask from input
input_mask = input()

# Form net parameters object
net = ipaddress.ip_network(input_ip + '/' + input_mask, strict=False)

# Print mask value
print(str(net.network_address))
