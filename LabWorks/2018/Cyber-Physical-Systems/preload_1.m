%% Task 1. Input-State-Output Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Matrix 2x2
A = [0 1; -10*rand() -10*rand()];

%% Matrix 2x1
B = [0; 10*rand()];

%% Matrix 1x2
C = [1 0];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Task 2. Get system poles for ISO case
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
poles_ISO = eig(A);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Task 3. Modify ISO to IO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create IO system transfer function object
state_space_system = ss(A,B,C,0);
transfer_function = tf(state_space_system);

%% Get system poles for IO case
poles_IO = pole(transfer_function);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Task 5. Parameter identification
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Y1 = 1;
Y2 = 1;

A_est_init = [0 1; A(2,1)*(1.5 - rand()) A(2,2)*(1.5 - rand())];
B_est_init = [0; B(2,1)*(1.5 - rand())];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%