%% Task 1. Create a stable system in IO form
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create IO system transfer function object
tf_fn = tf([0 1], [1 5 6]);

%% Get system poles for IO case
poles_IO = pole(tf_fn);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Task 2. Transfer the system to ISO form
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ss_sys = ss(tf_fn);

%% Matrix 2x2
A = ss_sys.A;

%% Matrix 2x1
B = ss_sys.B;

%% Matrix 1x2
C = ss_sys.C;

poles_ISO = eig(ss_sys.A);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Task 3. Calculate K
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
syms k_1 k_2 s;

A_0 = A - [k_1; k_2]*C;

eqn = coeffs(det(s*eye(2) - A_0) - (s + 6)*(s + 8), s);
eqn_1 = eqn(1) == 0;
eqn_2 = eqn(2) == 0;

ans_K = solve(eqn_1, eqn_2, k_1, k_2);
K = double([ans_K.k_1; ans_K.k_2]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

