import Jama.Matrix;
import Jama.QRDecomposition;
import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;


/**
 *  The {@code MultipleLinearRegression} class performs a multiple linear regression
 *  on an set of <em>N</em> data points using the model
 *  <em>y</em> = &beta;<sub>0</sub> + &beta;<sub>1</sub> <em>x</em><sub>1</sub> + ... + 
 &beta;<sub><em>p</em></sub> <em>x<sub>p</sub></em>,
 *  where <em>y</em> is the response (or dependent) variable,
 *  and <em>x</em><sub>1</sub>, <em>x</em><sub>2</sub>, ..., <em>x<sub>p</sub></em>
 *  are the <em>p</em> predictor (or independent) variables.
 *  The parameters &beta;<sub><em>i</em></sub> are chosen to minimize
 *  the sum of squared residuals of the multiple linear regression model.
 *  It also computes the coefficient of determination <em>R</em><sup>2</sup>.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class MultipleLinearRegression {
    private final Matrix beta;  // regression coefficients
    private double sse;         // sum of squared
    private double sst;         // sum of squared

    /**
     * Performs a linear regression on the data points {@code (y[i], x[i][j])}.
     * @param  x the values of the predictor variables
     * @param  y the corresponding values of the response variable
     * @throws IllegalArgumentException if the lengths of the two arrays are not equal
     */
    public MultipleLinearRegression(double[][] x, double[] y) {
        if (x.length != y.length) {
            throw new IllegalArgumentException("matrix dimensions don't agree");
        }

        // number of observations
        int n = y.length;

        Matrix matrixX = new Matrix(x);

        // create matrix from vector
        Matrix matrixY = new Matrix(y, n);

        // find least squares solution
        QRDecomposition qr = new QRDecomposition(matrixX);
        beta = qr.solve(matrixY);


        // mean of y[] values
        double sum = 0.0;
        for (int i = 0; i < n; i++)
            sum += y[i];
        double mean = sum / n;

        // total variation to be accounted for
        for (int i = 0; i < n; i++) {
            double dev = y[i] - mean;
            sst += dev*dev;
        }

        // variation not accounted for
        Matrix residuals = matrixX.times(beta).minus(matrixY);
        sse = residuals.norm2() * residuals.norm2();

    }

    /**
     * Returns the least squares estimate of &beta;<sub><em>j</em></sub>.
     *
     * @param  j the index
     * @return the estimate of &beta;<sub><em>j</em></sub>
     */
    public double beta(int j) {
        return beta.get(j, 0);
    }

    /**
     * Returns the coefficient of determination <em>R</em><sup>2</sup>.
     *
     * @return the coefficient of determination <em>R</em><sup>2</sup>,
     *         which is a real number between 0 and 1
     */
    public double R2() {
        return 1.0 - sse/sst;
    }

    // Path to .csv file
    private static final String CSV_FILE_PATH = "./src/main/resources/candy-data.csv";

    /**
     * Unit tests the {@code MultipleLinearRegression} data type.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) throws IOException {
        /// Max elements
        final int MAX_NUMBER = 71;
        final int NUM_COLUMNS = 16;

        /// Number of elements
        int number = 0;

        try (
                // Set a file stream
                Reader reader = Files.newBufferedReader(Paths.get(CSV_FILE_PATH));
                CSVReader csvReader = new CSVReader(reader, ';')
        ) {
            // Get elements from .csv file
            String[] nextRecord;

            double[][] x = new double[MAX_NUMBER][NUM_COLUMNS];
            double[] y = new double[MAX_NUMBER];
            double[][] x_y = new double[MAX_NUMBER][NUM_COLUMNS];

            System.out.println(Arrays.toString(csvReader.readNext()));

            /// While elements are still presented
            while ((nextRecord = csvReader.readNext()) != null) {

                for(int i = 0; i < nextRecord.length; i++) {
                    nextRecord[i] = nextRecord[i].replaceAll(",", ".");
                }
//                System.out.println(Arrays.toString(nextRecord));

//                x_y[number] = Arrays.stream(Arrays.copyOfRange(nextRecord, 1, nextRecord.length - 2))
//                        .mapToDouble(Double::parseDouble)
//                        .toArray();
                for(int i = 1; i < nextRecord.length - 2; i++) {
                    x_y[number][i] = Double.valueOf(nextRecord[i]);
//                    System.out.println(nextRecord[i]);
//                    System.out.println(Double.valueOf(nextRecord[i]));
                }

                x[number] = Arrays.copyOfRange(x_y[number], 0, x_y[number].length - 1);
                y[number] = x_y[number][x_y[number].length - 1];

                number++;
//                System.out.println(number + ") " + x_y[number][3] + " " + Arrays.toString(x_y[number]) + Arrays.toString(x[number]));
                  System.out.println(number + ") " + x[number].length);
            }
            System.out.println(x.length);
//            double[][] x = {{1, 10, 20},
//                    {1, 20, 40},
//                    {1, 40, 15},
//                    {1, 80, 100},
//                    {1, 160, 23},
//                    {1, 200, 18}};
//            double[] y = {243, 483, 508, 1503, 1764, 2129};

            try {
                MultipleLinearRegression regression = new MultipleLinearRegression(x, y);

                System.out.format("%.2f + %.2f beta1 + %.2f beta2  (R^2 = %.2f)\n",
                        regression.beta(0), regression.beta(1), regression.beta(2), regression.R2());
            }
            catch(Exception e) {
                System.out.println(e.toString());
            }

        }
    }
}