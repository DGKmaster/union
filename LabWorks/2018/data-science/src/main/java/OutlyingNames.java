// Parse .csv file
import com.opencsv.CSVReader;

// To work with files and paths
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

// To save matches and to sort them
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OutlyingNames {
    // Path to .csv file
    private static final String CSV_FILE_PATH = "./src/main/resources/boys.csv";

    // Show all columns of the table row in the simple format
    private static void showLine(String[] inputString) {
        // Go through all columns
        for (String partString : inputString) {
            System.out.print(partString + " ");
        }
        // Finish with new line
        System.out.println();
    }

    public static void main(String[] args) throws IOException {
        try (
                // Set a file stream
                Reader reader = Files.newBufferedReader(Paths.get(CSV_FILE_PATH));
                CSVReader csvReader = new CSVReader(reader, ';')
        ) {
            // Read the first row - the task
            String[] nextRecord = csvReader.readNext();
            showLine(nextRecord);
            nextRecord = csvReader.readNext();
            showLine(nextRecord);

            // Store born people numbers
            List<Integer> people = new ArrayList<>();

            // Read the file until it ends
            while ((nextRecord = csvReader.readNext()) != null) {
                // Search for the necessary name
                if (nextRecord[0].replaceAll("\\s+", "").equals("DANIEL")) {
                    // Push the value to the list
                    for(int i = 1; i < nextRecord.length; i++) {
                        people.add(Integer.parseInt(nextRecord[i].replaceAll("\\s+", "")));
                    }
                    showLine(nextRecord);
                    break;
                }
            }
            // Ascending sort of all values
            Collections.sort(people);

            // Search the median and its index
            int medianIndex = (int) Math.ceil((double) people.size() / 2);
            int median = people.get(medianIndex);

            // Print the list as two rows
            System.out.println(people.subList(0, medianIndex));
            System.out.println(people.subList(medianIndex, people.size()));

            // Search the first quartile and its index
            int firstQIndex = (int) Math.ceil((double) people.size() / 4);
            int firstQ = people.get(firstQIndex);

            // Search the third quartile and its index
            int thirdQIndex = (int) Math.ceil((double) people.size() * 3 / 4);
            int thirdQ = people.get(thirdQIndex);

            // Get the range of series
            int range = thirdQ - firstQ;

            // Find outliers
            if((1.5*range + thirdQ) < people.get(people.size() - 1)) {
                System.out.println("The upper boundary is NOT OK");
            } else {
                System.out.println("The upper boundary is OK");
            }
            if((firstQ - 1.5*range) > people.get(0)) {
                System.out.println("The lower boundary is NOT OK");
            } else {
                System.out.println("The lower boundary is OK");
            }

            // Print all debug information
            System.out.println("List size: " + people.size());
            System.out.println("Median Index: " + medianIndex +
                    " Median: " + median
            );
            System.out.println("First Q Index: " + firstQIndex +
                    " First Q: " + firstQ
            );
            System.out.println("Third Q Index: " + thirdQIndex +
                    " Third Q: " + thirdQ
            );
            System.out.println("Range: " + range);
        }
    }
}