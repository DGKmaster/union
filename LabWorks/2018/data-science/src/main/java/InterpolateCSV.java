// Parse .csv file
import com.opencsv.CSVReader;

// To work with files and paths
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class InterpolateCSV {
    // Path to .csv file
    private static final String CSV_FILE_PATH = "./src/main/resources/Task_1.csv";

    private static int stringToTime(String inputString) {
        return Integer.parseInt(inputString.substring(0, 2)) * 60 * 60 +
                Integer.parseInt(inputString.substring(3, 5)) * 60 +
                Integer.parseInt(inputString.substring(6, 8));
    }

    public static void main(String[] args) throws IOException {
        try (
                // Set a file stream
                Reader reader = Files.newBufferedReader(Paths.get(CSV_FILE_PATH));
                CSVReader csvReader = new CSVReader(reader, ',')
        ) {
            // Store record row
            String[] nextRecord;

            // Read the first row - the task
            nextRecord = csvReader.readNext();
            System.out.println(nextRecord[0]);

            // Read station names and print them out
            nextRecord = csvReader.readNext();
            System.out.println(nextRecord[0] + " " +
                    nextRecord[1] + " " +
                    nextRecord[2] + " " +
                    nextRecord[3] + " " +
                    nextRecord[4]);

            // Show the table row
            int iteration = 2;

            // To get time difference
            String[] previousRecord = {};

            // Read the file until it ends
            while ((nextRecord = csvReader.readNext()) != null) {
                ++iteration;

                // If the time is missing calculate it
                if (nextRecord[4].isEmpty()) {
                    int meanTime = 0;

                    // For other 3 time values
                    for (int i = 1; i < 4; i++) {
                        // Parse current time value in seconds
                        int next_time = stringToTime(nextRecord[i]);

//                        System.out.println(nextRecord[i].substring(0, 2) + " " +
//                                nextRecord[i].substring(3, 5) + " " +
//                                nextRecord[i].substring(6, 8));

                        // Parse previous time value in seconds
                        int previousTime = stringToTime(previousRecord[i]);

                        // Calculate time difference and add it to the mean value (firstly it is only a sum)
                        int diffTime = next_time - previousTime;
                        meanTime += diffTime;
                        //System.out.println("Diff time: " + diffTime);
                    }

                    // Calculate mean value
                    meanTime /= 3;
                    //System.out.println("Mean time: " + meanTime);

                    // Get the missing value by getting previous time in seconds and adding mean time
                    int time = stringToTime(previousRecord[4]) + meanTime;

                    // Format to HH:MM:SS
                    int hours = time / (60 * 60);
                    int minutes = time / 60 - hours * 60;
                    int seconds = time - hours * 60 * 60 - minutes * 60;

                    String minutesString;
                    String secondsString;

                    // If the value less than 10 the additional 0 is needed to get a two-digit value
                    if (minutes < 10) {
                        minutesString = "0" + String.valueOf(minutes);
                    } else {
                        minutesString = String.valueOf(minutes);
                    }
                    if (seconds < 10) {
                        secondsString = "0" + String.valueOf(seconds);
                    } else {
                        secondsString = String.valueOf(seconds);
                    }

                    // Format string
                    nextRecord[4] = String.valueOf(hours) + ":" +
                            minutesString + ":" +
                            secondsString;

                    // Show result string
                    System.out.println("Index: " + iteration + " Value: " + nextRecord[4]);
                }

                previousRecord = nextRecord;
            }
        }
    }
}