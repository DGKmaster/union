import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;

public class LinearRegression {

    // Path to .csv file
    private static final String CSV_FILE_PATH = "./src/main/resources/Task_2.csv";

    public static void main(String[] args) throws IOException {
        /// Max elements
        final int MAX_NUMBER = 1000;
        /// Number of elements
        int number = 0;
        /// Array for storing X and Y elements
        double[] x = new double[MAX_NUMBER];
        double[] y = new double[MAX_NUMBER];

        // Read data
        double x_sum = 0.0, y_sum = 0.0, x2_sum = 0.0;

        try (
                // Set a file stream
                Reader reader = Files.newBufferedReader(Paths.get(CSV_FILE_PATH));
                CSVReader csvReader = new CSVReader(reader, ',');
        ) {
            // Get elements from .csv file
            String[] nextRecord;

            /// While elements are still presented
            while ((nextRecord = csvReader.readNext()) != null) {
                /// Parse X and Y number
                x[number] = Double.parseDouble(nextRecord[1].substring(nextRecord[1].indexOf('=') + 2));
                y[number] = Double.parseDouble(nextRecord[2].substring(nextRecord[1].indexOf('=') + 2));
                /// Print parsed value for debug purposes
                System.out.println("x: " + Double.toString(x[number]) + ", y: " + Double.toString(y[number]));

                /// Calculate all sums and X squared sum
                x_sum += x[number];
                x2_sum += x[number] * x[number];
                y_sum += y[number];
                /// Increment iterator
                number++;
            }
            /// Get X and Y mean values
            double x_mean = x_sum / number;
            double y_mean = y_sum / number;
            System.out.println("x_mean: " + Double.toString(x_mean) + ", y_mean: " + Double.toString(y_mean));

            // Compute squared differences
            double xx_diff2 = 0.0, yy_diff2 = 0.0, xx_yy_diff = 0.0;
            for (int i = 0; i < number; i++) {
                xx_diff2 += (x[i] - x_mean) * (x[i] - x_mean);
                yy_diff2 += (y[i] - y_mean) * (y[i] - y_mean);
                xx_yy_diff += (x[i] - x_mean) * (y[i] - y_mean);
            }
            System.out.println("xx_diff2: " + Double.toString(xx_diff2) +
                               ", yy_diff2: " + Double.toString(yy_diff2) +
                               ", xx_yy_diff: " + Double.toString(xx_yy_diff));

            /// Get beta parameters values
            double beta_1 = xx_yy_diff / xx_diff2;
            DecimalFormat f = new DecimalFormat("##.000");
            beta_1 = Double.parseDouble(f.format(beta_1));
            double beta_0 = y_mean - beta_1 * x_mean;
            System.out.println("y = " + beta_1 + " * x + " + beta_0);

            // Analyze results
            int number_min_2 = number - 2;
            double rss = 0.0;      // residual sum of squares
            double ssr = 0.0;      // regression sum of squares
            for (int i = 0; i < number; i++) {
                double y_calc = beta_1 * x[i] + beta_0;
                rss += (y_calc - y[i]) * (y_calc - y[i]);
                ssr += (y_calc - y_mean) * (y_calc - y_mean);
            }

            double R2 = ssr / yy_diff2;
            double svar = rss / number_min_2;
            double beta_1_err2 = svar / xx_diff2;
            double beta_0_err2 = svar / number + x_mean * x_mean * beta_1_err2;

            System.out.println("R^2                 = " + R2);
            System.out.println("std error of beta_1 = " + Math.sqrt(beta_1_err2));
            System.out.println("std error of beta_0 = " + Math.sqrt(beta_0_err2));
            beta_0_err2 = svar * x2_sum / (number * xx_diff2);
            System.out.println("std error of beta_0 = " + Math.sqrt(beta_0_err2));

            System.out.println("SSTO = " + yy_diff2);
            System.out.println("SSE (residual sum of squares)   = " + rss);
            System.out.println("SSR (regression sum of squares) = " + ssr);
        }
    }
}