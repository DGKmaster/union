# Data Science Laboratories

Solutions for simple data science tasks such as fill in gaps or linear regression.
The most important files located in ```src/main/java```.

---

## Project structure

* **InterpolateCSV**
  * Get transport system schedule with gaps.
  * Calculate average time between stops.
  * Fill in gaps.
* **LinearRegression**
  * Calculate linear regression model of input CSV data.
* **MultipleLinearRegression**
  * Draft of linear regression model with multiple input paramaters.
* **OutlyingNames**
  * Search for hard-coded name in input CSV file.
  * Calculate the first and third quartiles of people born number for each month with specialised name.
  * Find out if upper or lower boundaries is reached or not.
