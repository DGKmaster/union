import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CryptoMessage {

    public static void main(String[] args) {
        // Public key
        double q = 5;
        double b = 9;
        double p = 19;

        // Message 1 and its signature
        double mes_1 = 7;
        double[] sign_1 = {11, 6};

        // Message 2 and its signature
        double mes_2 = 9;
        double[] sign_2 = {3, 6};

        // Store answers of two equation systems
        List<Double> c_1 = new ArrayList<>();
        List<Double> c_2 = new ArrayList<>();

        // Get solutions of first equation system
        for (double i = 0; i < 200; i++) {
            // Calculate
            double answer = (Math.pow(q, i)) % p;
            if (answer == b) {
                System.out.println("b: " + Double.toString(answer) + " c: " + Double.toString(i));
                c_1.add(i);
            }
        }

        System.out.println();

        // Get solutions of second equation system
        for (double r = 0; r < 300; r++) {
            for (double c = 0; c < 300; c++) {
                // Calculate
                double eq_1 = (Math.pow(q, (sign_1[0] * r + sign_1[1] * c)) % p) - 16;
                double eq_2 = (Math.pow(q, (sign_2[0] * r + sign_2[1] * c)) % p) - 1;
                // If it is solution
                if ((eq_1 == eq_2) && (eq_1 == 0)) {
                    System.out.println("r: " + Double.toString(r) + "" +
                            " c: " + Double.toString(c) +
                            " eq_1: " + Double.toString(eq_1) +
                            " eq_2: " + Double.toString(eq_2));
                    c_2.add(c);
                }
            }
        }

        // Sort answers to find matching
        Collections.sort(c_1);
        Collections.sort(c_2);

        // Print answers
        System.out.println(c_1);
        System.out.println(c_2);
    }
}
