import java.text.DecimalFormat;
import java.util.Scanner;

public class CurrencyFormat {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double payment = scanner.nextDouble();
        scanner.close();

        DecimalFormat df = new DecimalFormat("##,###.##");

        String us = "$" + df.format(payment);
        String india = "Rs." + df.format(payment);
        String china = "￥" + df.format(payment);
        String france = df.format(payment) + " €";
        
        System.out.println("US: " + us);
        System.out.println("India: " + india);
        System.out.println("China: " + china);
        System.out.println("France: " + france.replace(',', ' ').replace('.', ','));
    }
}

