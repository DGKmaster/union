import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class BowlingPins {
    private static final String FILE_PATH = "./src/main/resources/Output.txt";

    private static final Scanner scanner = new Scanner(System.in);

    /*
     * Complete the isWinning function below.
     */
    static String isWinning(int n, String config) {
        String result;
        int moves = 0;
        boolean flag = false;

        for (int i = 0; i < n; i++) {
            if (config.charAt(i) == 'I') {
                if (flag) {
                    flag = false;
                    moves++;
                } else {
                    flag = true;
                }
            } else if (config.charAt(i) == 'X') {
                if (flag) {
                    flag = false;
                    moves++;
                } else {

                }
            }
        }
        if(flag) {
            moves++;
        }

        System.out.println(moves);

        if ((moves % 2) == 1) {
            result = "WIN";
        } else {
            result = "LOSE";
        }

        return result;
    }

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FILE_PATH));

        int t = Integer.parseInt(scanner.nextLine().trim());

        for (int tItr = 0; tItr < t; tItr++) {
            int n = Integer.parseInt(scanner.nextLine().trim());

            String config = scanner.nextLine();

            String result = isWinning(n, config);

            bufferedWriter.write(result);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();
    }
}
