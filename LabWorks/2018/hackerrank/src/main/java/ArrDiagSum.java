import java.io.*;
import java.util.*;
import static java.lang.Math.abs;

public class ArrDiagSum {
    //private static final String FILE_PATH = "./src/main/resources/Output.txt";

    // Complete the diagonalDifference function below.
    static int diagonalDifference(int[][] arr) {
        int difference = 0;

        for(int i = 0; i < arr.length; i++) {
            difference += arr[i][i] - arr[i][arr.length-1 - i];
        }

        return abs(difference);
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FILE_PATH));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[][] arr = new int[n][n];

        for (int i = 0; i < n; i++) {
            String[] arrRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < n; j++) {
                int arrItem = Integer.parseInt(arrRowItems[j]);
                arr[i][j] = arrItem;
            }
        }

        int result = diagonalDifference(arr);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
