import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Locale;

class Result {

    /*
     * Complete the 'findDay' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts following parameters:
     *  1. INTEGER month
     *  2. INTEGER day
     *  3. INTEGER year
     */

    public static String findDay(int month, int day, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        LocalDate date = LocalDate.of(year, month, day);
        DayOfWeek dow = date.getDayOfWeek();

        return dow.getDisplayName(TextStyle.FULL, Locale.US).toUpperCase();
    }

}

public class CalendarWeek {
    public static void main(String[] args){

        int month = 8;

        int day = 5;

        int year = 2015;

        String res = Result.findDay(month, day, year);

        System.out.println(res);
    }
}