import java.util.Scanner;

public class OutFormat {
    private static final int MESSAGE_LENGTH = 15;
    private static final int DIGIT_LENGTH = 3;

    public static void main(String[] args) {
        /// Get input
        Scanner sc = new Scanner(System.in);

        System.out.println("================================");
        for (int i = 0; i < 3; i++) {
            /// To append spaces and 0 digits
            StringBuilder sbSpace = new StringBuilder();

            /// Parse input
            String str_name = sc.next();
            int int_num = sc.nextInt();
            String str_num = Integer.toString(int_num);

            /// Append spaces depending on name length to satisfy
            /// 15 characters requirement
            for(int j = 0; j < (MESSAGE_LENGTH - str_name.length()); j++) {
                sbSpace.append(' ');
            }

            /// Append 0 digit depending on number length to satisfy
            /// 3 characters requirement
            for(int j = 0; j < (DIGIT_LENGTH - str_num.length()); j++) {
                sbSpace.append(0);
            }

            /// Print formatted string
            System.out.println(str_name + sbSpace + str_num);
        }
        System.out.println("================================");
    }
}
