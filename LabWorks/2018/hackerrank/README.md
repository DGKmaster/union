# Hackerrank

Source code for completed HackerRank tasks.

---

## Project structure

* **ArrDiagSum** - Compute differences between element sums of primary and secondary diagonal.
* **BowlingPins** - Calculate if it is possible to win in bowling by received pins config.
* **CalendarWeek** - Print day of week using input day, month and year.
* **CryptoMessage**
* **CurrencyFormat** - Display one value in 4 different formats.
* **LinkedList** - Implement own linked list solution.
* **OutFormat** - Display value in specific format.
* **ReadTypes** - Read int, double and string and print it.
* **TwoDarray** - Find the biggest sum in 2D array of specific figure.