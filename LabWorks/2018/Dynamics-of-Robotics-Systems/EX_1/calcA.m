function output = calcA(theta, d, a, alpha)
    % calcA - Calculate transformation matrix using DH parameters
    %
    % Syntax: output = calcA(theta, d, a, alpha)
    %
    % Using trigonometric functions which accept degrees as in that case
    % sin(0) = 0, not 1.2e-16
    
    output = [cosd(theta*180/pi), -sind(theta*180/pi)*cosd(alpha*180/pi),  sind(theta*180/pi)*sind(alpha*180/pi), a*cosd(theta*180/pi);
              sind(theta*180/pi),  cosd(theta*180/pi)*cosd(alpha*180/pi), -cosd(theta*180/pi)*sind(alpha*180/pi), a*sind(theta*180/pi);
              0,           sind(alpha*180/pi),             cosd(alpha*180/pi),            d;
              0,           0,                      0,                     1
    ];

end