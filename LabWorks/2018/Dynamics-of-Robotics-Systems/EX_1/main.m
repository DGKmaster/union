% Prepare environment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc;
clear;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Set all variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% All links parameters for inertion
% a - length, b - width, c - thickness
syms a_1 a_2 a_3 a_4 real;
syms b_1 b_2 b_3 b_4 real;
syms c_1 c_2 c_3 c_4 real;

% Material density
syms po real;

% Time variable to get deviratives
syms t;

% Gravity vector
g_0 = [0; 0; -9.81];

% Initial linear and rotational velocities
v_0 = 0;
w_0 = 0;

% Describe what axis is actuating
% For DH z is always
z_act = [0; 0; 1];

% Simple zero vector
zero_v = [0; 0; 0];

% Describe link is rotating or translating
% 0 - is rotating
% 1 - is translating
sigma_1 = 0;
sigma_2 = 0;
sigma_3 = 1;
sigma_4 = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Link parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Link accelerations
syms s_dd_1 s_dd_2 s_dd_3 s_dd_4 real;
s_dd = [s_dd_1; s_dd_2; s_dd_3; s_dd_4];

% Link velocities
syms s_d_1 s_d_2 s_d_3 s_d_4 real;
s_d = [s_d_1; s_d_2; s_d_3; s_d_4];

% Link angles or length
syms s_1 s_2 s_3 s_4 real;
s = [s_1; s_2; s_3; s_4];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get all transformation matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Using handmade function A = calcA(theta, d, a, alpha)
A_0_1 = calcA(s_1, 0,   a_1, 0 );
A_1_2 = calcA(s_2, 0,   a_2, pi);
A_2_3 = calcA(0,   s_3, 0,   0 );
A_3_4 = calcA(s_4,   0, 0,   0 );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get all rotation matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rotation matrices from previos CS to next CS
R_0_1 = A_0_1(1:3,1:3);
R_1_2 = A_1_2(1:3,1:3);
R_2_3 = A_2_3(1:3,1:3);
R_3_4 = A_3_4(1:3,1:3);

% Get rotation matrices from base CS to next CS
R_0_2 = R_0_1 * R_1_2;
R_0_3 = R_0_1 * R_1_2 * R_2_3;
R_0_4 = R_0_1 * R_1_2 * R_2_3 * R_3_4;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get vector to coordinate of mass center
% And vector from previos CS to next CS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Link mass center w.r.t. local CS
r_c_1_1 = [-a_1/2; 0; 0];
r_c_2_2 = [-a_2/2; 0; 0];
r_c_3_3 = [0; 0; -s_3/2];
r_c_4_4 = [0; 0; 0];

% Link mass center w.r.t. base CS
r_c_0_1 = A_0_1 * [r_c_1_1; 1];
r_c_0_1 = r_c_0_1(1:3);
r_c_0_2 = A_0_1 * A_1_2 * [r_c_2_2; 1];
r_c_0_2 = r_c_0_2(1:3);
r_c_0_3 = A_0_1 * A_1_2 *A_2_3 * [r_c_3_3; 1];
r_c_0_3 = r_c_0_3(1:3);
r_c_0_4 = A_0_1 * A_1_2 * A_2_3 * A_3_4 * [r_c_4_4; 1];
r_c_0_4 = r_c_0_4(1:3);

% Translating vector to next CS w.r.t. local CS
r_0_1 = [a_1; 0; 0];
r_1_2 = [a_2; 0; 0];
r_2_3 = [0; 0; s_3];
r_3_4 = [0; 0; 0];

% Translating vector to next CS w.r.t. base CS
r_0_2 = A_0_1 * [r_1_2; 1];
r_0_2 = r_0_2(1:3);
r_0_3 = A_0_1 * A_1_2 * [r_2_3; 1];
r_0_3 = r_0_3(1:3);
r_0_4 = A_0_1 * A_1_2 * A_2_3 * [r_3_4; 1];
r_0_4 = r_0_4(1:3);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get linear and rotational velocities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate rotational velocity of next link using
% Handmade function calcW(R, w_current, sigma, s_d, z)
w_1 = calcW(R_0_1, w_0, sigma_1, s_d_1, z_act);
% Calculate linear velocity
% [v_c_new, v_new] = calcV(R, v_current, w_current, r_c, r_i_i, sigma, s_d, z)
[v_c_1, v_1] = calcV(R_0_1, v_0, w_1, r_c_0_1, r_0_1, sigma_1, s_d_1, z_act);

w_2 = calcW(R_1_2, w_1, sigma_2, s_d_2, z_act);
[v_c_2, v_2] = calcV(R_1_2, v_1, w_2, r_c_0_2, r_1_2, sigma_2, s_d_2, z_act);

w_3 = calcW(R_2_3, w_2, sigma_3, s_d_3, z_act);
[v_c_3, v_3] = calcV(R_2_3, v_2, w_3, r_c_0_3, r_2_3, sigma_3, s_d_3, z_act);

w_4 = calcW(R_3_4, w_3, sigma_4, s_d_4, z_act);
[v_c_4, v_4] = calcV(R_2_3, v_3, w_4, r_c_0_4, r_3_4, sigma_4, s_d_4, z_act);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get all inertion matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate inertion of block
syms m_1;
I_1 = diag([m_1/12*(b_1^2 + c_1^2), ... 
            m_1/12*(a_1^2 + c_1^2), ...
            m_1/12*(a_1^2 + b_1^2)]);

syms m_2;
I_2 = diag([m_2/12*(b_2^2 + c_2^2), ... 
            m_2/12*(a_2^2 + c_2^2), ...
            m_2/12*(a_2^2 + b_2^2)]);

syms m_3;
I_3 = diag([m_3/12*(b_3^2 + c_3^2), ... 
            m_3/12*(s_3^2 + c_3^2), ...
            m_3/12*(s_3^2 + b_3^2)]);

m_4 = 0;
I_4 = diag([0, 0, 0]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get kinetic and potential energies
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate kinetic energy of each link
K_1 = 1/2*m_1*(v_c_1'*v_c_1) + 1/2*w_1'*I_1*w_1;
K_2 = 1/2*m_2*(v_c_2'*v_c_2) + 1/2*w_2'*I_2*w_2;
K_3 = 1/2*m_3*(v_c_3'*v_c_3) + 1/2*w_3'*I_3*w_3;
K_4 = 1/2*m_4*(v_c_4'*v_c_4) + 1/2*w_4'*I_4*w_4;
% Sum up all links to get all robot kinetic energy
K = K_1 + K_2 + K_3 + K_4;

% Calculate potential energy of each link
P_1 = m_1*g_0'*r_c_1_1;
P_2 = m_2*g_0'*r_c_2_2;
P_3 = m_3*g_0'*r_c_3_3;
P_4 = m_4*g_0'*r_c_4_4;
% Sum up all link to get all robot potential energy
P = P_1 + P_2 + P_3 + P_4;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get gravity forces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
g_f_1 = diff(P_1, s_1);
g_f_2 = diff(P_2, s_2);
g_f_3 = diff(P_3, s_3);
g_f_4 = diff(P_4, s_4);
g_f = [g_f_1; g_f_2; g_f_3; g_f_4];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get Jacobians
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Joints: revolute, revolute, prismatic, revolute

% Angular Jacobian
% Set columns
J_w_1_c = z_act;
J_w_2_c = z_act;
J_w_3_c = zero_v;
J_w_4_c = z_act;
% Set matrices
J_w_1 = [J_w_1_c, zero_v, zero_v, zero_v];
J_w_2 = [J_w_1_c, J_w_2_c, zero_v, zero_v];
J_w_3 = [J_w_1_c, J_w_2_c, J_w_3_c, zero_v];
J_w_4 = [J_w_1_c, J_w_2_c, J_w_3_c, J_w_4_c];

% Linear Jacobian
% Set columns
J_v_1_c = cross(z_act, (r_0_4 - r_0_1));
J_v_2_c = cross(z_act, (r_0_4 - r_0_2));
J_v_3_c = z_act;
J_v_4_c = cross(z_act, (r_0_4 - r_0_3));
% Set matrices
J_v_1 = [J_v_1_c, zero_v, zero_v, zero_v];
J_v_2 = [J_v_1_c, J_v_2_c, zero_v, zero_v];
J_v_3 = [J_v_1_c, J_v_2_c, J_v_3_c, zero_v];
J_v_4 = [J_v_1_c, J_v_2_c, J_v_3_c, J_v_4_c];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get Inertion matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate inertion matrix using hand made function
% M = calcM(m, J_v, J_w, R, I)
M_1 = calcM(m_1, J_v_1, J_w_1, R_0_1, I_1);
M_2 = calcM(m_2, J_v_2, J_w_2, R_0_2, I_2);
M_3 = calcM(m_3, J_v_3, J_w_3, R_0_3, I_3);
M_4 = calcM(m_4, J_v_4, J_w_4, R_0_4, I_4);
M = M_1 + M_2 + M_3 + M_4;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get Coriolis forces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate Christoffel symbols
C_ijk = sym('C', [4 4 4]);
% And get Coriolis forces matrix
C_jk = sym('C', [4 4]);
C_jk(:,:) = 0;

% Go through each element
for i = 1:4
    for j = 1:4 
        for k = 1:4 
            C_ijk(i,j,k) = 1/2 * (diff(M(k,j), s(i)) + ...
                              diff(M(k,i), s(j)) - ...
                              diff(M(i,j), s(k)));
            C_jk(j,k) = C_jk(j, k) + C_ijk(i,j,k)*s_d(i);
        end
    end
end

% Calculate Coriolis forces
C_f = C_jk*s_d;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get general dynamic equation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t_f = M*s_dd + C_f + g_f;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%