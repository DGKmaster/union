function [v_c_new, v_new] = calcV(R, v_current, w_current, r_c, r_i_i, sigma, s_d, z)
    % calcV - Calculate linear velocity
    %
    % Syntax: [v_c_new, v_new] = calcV(R, v_current, w_current, r_c, r_i_i, sigma, s_d, z)
    %
    % Long description
        
    v_new = R'*(v_current + sigma*s_d*z + cross((w_current + (1-sigma)*s_d*z), r_i_i));
    v_c_new = v_new + cross(w_current, r_c);
end
