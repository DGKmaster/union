function [q, qd, qdd, M, c, taug, tau] = dynamics_calc(q_0, q_f, qd_0, qd_f, qdd_0, qdd_f, t, L, m_, M_, l, I_1, I_2)
%% Usage example

%! Variables:
% q_0 = [0, 0];
% q_f = [1, 2];
% qd_0 = [0, 0];
% qd_f = [0, 0];
% qdd_0 = [0, 0];
% qdd_f = [0, 0];
% t = 0:1e-2:5;
% L = 1;
% m_ = 1;
% M_ = 2;
% l = 0.5;
% I_1 = eye(3);
% I_2 = eye(3);

%! Function call
% [q, qd, qdd, M, c, taug, tau]  = dynamics_calc(q_0, q_f, qd_0, qd_f, qdd_0, qdd_f, t, L, m_, M_, l, I_1, I_2);


%% DH parameters
% First link
d_1 = 0;
a_1 = L;
alpha_1 = 0;
offset_1 = 0;

% Second link
d_2 = 0;
a_2 = 0;
alpha_2 = 0;
offset_2 = 0;

%% Dynamic parameters
% First link
m_1 = m_;
r_1 = [-l; 0; 0];

% Second link
m_2 = M_;
r_2 = [0; 0; 0];

% Gravity vector
g = [0; -9.81; 0];

%% Create robot object
% Create links based on DH parameters.
L_1 = Link('d',d_1, 'a',a_1, 'alpha',alpha_1, 'offset',offset_1, 'm',m_1, 'I',I_1, 'r',r_1);
L_2 = Link('d',d_2, 'a',a_2, 'alpha',alpha_2, 'offset',offset_2, 'm',m_2, 'I',I_2, 'r',r_2);

% Create manipulator based on links.
robot = SerialLink([L_1,L_2], 'name','Flywheel', 'gravity',g);

% Create trajectory
[q, qd, qdd] = jtraj(q_0, q_f, t, qd_0, qd_f);
    
%% Inverse dynamics
% Inertia matrix
M = robot.inertia(q);

% Coriolis matrix
C = robot.coriolis(q, qd);

% Coriolis forces
c = zeros(2, length(t));
for i = 1:length(t)
    c(:,i) = C(:,:,i)*qd(i,:)';
end

% Compute torque
tau = robot.rne(q, qd, qdd);

% Gravity force
taug = robot.gravload(q);

%% Plotting output part
figure(1);

% Position plot
subplot(3, 1, 1);
plot(t, q);
grid on;
ylabel('Position (rad)');
legend('Joint 1','Joint 2')

% Velocity plot
subplot(3,1,2);
plot(t,qd);
grid on;
ylabel('Velocity (rad/s)');
legend('Joint 1','Joint 2')

% Acceleration plot
subplot(3,1,3);
plot(t,qdd);
grid on;
ylabel('Acceleration (rad/s^2)');
legend('Joint 1','Joint 2')

xlabel('Time (s)');

% Joint torque plot
figure(2);
plot(t, tau);
grid on;
xlabel('Time (s)');
ylabel('Joint torque (Nm)')
legend('Joint 1','Joint 2')

% Gravity force
figure(3);
plot(t, taug);
grid on;
xlabel('Time (s)');
ylabel('Gravity torque (Nm)')
legend('Joint 1','Joint 2')

% Coriolis force
figure(4);
plot(t, c);
grid on;
xlabel('Time (s)');
ylabel('Coriolis torque (Nm)');
legend('Joint 1','Joint 2');

% Inertion matrix components.
figure(5);
plot(t, squeeze([M(1,1,:) M(1,2,:), M(2,1,:) M(2,2,:)]));
grid on;
xlabel('Time (s)');
ylabel('Inertia (kgms2)');
legend('M(1,1)','M(1,2)','M(2,1)','M(2,2)');

%% Saving part
% Figures
saveas(figure(1),'Trajectory.png');
saveas(figure(2),'Joint torque.png');
saveas(figure(3),'Gravity force.png');
saveas(figure(4),'Coriolis force.png');
saveas(figure(5),'Inertion matrix.png');

% Variables
save('Output.mat', 'q', 'qd', 'qdd', 'M', 'c', 'taug', 'tau');

end

