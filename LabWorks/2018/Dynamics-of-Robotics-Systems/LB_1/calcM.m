function M = calcM(m, J_v, J_w, R, I)
    % calcM - Calculate one sum of inertion matrix
    %
    % Syntax: M = calcM(m, J_v, J_w, R, I)
    %
    % Long description
        
    M = m .* J_v' * J_v + J_w' * R * I * R' * J_w;
end