function w_new = calcW(R, w_current, sigma, s_d, z)
    % calcW - Calculate rotational velocity
    %
    % Syntax: w_new = calcW(R, w, sigma, s_d, z)
    %
    % Long description
        
    w_new = R'*(w_current + (1-sigma)*s_d*z);

end