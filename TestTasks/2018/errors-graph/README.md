# Errors Graph

Type I and type 2 errors plots of two hypothesis with normal distributions.

## Instructions to run

### Install prerequirments

```bash
~$ sudo apt install python3-tk python3-dev python3-pip
~$ sudo -H pip3 install numpy matplotlib
```

### Run script

```bash
~$ python3 main.py
```
