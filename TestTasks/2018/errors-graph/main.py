# To draw the plot
from matplotlib import pyplot as mp
# To calculate the Gaussian distribution
import numpy as np
# To show errors area
from matplotlib.patches import Polygon


# Calculate the probability density
def gaussian(x, mu, sigma):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sigma, 2.)))


# Set the critical importance factor
alpha = 0.05
# Set random value
x = np.linspace(-8, 8, 120)

# Set H0 distribution parameters
mu_0 = -1
sigma_0 = 3
h_0 = gaussian(x, mu_0, sigma_0)

# Set H1 distribution parameters
mu_1 = 2
sigma_1 = 2
h_1 = gaussian(x, mu_1, sigma_1)

# Lists for storing the type I error points
x_1 = []
error_1 = []

# Lists for storing II error points
x_2 = []
error_2 = []

# Search error points
for i in range(len(x)):
    # Type 1 error
    if h_1[i] > h_0[i] and h_0[i] < alpha:
        x_1.append(x[i])
        error_1.append(h_0[i])
    # Type 2 error
    if h_0[i] > alpha:
        x_2.append(x[i])
        error_2.append(h_1[i])

# Add border values for right plotting
list_1 = [(x_1[0], 0)] + list(zip(x_1, error_1)) + [(x_1[-1], 0)]
list_2 = [(x_2[0], 0)] + list(zip(x_2, error_2)) + [(x_2[-1], 0)]

# Create two polygons to show the errors area
area_1 = Polygon(list_1, facecolor='0.9', edgecolor='0.5')
area_2 = Polygon(list_2, facecolor='27', edgecolor='0.2')

# Create a figure
figure, graph = mp.subplots()

# Plot H0 probability density with red color
graph.plot(x, h_0, 'r', label='H0')
# Plot H1 probability density with blue color
graph.plot(x, h_1, 'b', label='H1')

# Add errors areas
graph.add_patch(area_1)
graph.add_patch(area_2)

# Add a legend for H0 and H1 probability densities
legend = graph.legend(loc='upper left', shadow=True, fontsize='x-large')

# The label for type I error
mp.text(x_1[error_1.index(max(error_1))], error_1[round(len(error_1) / 2)] / 2, 'I',
        horizontalalignment='center', fontsize=30)

# The label for type II error
mp.text(x_2[error_2.index(max(error_2))], error_2[round(len(error_2) / 2)] / 2, 'II',
        horizontalalignment='center', fontsize=30)

# Put labels for axis and the title
mp.xlabel('Random value')
mp.ylabel('Probability density')
mp.title('Type I and type II errors')

# Show the plot
mp.show()
