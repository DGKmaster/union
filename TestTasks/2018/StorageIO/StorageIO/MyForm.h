#pragma once

namespace StorageIO {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			/// Load simple database to save files
			fileName = "File.xml";
			//file.Load(fileName);
			/// Create a database template
			file.LoadXml("<?xml version = \"1.0\" encoding = \"utf-8\"?> <table> <input></input> <output> </output> </table>");
		}

	/// Output database file descriptor
	public: System::Xml::XmlDocument file;
	/// Store the file name
	public: System::String^ fileName;
	/// Length of each object ID
	public: const int ID_LENGTH = 24;
	/// Key to indicate that new ID-Item will be pushed to dictionary
	public: System::String^ KEY_DICT = "! ";

	/// Store object names and ID
	public: System::Collections::Generic::Dictionary<System::String^, System::String^> idName;
	public: System::Collections::Generic::Dictionary<System::String^, System::String^> nameID;

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			/// Save database from RAM to disk
			SaveFile();

			if (components)
			{
				delete components;
			}
		}

	/// Input from user to ����� table
	public: System::Windows::Forms::TextBox^  inputDataLeft;
	/// Input from user to �������� table
	private: System::Windows::Forms::TextBox^  inputDataRight;
	/// Clear all table
	private: System::Windows::Forms::Button^  clearButton;
	/// Show program work information
	private: System::Windows::Forms::Label^  debugLabel;
	/// Design the panels layout
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel;

	/// Just welcome texts
	private: System::Windows::Forms::Label^  messageUpLeft;
	private: System::Windows::Forms::Label^  messageUpRight;

	/// ����� table and its columns
	private: System::Windows::Forms::DataGridView^  dataLeft;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataLeftName;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataLeftNum;

	/// �������� table and its columns
	private: System::Windows::Forms::DataGridView^  dataRight;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataRightName;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataRightNum;

	/// Buttons to send data from input to table
	private: System::Windows::Forms::Button^  enterLeft;
	private: System::Windows::Forms::Button^  enterRight;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->clearButton = (gcnew System::Windows::Forms::Button());
			this->inputDataLeft = (gcnew System::Windows::Forms::TextBox());
			this->tableLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->enterRight = (gcnew System::Windows::Forms::Button());
			this->dataRight = (gcnew System::Windows::Forms::DataGridView());
			this->dataRightName = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->dataRightNum = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->inputDataRight = (gcnew System::Windows::Forms::TextBox());
			this->messageUpRight = (gcnew System::Windows::Forms::Label());
			this->messageUpLeft = (gcnew System::Windows::Forms::Label());
			this->dataLeft = (gcnew System::Windows::Forms::DataGridView());
			this->dataLeftName = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->dataLeftNum = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->debugLabel = (gcnew System::Windows::Forms::Label());
			this->enterLeft = (gcnew System::Windows::Forms::Button());
			this->tableLayoutPanel->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataRight))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataLeft))->BeginInit();
			this->SuspendLayout();
			// 
			// clearButton
			// 
			this->clearButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->clearButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->clearButton->Location = System::Drawing::Point(3, 368);
			this->clearButton->Name = L"clearButton";
			this->clearButton->Size = System::Drawing::Size(229, 27);
			this->clearButton->TabIndex = 0;
			this->clearButton->Text = L"��������";
			this->clearButton->UseVisualStyleBackColor = true;
			this->clearButton->Click += gcnew System::EventHandler(this, &MyForm::clearButton_Click);
			// 
			// inputDataLeft
			// 
			this->inputDataLeft->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->inputDataLeft->CharacterCasing = System::Windows::Forms::CharacterCasing::Upper;
			this->inputDataLeft->Location = System::Drawing::Point(3, 35);
			this->inputDataLeft->Name = L"inputDataLeft";
			this->inputDataLeft->Size = System::Drawing::Size(229, 20);
			this->inputDataLeft->TabIndex = 1;
			//this->inputDataLeft->Enter += gcnew System::EventHandler(this, &MyForm::inputDataLeft_Enter);
			// 
			// tableLayoutPanel
			// 
			this->tableLayoutPanel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->tableLayoutPanel->ColumnCount = 2;
			this->tableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->tableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->tableLayoutPanel->Controls->Add(this->enterRight, 1, 2);
			this->tableLayoutPanel->Controls->Add(this->dataRight, 1, 3);
			this->tableLayoutPanel->Controls->Add(this->inputDataRight, 1, 1);
			this->tableLayoutPanel->Controls->Add(this->messageUpRight, 1, 0);
			this->tableLayoutPanel->Controls->Add(this->inputDataLeft, 0, 1);
			this->tableLayoutPanel->Controls->Add(this->messageUpLeft, 0, 0);
			this->tableLayoutPanel->Controls->Add(this->dataLeft, 0, 3);
			this->tableLayoutPanel->Controls->Add(this->clearButton, 0, 4);
			this->tableLayoutPanel->Controls->Add(this->debugLabel, 1, 4);
			this->tableLayoutPanel->Controls->Add(this->enterLeft, 0, 2);
			this->tableLayoutPanel->Location = System::Drawing::Point(13, 16);
			this->tableLayoutPanel->Name = L"tableLayoutPanel";
			this->tableLayoutPanel->RowCount = 5;
			this->tableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 29)));
			this->tableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 272)));
			this->tableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 33)));
			this->tableLayoutPanel->Size = System::Drawing::Size(470, 398);
			this->tableLayoutPanel->TabIndex = 2;
			this->tableLayoutPanel->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MyForm::tableLayoutPanel_Paint);
			// 
			// enterRight
			// 
			this->enterRight->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->enterRight->Location = System::Drawing::Point(238, 67);
			this->enterRight->Name = L"enterRight";
			this->enterRight->Size = System::Drawing::Size(229, 23);
			this->enterRight->TabIndex = 11;
			this->enterRight->Text = L"��������";
			this->enterRight->UseVisualStyleBackColor = true;
			this->enterRight->Click += gcnew System::EventHandler(this, &MyForm::enterRight_Click);
			// 
			// dataRight
			// 
			this->dataRight->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->dataRight->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
			this->dataRight->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataRight->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(2) {
				this->dataRightName,
					this->dataRightNum
			});
			this->dataRight->Location = System::Drawing::Point(238, 96);
			this->dataRight->Name = L"dataRight";
			this->dataRight->Size = System::Drawing::Size(229, 266);
			this->dataRight->TabIndex = 8;
			// 
			// dataRightName
			// 
			this->dataRightName->HeaderText = L"������������";
			this->dataRightName->Name = L"dataRightName";
			this->dataRightName->Width = 108;
			// 
			// dataRightNum
			// 
			this->dataRightNum->HeaderText = L"����������";
			this->dataRightNum->Name = L"dataRightNum";
			this->dataRightNum->ReadOnly = true;
			this->dataRightNum->Width = 91;
			// 
			// inputDataRight
			// 
			this->inputDataRight->AcceptsReturn = true;
			this->inputDataRight->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->inputDataRight->CharacterCasing = System::Windows::Forms::CharacterCasing::Upper;
			this->inputDataRight->Location = System::Drawing::Point(238, 35);
			this->inputDataRight->Name = L"inputDataRight";
			this->inputDataRight->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->inputDataRight->Size = System::Drawing::Size(229, 20);
			this->inputDataRight->TabIndex = 6;
			this->inputDataRight->TextChanged += gcnew System::EventHandler(this, &MyForm::inputDataRight_TextChanged);
			// 
			// messageUpRight
			// 
			this->messageUpRight->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->messageUpRight->AutoSize = true;
			this->messageUpRight->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->messageUpRight->Location = System::Drawing::Point(238, 0);
			this->messageUpRight->Name = L"messageUpRight";
			this->messageUpRight->Size = System::Drawing::Size(229, 32);
			this->messageUpRight->TabIndex = 3;
			this->messageUpRight->Text = L"���� ����� ������ ��� ��������";
			this->messageUpRight->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// messageUpLeft
			// 
			this->messageUpLeft->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->messageUpLeft->AutoSize = true;
			this->messageUpLeft->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->messageUpLeft->Location = System::Drawing::Point(3, 0);
			this->messageUpLeft->Name = L"messageUpLeft";
			this->messageUpLeft->Size = System::Drawing::Size(229, 32);
			this->messageUpLeft->TabIndex = 2;
			this->messageUpLeft->Text = L"���� ����� ������ ��� ������";
			this->messageUpLeft->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// dataLeft
			// 
			this->dataLeft->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->dataLeft->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
			this->dataLeft->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataLeft->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(2) {
				this->dataLeftName,
					this->dataLeftNum
			});
			this->dataLeft->Location = System::Drawing::Point(3, 96);
			this->dataLeft->Name = L"dataLeft";
			this->dataLeft->Size = System::Drawing::Size(229, 266);
			this->dataLeft->TabIndex = 7;
			// 
			// dataLeftName
			// 
			this->dataLeftName->HeaderText = L"������������";
			this->dataLeftName->Name = L"dataLeftName";
			this->dataLeftName->Width = 108;
			// 
			// dataLeftNum
			// 
			this->dataLeftNum->HeaderText = L"����������";
			this->dataLeftNum->Name = L"dataLeftNum";
			this->dataLeftNum->ReadOnly = true;
			this->dataLeftNum->Width = 91;
			// 
			// debugLabel
			// 
			this->debugLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->debugLabel->AutoSize = true;
			this->debugLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->debugLabel->ForeColor = System::Drawing::Color::Red;
			this->debugLabel->Location = System::Drawing::Point(238, 365);
			this->debugLabel->Name = L"debugLabel";
			this->debugLabel->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->debugLabel->Size = System::Drawing::Size(229, 33);
			this->debugLabel->TabIndex = 9;
			this->debugLabel->Text = L"Debug Text";
			this->debugLabel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// enterLeft
			// 
			this->enterLeft->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->enterLeft->Location = System::Drawing::Point(3, 67);
			this->enterLeft->Name = L"enterLeft";
			this->enterLeft->Size = System::Drawing::Size(229, 23);
			this->enterLeft->TabIndex = 10;
			this->enterLeft->Text = L"�����";
			this->enterLeft->UseVisualStyleBackColor = true;
			this->enterLeft->Click += gcnew System::EventHandler(this, &MyForm::enterLeft_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->AutoSize = true;
			this->ClientSize = System::Drawing::Size(499, 427);
			this->Controls->Add(this->tableLayoutPanel);
			this->ImeMode = System::Windows::Forms::ImeMode::Disable;
			this->KeyPreview = true;
			this->Name = L"MyForm";
			this->Padding = System::Windows::Forms::Padding(10, 0, 0, 10);
			this->Text = L"���������� ������������";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->tableLayoutPanel->ResumeLayout(false);
			this->tableLayoutPanel->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataRight))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataLeft))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void tableLayoutPanel_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
	}
	private: System::Void inputDataRight_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	}

	/// Get input string, process it and clear text box
	private: System::Void enterLeft_Click(System::Object^  sender, System::EventArgs^  e) {
		checkAndAddData(this->inputDataLeft->Text, 'L');
		this->inputDataLeft->Clear();
	}

	/// Get input string, process it and clear text box
	private: System::Void enterRight_Click(System::Object^  sender, System::EventArgs^  e) {
		checkAndAddData(this->inputDataRight->Text, 'R');
		this->inputDataRight->Clear();
	}

	/// Check that input is correct (symbols and length)
	/// Find same items in database and add (substract) from them
	private: void checkAndAddData(System::String^ inputString, char table) {
		/// Regular expressions to check all input and each object name
		System::Text::RegularExpressions::Regex regexBig("[^A-F, \d, \s]");
		System::Text::RegularExpressions::Regex regexSmall("[^A-F, \d]");

		/// Input must be more than ID_LENGTH characters
		if (inputString->Length < ID_LENGTH) {
			this->debugLabel->Text = "Bad Input";
			return;
		}

		/// Add to DB if it has key word in the beginning
		if (inputString->Substring(0, KEY_DICT->Length)->Contains("! ")) {
			/// Proper length of the name
			if (inputString->Length < (ID_LENGTH + KEY_DICT->Length + 2)) {
				return;
			}
			else {
				/// Get ID and object name
				System::String^ itemID = inputString->Substring(KEY_DICT->Length, ID_LENGTH);
				System::String^ itemName = inputString->Substring(KEY_DICT->Length + ID_LENGTH + 1);
					/// If ID is existed return
					if (idName.ContainsKey(itemID)) {
						this->debugLabel->Text = "Key is already in table";
						return;
					}
					/// If ID is new - add it to dictionary
					else {
						/// To get name for adding to the table
						idName.Add(itemID, itemName);
						/// To get id for saving to the file
						nameID.Add(itemName, itemID);
						return;
					}
			}
		}

		/// Check all input strings
		if (regexBig.IsMatch(inputString)) {
			this->debugLabel->Text = "Bad Input Strings";
			//return;
		}
		else {
			this->debugLabel->Text = "Good Input Strings";
		}

		/// Check that overall length is correct
		size_t numString = int(inputString->Length / ID_LENGTH);
		size_t checkLimits = inputString->Length - (ID_LENGTH * numString + (numString - 1));
		if (checkLimits != 0) {
			this->debugLabel->Text = "Bad Input Strings Lentgh";
			return;
		}
		else {
			this->debugLabel->Text = "Good Input String Lentgh";
		}

		/// Get all object names substrings
		/// Check for right format and uniqness
		/// Find them in right/left tables and add/subtract them
		/// Store all added/deleted values for uniqness check
		//System::Collections::Generic::List<int> hashCodes;
		for (size_t i = 0; i < numString; i++) {
			/// Get next object name
			System::String^ subString = inputString->Substring(i*(ID_LENGTH + 1), ID_LENGTH);

			/// Check item ID in dictionary
			if (idName.ContainsKey(subString)) {
			}
			else {
				this->debugLabel->Text = "ID is not in the table";
				return;
			}

			/// Check input string
			if (regexSmall.IsMatch(inputString)) {
				this->debugLabel->Text = "Bad Input Strings";
				//return;
			}
			else {
				this->debugLabel->Text = "Good Input Strings";
			}

			System::String^ itemTable;
			idName.TryGetValue(subString, itemTable);

			/// Item is in the tables
			bool itemExists = false;
			/// Item is unique
			//bool itemUnique = true;
			/// Different cases depends on from 
			/// what input (right or left) we get string
			switch (table)
			{
			/// String is from right (��������) table
			case 'R':
				/// Check inputRight for tableRight (to ADD)
				for (int i = 0; i < (this->dataRight->Rows->Count - 1); i++) {
					/// If the object name is already in table
					if (itemTable->GetHashCode() == this->dataRight->Rows[i]->Cells[0]->Value->ToString()->GetHashCode()) {
						/// Check that name is unique
						/*for (int i = 0; i < (hashCodes.Count - 1); i++) {
							if (subString->GetHashCode() == hashCodes[i].GetHashCode()) {
								this->debugLabel->Text = "Item is not unique";
								itemUnique = false;
								break;
							}
						}*/
						
						//if (itemUnique) {
						//	hashCodes.Add(subString->GetHashCode());
						/// Get current number of objects and increment it
						int currentValue = System::Int32::Parse(this->dataRight->Rows[i]->Cells[1]->Value->ToString());
						this->dataRight->Rows[i]->Cells[1]->Value = currentValue + 1;
						itemExists = true;
						//}

						break;
					}
				}
				/// Check inputRight for tableLeft (to REMOVE)
				for (int i = 0; i < (this->dataLeft->Rows->Count - 1); i++) {
					/// Get current number of objects and decrement it (or delete if it is 1)
					if (itemTable->GetHashCode() == this->dataLeft->Rows[i]->Cells[0]->Value->ToString()->GetHashCode()) {
						/// Current number
						int currentValue = System::Int32::Parse(this->dataLeft->Rows[i]->Cells[1]->Value->ToString());
						/// If it is one delete row
						if (currentValue == 1) {
							this->dataLeft->Rows->RemoveAt(i);
						}
						/// Else decrement number
						else {
							this->dataLeft->Rows[i]->Cells[1]->Value = currentValue - 1;
						}

						//itemExists = true;
						break;
					}
				}
				/// Add new row if item is new
				if (!itemExists) {
					//hashCodes.Add(subString->GetHashCode());
					this->dataRight->Rows->Add(itemTable, 1);
				}
				break;

			/// String is from left (��������) table
			case 'L':
				/// Check inputLeft for tableLeft (to ADD)
				for (int i = 0; i < (this->dataLeft->Rows->Count - 1); i++) {
					/// If the object name is already in table
					if (itemTable->GetHashCode() == this->dataLeft->Rows[i]->Cells[0]->Value->ToString()->GetHashCode()) {

						int currentValue = System::Int32::Parse(this->dataLeft->Rows[i]->Cells[1]->Value->ToString());
						this->dataLeft->Rows[i]->Cells[1]->Value = currentValue + 1;
						itemExists = true;

						break;
					}
				}
				/// Check inputLeft for tableRight (to REMOVE)
				for (int i = 0; i < (this->dataRight->Rows->Count - 1); i++) {
					/// Get current number of objects and decrement it (or delete if it is 1)
					if (itemTable->GetHashCode() == this->dataRight->Rows[i]->Cells[0]->Value->ToString()->GetHashCode()) {

						int currentValue = System::Int32::Parse(this->dataRight->Rows[i]->Cells[1]->Value->ToString());
						if (currentValue == 1) {
							this->dataRight->Rows->RemoveAt(i);
						}
						else {
							this->dataRight->Rows[i]->Cells[1]->Value = currentValue - 1;
						}

						//itemExists = true;
						break;
					}
				}
				if (!itemExists) {
					this->dataLeft->Rows->Add(itemTable, 1);
					//this->dataLeft->Rows->Add(subString, 1);
				}

			default:
				break;
			}
		}

		return;
	}

	/// Clear all tables
	private: System::Void clearButton_Click(System::Object^  sender, System::EventArgs^  e) {
		this->dataRight->Rows->Clear();
		this->dataLeft->Rows->Clear();
	}

	/// Save all data in tables into file
	private: void SaveFile(void) {
		try {
			/// Save input (�����) objects
			for (size_t i = 0; i < (this->dataLeft->Rows->Count - 1); i++) {
				/// Base tag
				System::Xml::XmlElement^ element = file.CreateElement("item");

				/// Add to base tags three childs: object id, name and count
				System::String^ itemName = this->dataLeft->Rows[i]->Cells[0]->Value->ToString();
				System::String^ itemID;
				nameID.TryGetValue(itemName, itemID);

				System::String^ elementText = "<id>" + itemID + "</id>" +
					"<name>" + itemName + "</name>" +
					"<count>" + this->dataLeft->Rows[i]->Cells[1]->Value->ToString() + "</count>";
				element->InnerXml = elementText;

				/// Append item to xml
				file.GetElementsByTagName("input")[0]->AppendChild(element);
			}
			/// Save output (��������) objects
			for (size_t i = 0; i < (this->dataRight->Rows->Count - 1); i++) {
				/// Base tag
				System::Xml::XmlElement^ element = file.CreateElement("item");

				/// Add to base tags three childs: object id, name and count
				System::String^ itemName = this->dataRight->Rows[i]->Cells[0]->Value->ToString();
				System::String^ itemID;
				nameID.TryGetValue(itemName, itemID);

				System::String^ elementText = "<id>" + itemID + "</id>" +
					"<name>" + itemName + "</name>" +
					"<count>" + this->dataRight->Rows[i]->Cells[1]->Value->ToString() + "</count>";
				element->InnerXml = elementText;

				/// Append item to xml
				file.GetElementsByTagName("output")[0]->AppendChild(element);
			}
			/// Save XML file
			file.Save(fileName);
		}
		catch (...) {

		}
	}
	};
}